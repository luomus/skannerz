# Skannerz #
### What is this repository for? ###
* This repository is for a software development course in Helsingin Yliopisto.
* We are developing a new version of a control software for digitalization line used by Luomus.
* The aim of the project is to process image data got from physical samples in the line, combine that with metadata, and ultimately send it to an digital archival system.

### Links to resources ###
* [Pivotal Tracker (backlog)](https://www.pivotaltracker.com/n/projects/2104412#)
* Estimated user stories as Epics (Left sidebar).
* [OneDrive](https://helsinkifi-my.sharepoint.com/personal/psmaatta_ad_helsinki_fi/_layouts/15/guestaccess.aspx?folderid=1e0be6be9f73041fdb0b2c29001661719&authkey=AeMXsrLw1DUKJACYhe0LRWw)
* [Hours](https://helsinkifi-my.sharepoint.com/personal/psmaatta_ad_helsinki_fi/_layouts/15/WopiFrame.aspx?sourcedoc=%7B01AED7F5-8AF8-4F75-9C63-9053B65D7725%7D&file=Skannerz%20stuff.xlsx&action=default&IsList=1&ListId=%7BE62AF914-DF77-4464-98D9-FBD4443CEB2C%7D&ListItemId=37)


### Staging ###

* [http://skannerz.it.helsinki.fi:8081/skannerz](http://skannerz.it.helsinki.fi:8081/skannerz)
  * Staging is accessible only from the university network.

### Installation? ###
* TBD when the project gets in working state.
* Clone the repository.
* Open in IDE.

### Usage ###
* TBD when the project gets in working state.
* Run in IDE.

### Who do I talk to? ###
* Any repo owner or admin.

### The Developers ###
* [Arto Haverinen](https://bitbucket.org/havarto/)
* [Jarmo Kallio](https://bitbucket.org/jarmokallio/)
* [Perttu Määttä](https://bitbucket.org/perdua/)
* [Peter Porttinen](https://bitbucket.org/porttpet/)
* [Riku Hänninen](https://bitbucket.org/rhanninen/)
* [Walter Grönholm](https://bitbucket.org/waxwax/)

### License ####

Copyright (c) 2017 University Of Helsinki

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

