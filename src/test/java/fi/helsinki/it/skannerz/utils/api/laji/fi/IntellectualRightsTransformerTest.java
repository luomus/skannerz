package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.domain.IntellectualRight;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author riku
 */
public class IntellectualRightsTransformerTest {

    private final static String SOURCE_JSON = "["
        + "{\"id\": \"1\", \"value\": \"Public Domain\"}, "
        + "{\"id\": \"2\", \"value\": \"Creative Commons\"}"
        + "]";

    private final static ResponseTransformer<List<IntellectualRight>> TRANSFORMER =
        new IntellectualRightsTransformer();

    @Test
    public void transformsCorrectly() {
        List<IntellectualRight> transformed = TRANSFORMER.transform(SOURCE_JSON);
        assertTrue(transformed.get(0).getId().equals("1"));
        assertTrue(transformed.get(0).getValue().equals("Public Domain"));
        assertTrue(transformed.get(1).getId().equals("2"));
        assertTrue(transformed.get(1).getValue().equals("Creative Commons"));
    }
    
}
