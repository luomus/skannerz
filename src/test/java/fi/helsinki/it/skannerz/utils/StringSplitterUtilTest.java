/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.utils;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author waxwax
 */
public class StringSplitterUtilTest {
    
    public StringSplitterUtilTest() {
    }

    /**
     * Test of getListOf method, of class StringSplitterUtil.
     */
    @Test
    public void testGetListOf() {
        System.out.println("testCheckTags");
        List<String> expectedList = new ArrayList<>();
        expectedList.add("test1");
        expectedList.add("test2");
        expectedList.add("test3");
        expectedList.add("test4");
        expectedList.add("test5");
        expectedList.add("test6");
        expectedList.add("test7");
        assertEquals(expectedList, StringSplitterUtil.getListOf("test1\ntest2,test3 , test4 ;test5; test6 \n\n test7"));
    }
    
}
