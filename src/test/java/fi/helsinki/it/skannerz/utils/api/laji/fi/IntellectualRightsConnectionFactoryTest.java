/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.utils.api.laji.fi;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author riku
 */
public class IntellectualRightsConnectionFactoryTest {

    private final ConnectionFactory factory = new IntellectualRightsConnectionFactory(
        "http://doesntmatter.com"
    );
    private LajiApiConnection connection;

    @Before
    public void setUp() {
        connection = factory.createConnection("bad_token");
    }

    @Test
    public void queryStringIsCorrect() {
        String expected = "access_token=bad_token&lang=en&asLookUpObject=false&classTypeAsList=false";
        String queryString = connection.getQueryString();
        assertEquals(expected, queryString);
    }

    @Test
    public void hasCorrectHeader() {
        String expectedValue = "application/json";
        assertTrue(connection.getHeaderField("Accept") != null);
        assertEquals(connection.getHeaderField("Accept"), expectedValue);
    }
    
}
