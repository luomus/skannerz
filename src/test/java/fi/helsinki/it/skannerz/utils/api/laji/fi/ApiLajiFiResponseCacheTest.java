package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.utils.PathCreator;
import java.io.File;
import java.net.CacheResponse;
import java.net.HttpURLConnection;
import java.net.URI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author riku
 */
public class ApiLajiFiResponseCacheTest {
    private static final Logger LOG = LoggerFactory.getLogger(ApiLajiFiResponseCacheTest.class);

    private String cacheDirRoot = "";
    private ApiLajiFiResponseCache cache;

    @Before
    public void setUp() {
        cacheDirRoot = ApiLajiFiResponseCacheTest.class.getClassLoader()
            .getResource("").getPath() + "testCache";
        LOG.info("testCache: {}", cacheDirRoot);
        this.cache = new ApiLajiFiResponseCache(cacheDirRoot);
    }

    @After
    public void tearDown() {
        File cacheDirectory = new File(cacheDirRoot);
        delete(cacheDirectory);
    }

    private void delete(File f) {
        if (f.isDirectory()) {
            for (File children : f.listFiles()) {
                delete(children);
            }
        }
        LOG.info("Deleting {}", f.getAbsolutePath());
        f.delete();
    }

    @Test
    public void cacheGetReturnsNullIfConnectionIsNotForApiLajiFi() throws Exception {
        URI uri = new URI("http://helsinki.fi");
        assertEquals(null, cache.get(uri, null, null));
    }

    @Test
    public void cacheFileFoundAndIsNewEnoughReturnIt() throws Exception {
        URI uri = new URI("https://api.laji.fi");
        String requestMethod = "GET";
        String location = PathCreator.create(
            cacheDirRoot,
            Digester.getHexString("SHA-256", uri.toASCIIString()), 
            "GET",
            "body"
        );
        File cacheFile = new File(location);
        cacheFile.getParentFile().mkdirs();
        cacheFile.createNewFile();
        CacheResponse response = cache.get(uri, requestMethod, null);
        LOG.info("{}", location);
        assertNotNull(response);
    }

    @Test
    public void cacheFileFoundAndIsOldEnoughReturnNull() throws Exception {
        URI uri = new URI("https://api.laji.fi");
        String requestMethod = "GET";
        String location = PathCreator.create(
            cacheDirRoot,
            Digester.getHexString("SHA-256", uri.toASCIIString()), 
            "GET",
            "body"
        );
        File cacheFile = new File(location);
        cacheFile.getParentFile().mkdirs();
        cacheFile.createNewFile();
        cacheFile.setLastModified(System.currentTimeMillis() - 60*60*1000);
        CacheResponse response = cache.get(uri, requestMethod, null);
        assertNull(response);
    }

    @Test
    public void cacheFileNotFoundReturnNull() throws Exception {
        URI uri = new URI("https://api.laji.fi");
        String requestMethod = "GET";
        String location = PathCreator.create(
            cacheDirRoot,
            Digester.getHexString("SHA-256", uri.toASCIIString()), 
            "GET",
            "body"
        );
        CacheResponse response = cache.get(uri, requestMethod, null);
        assertNull(response);
    }

    @Test
    public void cacheFileIsPutIntoTheCacheForApiLajiFi() throws Exception {
        URI uri = new URI("https://api.laji.fi");
        HttpURLConnection mockConnection = mock(HttpURLConnection.class);
        when(mockConnection.getRequestMethod()).thenReturn("GET");
        assertNotNull(cache.put(uri, mockConnection));
    }

    @Test
    public void cacheFileIsNotPutIntoTheCacheForNotApiLajiFi() throws Exception {
        URI uri = new URI("https://helsinki.fi");
        HttpURLConnection mockConnection = mock(HttpURLConnection.class);
        when(mockConnection.getRequestMethod()).thenReturn("GET");
        assertNull(cache.put(uri, mockConnection));
    }
    
}
