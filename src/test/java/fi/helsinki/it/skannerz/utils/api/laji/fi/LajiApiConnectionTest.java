package fi.helsinki.it.skannerz.utils.api.laji.fi;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author riku
 */
public class LajiApiConnectionTest {

    private final String URL = "https://api.laji.fi/v0/metadata/properties/MZ.intellectualRights/ranges";

    @Test(expected = IOException.class)
    public void IOExceptionThrownWithBadAccessToken() throws IOException {
        new LajiApiConnection.Builder("GET", "bad_token", URL)
            .build().getResult();
    }

    @Test(expected = IllegalArgumentException.class)
    public void IllegalArgumentExceptionThrownWithBadlyMalformedURL() throws IOException {
        new LajiApiConnection.Builder("GET", "bad_token", "bad_url")
            .build().getResult();
    }

    
}
