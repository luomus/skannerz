package fi.helsinki.it.skannerz.utils;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.qrcode.QRCodeReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author riku
 */
public class BarcodeReaderTest {

    private static final Logger LOG = LoggerFactory.getLogger(BarcodeReaderTest.class);

    private ClassLoader classLoader;

    private static final String BASE_PATH = "qr/";
    private String invalidPath;
    private URI invalid;

    private final Map<String, String> identifiers = new HashMap<>();

    private IdResolver QRReader;

    public BarcodeReaderTest() {
        this.identifiers.put("Image002_11.tif", "http://id.luomus.fi/F.119880");
        this.identifiers.put("Image002_13.tif", "http://id.luomus.fi/F.119878");
        this.identifiers.put("Image002_15.tif", "http://id.luomus.fi/F.119877");
        this.identifiers.put("Image002_17.tif", "http://id.luomus.fi/F.119876");
        this.identifiers.put("Image002_23.tif", "http://id.luomus.fi/F.119852");
        this.identifiers.put("Image002_27.tif", "http://id.luomus.fi/F.119850");
        this.identifiers.put("Image002_29.tif", "http://id.luomus.fi/F.119849");
        this.identifiers.put("Image002_31.tif", "http://id.luomus.fi/F.119845");
        this.identifiers.put("Image002_33.tif", "http://id.luomus.fi/F.119844");
        this.identifiers.put("Image002_35.tif", "http://id.luomus.fi/F.119842");
    }

    @Before
    public void setUp() throws URISyntaxException {

        this.classLoader = BarcodeReaderTest.class.getClassLoader();
        this.QRReader = new BarcodeReader(new QRCodeReader());

        this.invalidPath = "testRootFolder/dc1.2017-09-01_11-06-38_7bbdc5/Image001.tif";

        this.invalid = classLoader.getResource(invalidPath).toURI();

    }

    @Test(expected = NullPointerException.class)
    public void URINullExceptionIsThrown() throws ReaderException, IOException {
        QRReader.getIdentifier(null);
    }

    @Test(expected = FormatException.class)
    public void NoCodeInImageExceptionIsThrown() throws ReaderException, IOException {
        QRReader.getIdentifier(invalid);
    }

    @Test
    public void correctnessMegaTest() {
        double maxErrorRatio = 0.101;

        Map<String, String> resultMap = new HashMap<>();
        for (String imageName : identifiers.keySet()) {
            try {
                URI uri = classLoader.getResource(BASE_PATH + imageName).toURI();
                resultMap.put(imageName, QRReader.getIdentifier(uri));
            } catch (URISyntaxException | ReaderException | IOException ex) {
                LOG.error("Error parsing " + imageName, ex);
            }
        }
        for (Entry<String, String> entry : resultMap.entrySet()) {
            assertEquals("Got wrong identifier", identifiers.get(entry.getKey()), entry.getValue());
        }
        if (identifiers.size() != resultMap.size()) {
            LOG.warn(resultMap.size() + "/" + identifiers.size() + " succeeded");
            Set<String> erronous = new HashSet<>(identifiers.keySet());
            erronous.removeAll(resultMap.keySet());
            LOG.warn("Erronous or missing: " + erronous);
            if (erronous.size() / (double) identifiers.size() > maxErrorRatio) {
                fail("Too many failing QR reads, " + resultMap.size() + "/" + identifiers.size() + " succeeded");
            }
        } else {
            LOG.info("All succeeded!");
        }
    }

}
