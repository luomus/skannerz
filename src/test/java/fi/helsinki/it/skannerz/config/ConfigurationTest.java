package fi.helsinki.it.skannerz.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author havarto, waxwax
 */
public class ConfigurationTest {

    private static class TestPropertiesLoader extends PropertiesLoader {

        @Override
        public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
            properties.put("pipelineThreads", "42");
            properties.put("folderNameRegex", "*");
            properties.put("domains", "domain1,domain2");
            properties.put("imageFolderPath", "root");
            properties.put("imageFolderPath.relative", "true");
            properties.put("username", "testuser");
            properties.put("password", "th!s1s4p455w0rd");
            properties.put("imageStorageDbUri", "https://uri.net/api");
            properties.put("keywords", "keyword1,keyword2");
            properties.put("intellectualRights.api.accessToken", "token_token");
            properties.put("intellectualRights.api.url", "http://doesntmatter.com");
            properties.put("intellectualRights.defaultValue", "testIntellectualRight");
            properties.put("history.maxDisplayedRows", "1250");
            properties.put("api.laji.fi.cache.location", "/tmp/api_laji_fi_cache/");
            properties.put("label.MM.person.url", "http://doesntmatter.com");
            properties.put("label.MM.image.url", "http://doesntmatter.com");
            properties.put("defaultLanguage", "en");
            properties.put("defaultWaitTimeForFolder", "3000");
        }
    }

    /**
     * Test of getUsername method, of class Configuration.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetUsername() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "testuser";
        String result = Configuration.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPassword method, of class Configuration.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetPassword() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "th!s1s4p455w0rd";
        String result = Configuration.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of getImageStorageDdUri method, of class Configuration.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetImageStorageDdUri() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "https://uri.net/api";
        String result = Configuration.getImageStorageDdUri();
        assertEquals(expResult, result);
    }

    /*
     * When System property (-D parameters) local.properties.path is set,
     * Configuration should be read from that path.
     */
    @Test
    public void testInitSystemPropertySet() throws Exception {
        System.setProperty("local.properties.path", "src/main/resources/local.test.properties");
        Configuration.init(null);
        System.clearProperty("local.properties.path");

        String expUser = "testuser";
        String user = Configuration.getUsername();
        assertEquals(expUser, user);

        String expPassword = "testpw";
        String password = Configuration.getPassword();
        assertEquals(expPassword, password);

        String expUri = "https://url.fi/api";
        String uri = Configuration.getImageStorageDdUri();
        assertEquals(expUri, uri);
        
        String expDefaultIR = "MZ.intellectualRightsARR";
        String IR = Configuration.getDefaultIntellectualRight();
        assertEquals(expDefaultIR, IR);
    }

    @Test
    public void testGetPipelineThreads() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        Integer expResult = 42;
        Integer result = Configuration.getPipelineThreads();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetFolderNameRegex() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "*";
        String result = Configuration.getFolderNameRegex();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDomainList() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        List<String> expResult = Arrays.asList(new String[]{
            "domain1", "domain2"
        });
        List<String> result = Configuration.getDomainList();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetImageFolderPath() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "root";
        String result = Configuration.getImageFolderPath();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetKeywordsList() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        List<String> expResult = Arrays.asList(new String[]{
            "keyword1", "keyword2"
        });
        List<String> result = Configuration.getKeywordsList();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetApiLajiFiAccessToken() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "token_token";
        String result = Configuration.getApiLajiFiAccessToken();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetApiLajiFiIntellectualRightsURL() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "http://doesntmatter.com";
        String result = Configuration.getIntellectualRightURL();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetApiLajiFiCacheLocation() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");
        String expResult = "/tmp/api_laji_fi_cache/";
        String result = Configuration.getApiLajiFiCacheLocation();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsImageFolderPathRelative() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        Boolean expResult = true;
        Boolean result = Configuration.isImageFolderPathRelative();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDefaultIntellectualRights() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        String expResult = "testIntellectualRight";
        String result = Configuration.getDefaultIntellectualRight();
        assertEquals(expResult, result);
    }

    @Test
    public void testLabelPersonURL() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");
        String expResult = "http://doesntmatter.com";
        String result = Configuration.getMMPersonLabelURL();
        assertEquals(expResult, result);
    }

    @Test
    public void testLabelImageURL() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");
        String expResult = "http://doesntmatter.com";
        String result = Configuration.getMMImageLabelURL();
        assertEquals(expResult, result);
    }

    @Test
    public void getDefaultLanguage() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");
        String expResult = "en";
        String result = Configuration.getDefaultLanguage();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testMaxDisplayedHistoryRows() throws Exception {
        Configuration.init(new TestPropertiesLoader(), "default", "local");

        Integer expResult = 1250;
        Integer result = Configuration.getMaxDisplayedHistoryRows();
        assertEquals(expResult, result);
    }

    @Test(expected = IOException.class)
    public void testInitConfigurationFileReadFail() throws Exception {
        Configuration.init("doesnotexist");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitConfigurationFileMissingDefaultField() throws Exception {

        TestPropertiesLoader testPropertiesLoader = new TestPropertiesLoader() {
            @Override
            public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
                super.loadProperties(properties, fileName, systemPropertyName);
                properties.remove("folderNameRegex");
            }
        };

        Configuration.init(testPropertiesLoader, "default", "local");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitConfigurationFileMissingLocalField() throws Exception {

        TestPropertiesLoader testPropertiesLoader = new TestPropertiesLoader() {
            @Override
            public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
                super.loadProperties(properties, fileName, systemPropertyName);
                properties.remove("password");
            }
        };

        Configuration.init(testPropertiesLoader, "default", "local");
    }

    @Test(expected = NumberFormatException.class)
    public void testInitConfigurationFileBrokenIntegerField() throws Exception {

        TestPropertiesLoader testPropertiesLoader = new TestPropertiesLoader() {
            @Override
            public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
                super.loadProperties(properties, fileName, systemPropertyName);
                properties.put("pipelineThreads", "not a number");
            }
        };

        Configuration.init(testPropertiesLoader, "default", "local");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitConfigurationFileBrokenBooleanField() throws Exception {

        TestPropertiesLoader testPropertiesLoader = new TestPropertiesLoader() {
            @Override
            public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
                super.loadProperties(properties, fileName, systemPropertyName);
                properties.put("imageFolderPath.relative", "not a boolean");
            }
        };

        Configuration.init(testPropertiesLoader, "default", "local");
    }

}
