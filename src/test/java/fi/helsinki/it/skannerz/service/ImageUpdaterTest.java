package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.luomus.kuvapalvelu.client.ImageApiClientImpl;
import fi.luomus.kuvapalvelu.client.model.Meta;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import java.io.File;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.when;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Walter Grönholm
 */
public class ImageUpdaterTest {

    private SpecimenData specimenData;
    private FolderMock folder;
    private ExtendedMeta meta;

    public ImageUpdaterTest() {
    }

    @Before
    public void setUp() throws Exception {
        specimenData = mock(SpecimenData.class);
        meta = new ExtendedMeta();
        meta.setLicense("something");
        folder = new FolderMock(new ExtendedMeta());
        when(specimenData.getFolder()).thenReturn(folder);
    }

    @After
    public void tearDown() {
    }

    @Test(expected = NullPointerException.class)
    public void testUpdateNullId() {
        new ImageUpdater().update(meta, new ImageApiClientMock(false));
    }

    @Test(expected = NullPointerException.class)
    public void testUpdateNullMeta() {
        meta.setUploadedId("this is my id");
        new ImageUpdater().update(null, new ImageApiClientMock(false));
    }

    @Test(expected = NullPointerException.class)
    public void testUpdateNullImageApiClient() {
        meta.setUploadedId("this is my id");
        new ImageUpdater().update(meta, null);
    }

    @Test
    public void testUpdateWithFileNotFoundException() {
        meta.setUploadedId("this is my id");
        ImageApiClientMock imageApiClient = new ImageApiClientMock(true);
        PipelineReturnCode code = new ImageUpdater().update(meta, imageApiClient);

        assertEquals(PipelineReturnCode.COULD_NOT_FIND_FILE, code);
        assertTrue(imageApiClient.wasUpdateCalledWith("this is my id", meta));
    }

    @Test
    public void testUpdate() {
        meta.setUploadedId("this is my id");
        ImageApiClientMock imageApiClient = new ImageApiClientMock(false);
        PipelineReturnCode code = new ImageUpdater().update(meta, imageApiClient);

        assertEquals(PipelineReturnCode.OK, code);
        assertTrue(imageApiClient.wasUpdateCalledWith("this is my id", meta));
    }

    private static class FolderMock extends Folder {

        private ExtendedMeta saved;
        private final ExtendedMeta meta;

        public FolderMock(ExtendedMeta meta) {
            super("");
            this.meta = meta;
        }

        @Override
        public Boolean getProcessed() {
            return true;
        }

        @Override
        public String getName() {
            return "FolderStub";
        }

        private boolean wasMetaSaved(ExtendedMeta meta) {
            return saved.equals(meta);
        }
    }

    private static class ImageApiClientMock extends ImageApiClientImpl {

        private Pair<String, Meta> methodCall;
        private final boolean throw404;

        public ImageApiClientMock(boolean throw404) {
            super(null);
            this.throw404 = throw404;
        }

        @Override
        public void update(String id, Meta meta) throws ApiException, NotFoundException {
            if (methodCall == null) {
                methodCall = Pair.of(id, meta);
            } else {
                fail("Attempted to update twice with id: " + id + ", and Meta: " + meta);
            }
            if (throw404) {
                throw new NotFoundException("404");
            }
        }

        public boolean wasUpdateCalledWith(String id, Meta meta) {
            return Pair.of(id, meta).equals(methodCall);
        }
    }

}
