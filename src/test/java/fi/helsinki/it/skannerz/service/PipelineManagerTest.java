package fi.helsinki.it.skannerz.service;

import com.google.common.base.Optional;
import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.utils.IdResolver;
import fi.helsinki.it.skannerz.utils.MetadataUtils;
import fi.luomus.kuvapalvelu.client.ImageApiClient;
import fi.luomus.kuvapalvelu.client.model.License;
import fi.luomus.kuvapalvelu.client.model.Media;
import fi.luomus.kuvapalvelu.client.model.Meta;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

public class PipelineManagerTest {

    private static final Logger LOG = LoggerFactory.getLogger(PipelineManagerTest.class);
    private final String rootFolder = "/testRootFolder";
    private final String rootFolderPath = this.getClass().getResource(rootFolder).getFile();

    public PipelineManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        Configuration.init("local.test.properties");
    }

    @Test
    public void TestOldMetadataIsMergedWhenFoundInFolder() throws Exception {
        IdResolver resolverMock = mock(IdResolver.class);
        when(resolverMock.getIdentifier(any())).thenReturn("111");
        RootFolderReaderStub3 folderReader = new RootFolderReaderStub3();

        PipelineManager manager = new PipelineManager(folderReader, resolverMock, new PipelineFactory());
        manager.setMailbox(new MailboxStub());
        manager.setImageApiClient(new ImageApiClientStub());
        
        List<String> tagsOld = new ArrayList<>();
        tagsOld.add("oldtag");
        tagsOld.add("label");
        List<String> tagsNew = new ArrayList<>();
        tagsNew.add("newtag");
        ExtendedMeta oldMeta = createMetadata("O1", "L1", tagsOld, null);
        ExtendedMeta newMeta = createMetadata("O2", null, tagsNew, "U2");
        ExtendedMeta expectedMeta = MetadataUtils.merge(oldMeta, newMeta);

        Folder testFolder = new Folder(new File(rootFolderPath + "/testikansio3"));
        testFolder.mkdir();
        testFolder.deleteOnExit();
        testFolder.addTifImages(new File("testikuva"));
        try {
            Image testikuva = testFolder.getTifImage("testikuva");
            testikuva.createMetadataObjectFrom(oldMeta);
            testikuva.saveMeta();
            folderReader.setReturnFolder(testFolder);

            manager.setImageMetadata(newMeta);
            Thread th = manager.startPipelines("PipelineManagerTest.TestOldMetadataIsUsedWhenFoundInFolder");
            Thread.sleep(3000);

            Meta metadataInFolder = testFolder.getTifImage("testikuva").getMeta();
            System.out.println("test:      " + metadataInFolder);
            System.out.println("should be: " + expectedMeta);

            assertEquals("O2", metadataInFolder.getRightsOwner());
            assertEquals("L1", metadataInFolder.getLicense());
            List<String> expectedTags = new ArrayList<>();
            expectedTags.add("newtag");
            expectedTags.add("label");
            assertEquals(expectedTags, metadataInFolder.getTags());
            assertEquals("U2", metadataInFolder.getUploadedBy());

            th.interrupt();
        } finally {
            removeFiles("/testikansio3");
        }
    }

     @Test
    public void TestSetImageMetadataChangesMetadataForNewFolders() throws Exception {
        IdResolver resolverMock = mock(IdResolver.class);
        when(resolverMock.getIdentifier(any())).thenReturn("111");
        RootFolderReaderStub3 folderReader = new RootFolderReaderStub3();

        PipelineManager manager = new PipelineManager(folderReader, resolverMock, new PipelineFactory());
        manager.setMailbox(new MailboxStub());
        manager.setImageApiClient(new ImageApiClientStub());

        //greate new meta objects
        List<String> tags = new ArrayList<>();
        tags.add("tag1");
        ExtendedMeta metaA = createMetadata("O1", "L1", new ArrayList(tags), "U1");
        ExtendedMeta metaB = createMetadata("O2", "L2", new ArrayList(tags), "U2");

        Folder folderA = new Folder(new File(rootFolderPath + "/testikansio1"));
        Folder folderB = new Folder(new File(rootFolderPath + "/testikansio2"));
        folderA.mkdir();
        folderB.mkdir();
        folderA.deleteOnExit();
        folderB.deleteOnExit();

        folderA.addTifImages(new File("tifImageA"));
        folderB.addTifImages(new File("tifImageB"));

        folderReader.setReturnFolder(folderA);
        manager.setImageMetadata(metaA);
        Thread th = manager.startPipelines("PipelineManagerTest.TestSetImageMetadataChangesMetadataForNewFolders");
        Thread.sleep(2000);

        folderReader.setReturnFolder(folderB);
        manager.setImageMetadata(metaB);
        synchronized (folderReader) {
            folderReader.notify();
        }

        Thread.sleep(5000);

        try {
            ExtendedMeta readMetaA = folderA.getTifImage("tifImageA").getMeta();
            ExtendedMeta readMetaB = folderB.getTifImage("tifImageB").getMeta();
       
            assertEquals("O1", readMetaA.getRightsOwner());
            assertEquals("L1", readMetaA.getLicense());
            List<String> expectedTags1 = new ArrayList<>();
            expectedTags1.add("tag1");
            expectedTags1.add("label");
            assertEquals(expectedTags1, readMetaA.getTags());
            assertEquals("U1", readMetaA.getUploadedBy());
            
            assertEquals("O2", readMetaB.getRightsOwner());
            assertEquals("L2", readMetaB.getLicense());
            List<String> expectedTags2 = new ArrayList<>();
            expectedTags2.add("tag1");
            expectedTags2.add("label");
            assertEquals(expectedTags2, readMetaB.getTags());
            assertEquals("U2", readMetaB.getUploadedBy());
        } finally {
            removeFiles("/testikansio1");
            removeFiles("/testikansio2");
        }
        
        th.interrupt();
    }

    private void removeFiles(String file) {
        System.out.println("  Removing file(s) in path: " + rootFolderPath + file);
        File f = new File(rootFolderPath + file);

        while (f.exists()) {
            if (f.isDirectory() && f.listFiles().length > 0) {
                for (File fi : f.listFiles()) {
                    System.out.println(fi.getPath());
                    fi.delete();
                }
            }
            f.delete();
        }
    }

    private ExtendedMeta createMetadata(String owner, String licence, List tags, String uploader) {
        ExtendedMeta meta = new ExtendedMeta();
        meta.setRightsOwner(owner);
        meta.setLicense(licence);
        meta.setTags(tags);
        meta.setUploadedBy(uploader);
        meta.setDomain("test");
        return meta;
    }

    @Test
    public void TestThreading() throws Exception {
        IdResolver resolverMock = mock(IdResolver.class);
        when(resolverMock.getIdentifier(any())).thenReturn("111");
        PipelineManager pipelineManager = new PipelineManager(new RootFolderReaderStub(), resolverMock, new PipelineFactoryStub());
        pipelineManager.setMailbox(new MailboxStub());
        pipelineManager.setImageMetadata(new ExtendedMeta());
        pipelineManager.setImageApiClient(new ImageApiClientStub());
        Thread th = pipelineManager.startPipelines("PipelineManagerTest.TestThreading");
        Thread.sleep(2000);
        List<Future<PipelineReturnCode>> futures = pipelineManager.giveCurrentFutures();
        pipelineManager.stopPipelines();
        Thread.sleep(1000);
        //System.out.println(((PipelineReturnCode) futures.get(0).get()).getMessage());
        assertEquals(futures.get(0).get(), PipelineReturnCode.OK);
        th.interrupt();
    }

    @Test
    public void TestThreading2() throws Exception {
        IdResolver resolverMock = mock(IdResolver.class);
        when(resolverMock.getIdentifier(any())).thenReturn("111");
        RootFolderReaderInterface rFRstub = new RootFolderReaderStub2();
        new Thread(rFRstub,"PipelineManagerTest-RFRThread").start();
        PipelineManager pipelineManager = new PipelineManager(rFRstub, resolverMock, new PipelineFactoryStub());
        pipelineManager.setMailbox(new MailboxStub());
        pipelineManager.setImageMetadata(new ExtendedMeta());
        pipelineManager.setImageApiClient(new ImageApiClientStub());
        Thread th = pipelineManager.startPipelines("PipelineManagerTest.TestThreading2");
        Thread.sleep(1000);
        List<Future<PipelineReturnCode>> futures = pipelineManager.giveCurrentFutures();
        pipelineManager.stopPipelines();
        Thread.sleep(1000);
        //System.out.println(((PipelineReturnCode) futures.get(0).get()).getMessage());
        assertEquals(futures.get(0).get(), PipelineReturnCode.OK);
        th.interrupt();
    }

    @Test(expected = CancellationException.class)
    public void TestThreadingWithShortInterrupt() throws Exception {
        IdResolver resolverMock = mock(IdResolver.class);
        when(resolverMock.getIdentifier(any())).thenReturn("111");
        PipelineManager pipelineManager = new PipelineManager(new RootFolderReaderStub(), resolverMock, new PipelineFactoryStub());
        pipelineManager.setMailbox(new MailboxStub());
        pipelineManager.setImageMetadata(new ExtendedMeta());
        pipelineManager.setImageApiClient(new ImageApiClientStub());
        Thread th = pipelineManager.startPipelines("PipelineManagerTest.TestThreadingWithShortInterrupt");
        Thread.sleep(100);
        List<Future<PipelineReturnCode>> futures = pipelineManager.giveCurrentFutures();
        pipelineManager.stopPipelines();
        Thread.sleep(1000);
        // This is just to trigger the CancellationException.
        futures.get(0).get();
        th.interrupt();
    }

    private static class MailboxStub implements MailboxInterface<PipelineMessage> {

        public MailboxStub() {
        }

        @Override
        public void put(PipelineMessage message) {
        }

        @Override
        public void addReceiver(MailboxReceiver<PipelineMessage> receiver, boolean handleMsgQueue) {
        }

        @Override
        public void handleMail(MailboxReceiver<PipelineMessage> caller) {
        }

        @Override
        public void removeReceiver(MailboxReceiver<PipelineMessage> receiver) {
        }

        @Override
        public Set<MailboxReceiver<PipelineMessage>> getReceivers() {
            return null;
        }
    }

    private static class ImageApiClientStub implements ImageApiClient {

        @Override
        public List<Media> getImages(String fieldName, String value) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<Media> getImages(String fieldName, String value, int limit, int offset) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<Media> getImages(String fieldName, List<String> values) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<Media> getImages(String fieldName, List<String> values, int limit, int offset) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<Media> getSubtaxonImages(String... taxons) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<Media> getSubtaxonImages(int limit, int offset, String... taxons) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public List<License> getLicenses() throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String upload(File file, Meta meta) throws ApiException {
            return "this is an id";
        }

        @Override
        public String upload(InputStream pictureStream, String filename, Meta meta) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void update(String id, Meta newMeta) throws ApiException, NotFoundException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void delete(String id) throws ApiException, NotFoundException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Optional<Media> get(String id) throws ApiException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     * this stub returns only 2 folders and then it's done.
     */
    private static class RootFolderReaderStub implements RootFolderReaderInterface {

        private int counter = 2;

        @Override
        public Folder returnNextFolder() {
            if (counter == 0) {
                throw new IllegalStateException("quit");
            }
            counter--;
            return new Folder("tst");
        }

        @Override
        public ArrayList<Folder> returnRestFoldersAsList() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void run() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /**
     * this stub returns only 2 folders and then it's done. if addFile is
     * called, a new file is added and can be returned
     */
    private static class RootFolderReaderStub2 implements RootFolderReaderInterface {

        private int counter = 2;

        @Override
        public Folder returnNextFolder() {
            if (counter < 0) {
                throw new IllegalStateException("quit");
            }
            if (counter == 0) {
                counter--;
                return null;
            }
            counter--;
            return new Folder("tst");
        }

        @Override
        public ArrayList<Folder> returnRestFoldersAsList() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void run() {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(PipelineManagerTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            synchronized (this) {
                this.notify();
            }
        }

    }
    
    /**
     * this stub returns only 2 folders and then it's done. if addFile is
     * called, a new file is added and can be returned
     */
    private static class RootFolderReaderStub3 implements RootFolderReaderInterface {

        Folder returnFolder;
        boolean hasNewFolder = false;

        @Override
        public Folder returnNextFolder() {

            System.out.println("returnNextFolder called");

            if (hasNewFolder) {
                this.hasNewFolder = false;
                return returnFolder;
            } else {
                return null;
            }
        }

        @Override
        public ArrayList<Folder> returnRestFoldersAsList() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void setReturnFolder(Folder folder) {
            this.returnFolder = folder;
            this.hasNewFolder = true;
        }

        @Override
        public void run() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
    private static class PipelineFactoryStub extends PipelineFactory {

        @Override
        public PipelineInterface getPipeline(SpecimenData specimendata, ImageApiClient imageApiClient, IdResolver resolver) {
            return new PipelineStub();
        }

        class PipelineStub implements PipelineInterface {

            @Override
            public PipelineReturnCode call() {
                for (int i = 1; i < 10; i++) {
                    LOG.info("Running " + i + " time");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        LOG.info("I was interrupted on " + i + " run, bye.");
                        return PipelineReturnCode.SEND_FAILED_RESEND;
                    }
                }
                return PipelineReturnCode.OK;
            }
        }
    }

}
