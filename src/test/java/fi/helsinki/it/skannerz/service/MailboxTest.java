package fi.helsinki.it.skannerz.service;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 *
 * @author riku
 */
public class MailboxTest {

    private MailboxInterface<String> mailbox;
    private MailboxReceiver<String> mockReceiver;

    @Before
    public void setUp() {
        mailbox = new Mailbox<>();
        mockReceiver = mock(MailboxReceiver.class);
    }

    @Test
    public void receiverIsAddedToReceivers() {
        mailbox.addReceiver(mockReceiver, false);
        assertTrue(mailbox.getReceivers().contains(mockReceiver));
    }

    @Test
    public void receiverIsNotifiedWhenNewMessageArrives() {
        mailbox.addReceiver(mockReceiver, false);
        String msg = "Test message!";
        mailbox.put(msg);
        verify(mockReceiver).handleMail(msg);
    }

    @Test
    public void receiverIsAddedToReceiversAndMailboxQueueIsHandled() {
        String firstMsg = "First message.";
        String secondMsg = "Second message.";
        String thirdMsg = "Third message.";
        mailbox.put(firstMsg);
        mailbox.put(secondMsg);
        mailbox.put(thirdMsg);
        mailbox.addReceiver(mockReceiver, true);
        verify(mockReceiver).handleMail(firstMsg);
        verify(mockReceiver).handleMail(secondMsg);
        verify(mockReceiver).handleMail(thirdMsg);
    }


    @Test
    public void receiverIsRemovedFromReceivers() {
        mailbox.addReceiver(mockReceiver, false);
        mailbox.removeReceiver(mockReceiver);
        assertTrue(!mailbox.getReceivers().contains(mockReceiver));
    }

    @Test
    public void callbackCausesCallersHandleMailToBeInvoked() {
        String testMsg = "Test message!";
        mailbox.put(testMsg);
        mailbox.addReceiver(mockReceiver, false);
        mailbox.handleMail(mockReceiver);
        verify(mockReceiver).handleMail(testMsg);
    }

    @Test(expected = IllegalArgumentException.class)
    public void callbackCausesExceptionOnUnregisteredReceiver() {
        mailbox.handleMail(mockReceiver);
    }
    
}
