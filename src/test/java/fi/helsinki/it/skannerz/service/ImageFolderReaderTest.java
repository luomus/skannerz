/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.Folder;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Peter
 */
public class ImageFolderReaderTest {

    String rootFolder, subfolder1, subfolder2, rootPath;
    Folder modelFolder;
    File testJpg1, testJpg2, testAscii, testJpgThumb1, testJpgThumb2, testXml, testTif1, testTif2;

    public ImageFolderReaderTest() {
        this.rootFolder = "/testRootFolder";
        this.subfolder1 = this.getClass().getResource("/testRootFolder/dc1.2017-09-01_11-03-48_b8e085").getFile();
        this.subfolder2 = this.getClass().getResource("/testRootFolder/dc1.2017-09-01_11-06-38_7bbdc5").getFile();
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * create test folder for comparison.
     */
    @Before
    public void setUp() {

        testAscii = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/digitisation.properties").getFile());

        testJpg1 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Preview001.jpg").getFile());
        testJpg2 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Preview002.jpg").getFile());

        testJpgThumb1 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Thumb001.jpg").getFile());
        testJpgThumb2 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Thumb002.jpg").getFile());

        testXml = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/dc1.2017-09-01_11-06-38_7bbdc5.xml").getFile());

        testTif1 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Image001.tif").getFile());
        testTif2 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Image002.tif").getFile());

        this.modelFolder = new Folder(subfolder2);
        modelFolder.setAscii(testAscii);
        modelFolder.setJpg(testJpg1, testJpg2);
        modelFolder.setJpgThumbs(testJpgThumb1, testJpgThumb2);
        modelFolder.addTifImages(testTif1, testTif2);
        modelFolder.setXmlFile(testXml);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createFolderObject method, of class ImageFolderReader.
     */
    @Test
    public void folderComparisonTest() {
        ImageFolderReaderInterface ir = new ImageFolderReader();
        Folder testFolder = ir.createFolderObject(new File(subfolder2));

        assertEquals(modelFolder.getXml(), testFolder.getXml());
        assertEquals(modelFolder.getJpg(), testFolder.getJpg());
        assertEquals(modelFolder.getTifImages(), testFolder.getTifImages());
        assertEquals(modelFolder.getAscii(), testFolder.getAscii());
    }

    @Test(expected = IllegalStateException.class)
    public void testCreateFolderFromNullWorkingFile() {
        ImageFolderReaderInterface ir = new ImageFolderReader();
        Folder nullFolder = ir.createFolderObject(new File(this.getClass().getResource(rootFolder).getPath()));
    }

    /**
     * Test of distinquishJpgs method, of class ImageFolderReader.
     */
    @Test
    public void testDistinquishJpgs() {
        ImageFolderReader ir = new ImageFolderReader();
        Folder mockFolder = mock(Folder.class);

        ir.distinquishJpg(testJpg1, mockFolder);
        verify(mockFolder).setJpg(testJpg1);
        ir.distinquishJpg(testJpgThumb1, mockFolder);
        verify(mockFolder).setJpgThumbs(testJpgThumb1);

    }
}
