package fi.helsinki.it.skannerz.service;

import com.google.zxing.ReaderException;
import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.utils.BarcodeReader;
import fi.helsinki.it.skannerz.utils.IdResolver;
import fi.helsinki.it.skannerz.utils.MetadataUtils;
import fi.luomus.kuvapalvelu.client.ImageApiClient;
import fi.luomus.utils.exceptions.ApiException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author jambo
 */
public class PipelineTest {

    private Pipeline pipeline;
    private Folder folder;
    private ExtendedMeta meta;
    private SpecimenData specimenMock;
    private IdResolver resolverMock;
    private ImageApiClient imageClientMock;
    private Image imageMock;
    List<Image> list;

    public PipelineTest() {

    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        Configuration.init("local.test.properties");
    }

    @Before
    public void setUp() throws ApiException, ReaderException, IOException {
        folder = mock(Folder.class);
        meta = new ExtendedMeta();
        meta.setDomain("http://domain.com");
        when(folder.getName()).thenReturn("testMockFolder");
        list = new ArrayList<>();
        
        imageMock = mock(Image.class);
        specimenMock = mock(SpecimenData.class);
        when(specimenMock.getFolder()).thenReturn(folder);
        when(imageMock.getMeta()).thenReturn(meta);
        list.add(imageMock);
        when(folder.getTifImages()).thenReturn(new HashSet<Image>(list));
        when(specimenMock.getImages()).thenReturn(new HashSet<Image>(list));
        // these settings are for testSetTimeStampToMetadata
        // check that method setTimeStampToMetadata returns DateTime object with correct value
        when(folder.getPath()).thenReturn("dc1.2017-09-01_11-06-38_7bbdc5");
        when(imageMock.getParentFolder()).thenReturn(new Folder(PipelineTest.class.getClassLoader().getResource("testRootFolder/dc1.2017-09-01_11-06-38_7bbdc5").getPath()));

        imageClientMock = mock(ImageApiClient.class);
        when(imageClientMock.upload(any(), any())).thenReturn("222");

        resolverMock = mock(IdResolver.class);
        when(resolverMock.getIdentifier(any())).thenReturn("111");
        pipeline = new Pipeline(specimenMock, imageClientMock, resolverMock);

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetTimeStampToMetadataFunctionsAsExpected() {
        DateTime expectedDateTime = new DateTime(2017, 9, 1, 11, 6, 38);
        when(folder.getPath()).thenReturn("dc1.2017-09-01_11-06-38_7bbdc5");
        Pipeline.setTimeStampToMetadata(meta, folder, null);
        assertEquals(expectedDateTime, meta.getCaptureDateTime());
    }

//    @Test FIX THIS
//    public void testSetTimeStampToMetadataFunctionsAsExpectedWhenFolderNameNotIncludeTime() throws Exception {
//        Folder folder = new Folder(this.getClass().getResource("/testRootFolder/qr").getFile());
//        BasicFileAttributes attr = Files.readAttributes(folder.toPath(), BasicFileAttributes.class);
//        FileTime fileTime = attr.creationTime();
//        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ");
//        DateTime expectedDateTime = df.parseDateTime(fileTime.toString());
//        Pipeline.setTimeStampToMetadata(meta, folder, null);
//        assertEquals(expectedDateTime, meta.getCaptureDateTime());
//    }
    @Test
    public void folderNameDoesNotFollowRegex() throws Exception {
        when(folder.getPath()).thenReturn("dc1.2017-0999999-06-38_7bbdc5");
        pipeline.setTimeStampToMetadata(meta, folder, null);
        assertNotNull(meta.getCaptureDateTime());
    }

    @Test
    public void testGetSpecimenUriFromQrWithOutDomain() throws Exception {
        IdResolver resolverMock = mock(BarcodeReader.class);
        when(resolverMock.getIdentifier(new URI(""))).thenReturn("beast666");
        Image tifFile = mock(Image.class);
        when(tifFile.toURI()).thenReturn(new URI(""));
        when(tifFile.getMeta()).thenReturn(meta);
        String state = Pipeline.getSpecimenUriFromQr(tifFile, resolverMock, null);
        assertNotNull(state);
        assertEquals("label", meta.getTags().get(0));
    }

    @Test
    public void testGetSpecimenUriFromQrWithDomain() throws Exception {
        IdResolver resolverMock = mock(BarcodeReader.class);
        when(resolverMock.getIdentifier(new URI(""))).thenReturn("http://different.do/beast666");
        Image tifFile = mock(Image.class);
        when(tifFile.toURI()).thenReturn(new URI(""));
        when(tifFile.getMeta()).thenReturn(meta);
        String state = Pipeline.getSpecimenUriFromQr(tifFile, resolverMock, null);
        assertNotNull(state);
        assertEquals("label", meta.getTags().get(0));
    }

    @Test
    public void testCallKuvapalveluClient() throws ApiException {
        //Meta meta, Set<File> tifImages , ImageApiClient imageApiClient
//        Meta metaMock = mock(Meta.class);
//        Image imageMock = mock(Image.class);
//        when(imageMock.getMetadataObject()).thenReturn(meta);
        ImageApiClient imageClientMock = mock(ImageApiClient.class);

        Pipeline.callKuvapalveluClient(imageMock, imageClientMock, null);
        verify(imageClientMock).upload(imageMock, MetadataUtils.convertMeta(meta));
    }

    @Test
    public void testCall() {
        meta.setDomain("domain");
        assertEquals(pipeline.call(), PipelineReturnCode.OK);
        //Check that the QR skip also works (if the QR was inserted manually or so.
        assertEquals(PipelineReturnCode.OK,pipeline.call());

    }

    @Test(expected = NullPointerException.class)
    public void testCallFailed() {
        Pipeline pipeline = new Pipeline(null, null, null);
        assertEquals(pipeline.call(), PipelineReturnCode.OK);
    }

    @Test
    public void testSetCoordinatesToMetadataSetsNotNull() {
        pipeline.setCoordinatesToMetadata(meta);
        assertNotNull(meta.getWgs84Coordinates());
    }

    @Test
    public void testSetSourceSystemToMetadata() {
        String expected = Configuration.getUsername();
        pipeline.setSourceSystemToMetadata(meta);
        assertEquals(expected, meta.getSourceSystem());
    }

    @Test
    public void pipelineCreatesMetaDataFilesForAllImages() {
        folder = new Folder("testRootFolder/dc1.2017-09-01_11-06-38_7bbdc5/");
        pipeline.call();
        Set<Image> tifImages = folder.getTifImages();
        for (Image image  : tifImages) {
            assertTrue(image.hasMetaFile());
        }
    }

}
