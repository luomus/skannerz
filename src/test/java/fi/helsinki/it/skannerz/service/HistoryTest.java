/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.Folder;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import static org.junit.Assert.assertTrue;
import org.junit.*;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import org.slf4j.LoggerFactory;

/**
 *
 * @author porttpet
 */
public class HistoryTest {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(HistoryTest.class);
    String rootFolder = "/testRootFolder";
    String rootFolderPath = this.getClass().getResource(rootFolder).getFile();
    History history = new History();

    @Before
    public void setUp() {
        history.setPath(rootFolderPath);
        history.cleanHistory();
    }

    @After
    public void tearDown() {
        File f = new File(rootFolderPath + "/history");
        while (f.exists()) {
            f.delete();
        }
        File hist = new File(rootFolder + "/history");
        hist.delete();
    }

    public HistoryTest() {
    }

    @Test
    public void setAndGetPath() {
        history.setPath("test");
        assertTrue(history.getPath().equals("test"));
    }

    @Test
    public void addAndGetFolder() {
        Folder f = new Folder("TestFolder");
        Folder f2 = new Folder("TestFolder2");
        history.addFolderToHistory(f, f2);

        ArrayList historyListFull = history.getHistoryListFull();
        assertTrue(historyListFull.get(0).equals(f) && historyListFull.get(1).equals(f2));
    }

    @Test
    public void removeAndCleanFolder() {
        Folder f = new Folder("TestFolder");
        Folder f2 = new Folder("TestFolder2");
        history.addFolderToHistory(f, f2);
        assertTrue(history.getHistoryListFull().size() == 2);
        history.removeFolderFromHistory(f);
        assertTrue(history.getHistoryListFull().size() == 1);
        assertTrue(history.getHistoryListFull().get(0).equals(f2));
        history.cleanHistory();
        assertTrue(history.getHistoryListFull().isEmpty());
    }

    @Test
    public void saveAndLoadHistory() {
        try {
            Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
            Folder fcopy = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5_copy"));
            fcopy.deleteOnExit();
            FileUtils.copyDirectory(f, fcopy);

            history.addFolderToHistory(f);

            history.saveHistoryToFile();
            history.cleanHistory();
            history.loadHistoryFromFile();
            assertTrue(!history.getHistoryListFull().isEmpty());
        } catch (IOException ex) {
            Logger.getLogger(HistoryTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testAppendToHistory() {
        try {
            Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
            Folder fcopy = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5_copy"));
            Folder fcopy2 = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5_copy2"));
            fcopy.deleteOnExit();
            fcopy2.deleteOnExit();
            FileUtils.copyDirectory(f, fcopy);
            FileUtils.copyDirectory(f, fcopy2);

            history.addFolderToHistory(fcopy);
            assertTrue(history.getHistoryListFull().size() == 1);

            history.addFolderToHistory(fcopy2);
            assertTrue(history.getHistoryListFull().size() == 2);
        } catch (IOException ex) {
            Logger.getLogger(HistoryTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testHistorySaveToFileNoExist() {
        try {
            File hist = new File(rootFolder + "/history");
            hist.delete();
            Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
            Folder fcopy = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5_copy"));
            fcopy.deleteOnExit();

            FileUtils.copyDirectory(f, fcopy);

            history.addFolderToHistory(fcopy);
            assertTrue(history.getHistoryListFull().size() == 1);
        } catch (IOException ex) {
            Logger.getLogger(HistoryTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testHistoryLoadFromFileNoExist() {
        File hist = new File(rootFolder + "/history");
        hist.delete();
        history.loadHistoryFromFile();
    }

    @Test
    public void testSendToMonitor() {
        Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
        Folder f2 = new Folder(new File(rootFolder + "/ddc1.2017-09-01_11-03-48_b8e085"));
        history.addFolderToHistory(f);
        history.addFolderToHistory(f2);
        history.sendToMonitor();
    }

    @Test
    public void testSendToMonitorMock() {
        History historyMock = mock(History.class);
        Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
        Folder f2 = new Folder(new File(rootFolder + "/ddc1.2017-09-01_11-03-48_b8e085"));
        historyMock.addFolderToHistory(f);
        historyMock.addFolderToHistory(f2);
        historyMock.sendToMonitor();
        verify(historyMock).sendToMonitor();
    }

    @Test
    public void testMerge() {
        File hist = new File(rootFolder + "/history");
        hist.delete();
        Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
        Folder f2 = new Folder(new File(rootFolder + "/ddc1.2017-09-01_11-03-48_b8e085"));
        history.addFolderToHistory(f);
        history.saveHistoryToFile();
        history.cleanHistory();
        history.addFolderToHistory(f2);
        history.saveHistoryToFile();
        history.addFolderToHistory(f2);
        history.saveHistoryToFile();
        history.sendToMonitor();
    }

    @Test
    public void testFailToSave() {
        File hist = new File(rootFolder + "/history");
        history.setPath(null);
        Folder f = new Folder(new File(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5"));
        history.addFolderToHistory(f);
        history.saveHistoryToFile();

    }
}
