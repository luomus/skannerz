package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.config.PropertiesLoader;
import fi.helsinki.it.skannerz.domain.Folder;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.LoggerFactory;

/**
 *
 * @author porttpet
 */
public class RootFolderReaderV2Test {

    private RootFolderReaderV2 reader;
    private final String rootFolder = "/testRootFolder";
    private final String rootFolderPath = this.getClass().getResource(rootFolder).getFile();
    private final String subfolder2Path = this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5").getFile();
    private final String subfolder1Path = this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-03-48_b8e085").getFile();
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(RootFolderReaderV2Test.class);

    public RootFolderReaderV2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        try {
            reader = new RootFolderReaderV2(rootFolderPath, new ImageFolderReaderStub());
        } catch (IOException ex) {
            System.out.println(ex);
        }
        try {
            removeFiles();
        } catch (InterruptedException ex) {
            Logger.getLogger(RootFolderReaderV2Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @After
    public void tearDown() {
        try {
            removeFiles();
        } catch (InterruptedException ex) {
            Logger.getLogger(RootFolderReaderV2Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void removeFiles() throws InterruptedException {
        File f = new File(rootFolderPath + "/testikansio");
        File f2 = new File(rootFolderPath + "/testikansio2");
        while (f.exists()) {
            if (f.isDirectory() && f.listFiles().length > 0) {
                for (File file : f.listFiles()) {
                    System.out.println(file.getPath());
                    file.delete();
                }
            }
            f.delete();
        }
        while (f2.exists()) {
            if (f.isDirectory() && f.listFiles().length > 0) {
                for (File file : f.listFiles()) {
                    file.delete();
                }
            }
            f2.delete();
        }
    }

    /**
     * Test of returnNextSample method, of class Reader.
     */
    @Test
    public void testReturnNextFolder() {
        Folder one = reader.returnNextFolder();
        Folder two = reader.returnNextFolder();
        assertEquals(one, new Folder(subfolder1Path));
        assertEquals(two, new Folder(subfolder2Path));
    }

    @Test
    public void testReturnNextFolderWhenThereIsNone() {
        reader.returnNextFolder();
        reader.returnNextFolder();
        reader.returnNextFolder();
        reader.returnNextFolder();
        reader.returnNextFolder();
        reader.returnNextFolder();
        reader.returnNextFolder();
        Folder returnNextFolder = reader.returnNextFolder();
        assertNull(returnNextFolder);
    }

    @Test(timeout = 10000)
    public void testReturnFolders() {
        ArrayList<Folder> folderList = reader.returnRestFoldersAsList();
        assertEquals(3, folderList.size());
    }

    @Test(timeout = 15000)
    public void testFileSystemReaderLoop() throws IOException, URISyntaxException, InterruptedException, NoSuchFieldException {
        Configuration.init(new RootFolderReaderV2Test.TestPropertiesLoader(), "default", "local");
        Thread readerThread = new Thread(reader, "RootFolderReaderV2Test-RFRThread");
        readerThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(RootFolderReaderV2Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        FileCreatorStub fcs = new FileCreatorStub(this.getClass().getResource(rootFolder).getFile() + "/testikansio");
        Thread th = new Thread(fcs, "RootFolderReaderV2Test-FileCreatorStub");
        System.out.println("reader:" + reader.returnNextFolder());
        System.out.println("reader:" + reader.returnNextFolder());
        System.out.println("reader:" + reader.returnNextFolder());
        Thread.sleep(1000);
        th.start();
        Folder testfolder = null;

        while (testfolder == null) {
            testfolder = reader.returnNextFolder();
        }
        assertEquals("testikansio", testfolder.getName());

        readerThread.interrupt();
    }

    public class ImageFolderReaderStub implements ImageFolderReaderInterface {

        @Override
        public Folder createFolderObject(File path) {
            return new FolderStub(path.getPath());
        }

    }

    public class FolderStub extends Folder {

        public FolderStub(String folderPath) {
            super(folderPath);
        }

        @Override
        public boolean isValid() {
            return true;
        }

    }

    public class FileCreatorStub implements Runnable {

        private final String path;

        public FileCreatorStub(String path) {
            this.path = path;
        }

        @Override
        public void run() {
            try {
                File f = new File(path);
                FileUtils.copyDirectory(new File(subfolder1Path), f);
            } catch (IOException ex) {
                Logger.getLogger(RootFolderReaderV2Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private static class TestPropertiesLoader extends PropertiesLoader {

        @Override
        public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
            properties.put("pipelineThreads", "42");
            properties.put("folderNameRegex", "*");
            properties.put("domains", "domain1,domain2");
            properties.put("imageFolderPath", "root");
            properties.put("imageFolderPath.relative", "true");
            properties.put("username", "testuser");
            properties.put("password", "th!s1s4p455w0rd");
            properties.put("imageStorageDbUri", "https://uri.net/api");
            properties.put("keywords", "keyword1,keyword2");
            properties.put("intellectualRights.api.accessToken", "token_token");
            properties.put("intellectualRights.api.url", "http://doesntmatter.com");
            properties.put("intellectualRights.defaultValue", "testIntellectualRight");
            properties.put("history.maxDisplayedRows", "1250");
            properties.put("api.laji.fi.cache.location", "/tmp/api_laji_fi_cache/");
            properties.put("label.MM.person.url", "http://doesntmatter.com");
            properties.put("label.MM.image.url", "http://doesntmatter.com");
            properties.put("defaultLanguage", "en");
            properties.put("defaultWaitTimeForFolder", "3000");
        }
    }
}
