package fi.helsinki.it.skannerz.domain;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author porttpet
 */
public class FolderTest {

    String rootFolder, testFolderPath;

    String subfolder1;
    String subfolder2;
    Folder modelFolder;
    File testJpg2;
    File testJpg1;
    File testAscii;
    File testJpgThumb1;
    File testJpgThumb2;
    File testXml;
    File testTif1;
    File testTif2;

    public FolderTest() {
        this.rootFolder = "/testRootFolder";
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        testFolderPath = this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5").getFile();
        testAscii = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/digitisation.properties").getFile());

        testJpg1 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Preview001.jpg").getFile());
        testJpg2 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Preview002.jpg").getFile());

        testJpgThumb1 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Thumb001.jpg").getFile());
        testJpgThumb2 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Thumb002.jpg").getFile());

        testXml = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/dc1.2017-09-01_11-06-38_7bbdc5.xml").getFile());

        testTif1 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Image001.tif").getFile());
        testTif2 = new File(this.getClass().getResource(rootFolder + "/dc1.2017-09-01_11-06-38_7bbdc5/Image002.tif").getFile());
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPath method, of class Folder.
     */
    @Test
    public void testGetFolderPath() {
        System.out.println("getFolderPath");
        Folder instance = new Folder("Path");
        String expResult = "Path";
        String result = instance.getPath();
        assertNotNull(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of setXmlFile method, of class Folder.
     */
    @Test
    public void testSetGetXmlFile() {
        System.out.println("XmlFile");
        Folder instance = new Folder("Path");
        File file1;
        file1 = testXml;
        instance.setXmlFile(file1);
        assertNotNull(instance.getXml());
        assertEquals(file1, instance.getXml());
    }

    /**
     * Test of getAscii method, of class Folder.
     */
    @Test
    public void testSetGetAscii() {
        System.out.println("AsciiFile");
        Folder instance = new Folder("Path");
        File file1;
        file1 = testAscii;

        instance.setAscii(file1);
        assertEquals(file1, instance.getAscii());
    }

    /**
     * Test of getJpg method, of class Folder.
     */
    @Test
    public void testSetGetJpg() {
        System.out.println("JpgFile");
        Folder instance = new Folder("Path");
        File file1, file2;
        file1 = testJpg1;
        file2 = testJpg2;
        instance.setJpg(file1, file2);
        assertTrue(instance.getJpg().contains(file1));
        assertTrue(instance.getJpg().contains(file2));
    }

    /**
     * Test of getJpgThumbs method, of class Folder.
     */
    @Test
    public void testSetGetJpgThumbs() {
        System.out.println("JpgThumbsFile");
        Folder instance = new Folder("Path");
        File file1, file2;
        file1 = testJpgThumb1;
        file2 = testJpgThumb1;
        instance.setJpgThumbs(file1, file2);
        assertTrue(instance.getJpgThumbs().contains(file1));
        assertTrue(instance.getJpgThumbs().contains(file2));
    }

    /**
     * Test of getTifImages method, of class Folder.
     */
    @Test
    public void testSetGetTifImages() {
        System.out.println("TifFile");
        Folder folder = new Folder("Path");
        File file1, file2;
        file1 = testTif1;
        file2 = testTif2;
        folder.addTifImages(file1, file2);
        System.out.println(folder.getTifImages());
        assertTrue(folder.getTifImages().contains(file1));
        assertTrue(folder.getTifImages().contains(file2));
    }

    /**
     * Test of equals method, of class Folder.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Folder instance = new Folder("test");
        Folder instance2 = new Folder("test2");
        boolean expResult = false;
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of setProcessed method, of class Folder.
     */
    @Test
    public void testSetProcessed() throws Exception {
        Folder testFolder = new Folder(testFolderPath);
        File testProcessedFile = new File(testFolderPath + "/processed");
        testFolder.setProcessed(Boolean.TRUE);
        assertTrue(testProcessedFile.exists());
        testFolder.setProcessed(Boolean.FALSE);
        assertFalse(testProcessedFile.exists());
    }

    @Test
    public void testGetProcessed() throws Exception {
        Folder testFolder = new Folder(testFolderPath);
        assertFalse(testFolder.getProcessed());
        testFolder.setProcessed(Boolean.TRUE);
        assertTrue(testFolder.getProcessed());
        testFolder.setProcessed(Boolean.FALSE);
    }

}
