package fi.helsinki.it.skannerz.webframework;

import fi.helsinki.it.skannerz.Skannerz;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.LoggerFactory;

/**
 * @author porttpet
 */
public class SeleniumTests {

    private static WebDriver driver;
    private final String url = "http://localhost:8090/form";
    private static Skannerz skannerz;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SeleniumTests.class);

    private WebElement intellectualOwner;
    private List<WebElement> intellectualRights;
    private WebElement keywordsText;
    private List<WebElement> pubRest;
    private WebElement reset;
    private List<WebElement> domain;
    private WebElement person;
    private WebElement submit;
    private WebElement keywordsButton;

    /**
     * Required to start the local server for the selenium tests to run in.
     */
    @BeforeClass
    public static void setupClass() {
        skannerz = new Skannerz();
        skannerz.init();
    }

    /**
     * start the selenium test driver. the argument true is necessary for
     * javascript support.
     */
    @Before
    public void setupTest() {
        driver = new HtmlUnitDriver(false);
        driver.get(url);
        //System.setProperty("webdriver.chrome.driver", "/home/porttpet/Downloads/chromedriver");
        //driver = new ChromeDriver();

        intellectualOwner = driver.findElement(By.name("intellectualOwner"));
        intellectualRights = driver.findElements(By.xpath("//*[@name=\"intellectualRights\"]/*"));
        keywordsText = driver.findElement(By.id("keywords-text"));
        pubRest = driver.findElements(By.xpath("//*[@name=\"publicityRestrictions\"]/*"));
        reset = driver.findElement(By.name("resetButton"));
        domain = driver.findElements(By.xpath("//*[@name=\"domain\"]/*"));
        person = driver.findElement(By.name("person"));
        submit = driver.findElement(By.name("submitButton"));
        keywordsButton = driver.findElement(By.id("keywords-button"));
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();

        }
    }

    @AfterClass
    public static void teardownclass() {
        try {
            skannerz.stopManager();
            skannerz.destroy();
            driver.close();
        } catch (Exception ex) {
            System.out.println("tore down selenium tests");
        }

    }

    /**
     * Tests that the selenium driver is actually running properly.
     */
    @Test
    public void testGetPageTitle() {
        String title = driver.getTitle();
        assertEquals("Skannerz", title);
    }

    private void addKeyword(String keywordValue) {
        keywordsText.sendKeys(keywordValue);
        keywordsButton.click();
    }

    @Test
    public void testFillFields() {
        intellectualOwner.sendKeys("testOwner");
        intellectualRights.get(1).click();
        keywordsText.sendKeys("testKeywords");
        pubRest.get(2).click();

        assertEquals("testOwner", intellectualOwner.getAttribute("value"));
        assertTrue(intellectualRights.get(1).isSelected() && !intellectualRights.get(0).isSelected() && !intellectualRights.get(2).isSelected());
        assertTrue(keywordsText.getAttribute("value").contains("testKeywords"));
        assertTrue(pubRest.get(2).isSelected() && !pubRest.get(0).isSelected() && !pubRest.get(1).isSelected());

    }

    @Test
    public void testSubmitNull() {
        submit.click();
        assertEquals(url, driver.getCurrentUrl());

        WebElement error = driver.findElement(By.name("messageField"));
        assertEquals("person () should be at least 1 characters long.", error.getText());
    }

    @Test
    public void testSubmit() {
        domain.get(1).click();
        intellectualOwner.sendKeys("testOwner");
        person.sendKeys("personTest");
        intellectualRights.get(1).click();
        addKeyword("testKeywords");
        pubRest.get(2).click();
        submit.click();

        WebElement error = driver.findElement(By.name("messageField"));
        System.out.println(error.getText());
        assertEquals("Form sent succesfully!", error.getText());
    }

    @Test
    public void testMissingPerson() {
        intellectualOwner.clear();
        intellectualRights.get(1).click();
        addKeyword("testKeywords");
        pubRest.get(2).click();
        submit.click();

        WebElement error = driver.findElement(By.name("messageField"));
        assertEquals("person () should be at least 1 characters long.", error.getText());
    }

    @Test
    public void testGetMonitor() {
        testSubmit();
        assertTrue(driver.getCurrentUrl().contains("monitor"));
        //System.out.println(driver.getCurrentUrl());
    }

    //Will fail on HtmlUnitDriver, but will work on ChromeDriver instead.
//    @Test
    public void testBackToFormFromMonitor() throws InterruptedException {
        testSubmit();
        driver.get(url);
        WebElement formButton = driver.findElement(By.name("formButton"));
        formButton.click();
        //LOG.info("Testback :"+driver.getCurrentUrl());
        assertTrue(driver.getCurrentUrl().contains("lomake"));
    }

    //Will fail on HtmlUnitDriver, but will work on ChromeDriver instead.
//    @Test
    public void testAbortMonitor() {
        testSubmit();
        driver.get(url);
        WebElement formButton = driver.findElement(By.name("abortButton"));

        formButton.click();
        WebElement messageField = driver.findElement(By.name("messageField"));
        assertTrue(messageField.getText().equals("Process has been aborted!"));
    }
}
