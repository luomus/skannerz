package fi.helsinki.it.skannerz.webframework.validation;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by riku on 6.10.2017.
 */
public class StringValidatorTest {

    @Test
    public void nothingSpecified() {
        StringValidator validator = new StringValidator.Builder().build();
        String invalid = null;
        String valid = "";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("invalid", invalid, msgContainer));
        assertTrue(validator.validate("valid", valid, msgContainer));

    }

    @Test
    public void onlyMinimumLengthSpecified() {
        StringValidator validator = new StringValidator.Builder().minimumLength(2).build();
        String wayTooShort = null;
        String tooShort = "a";
        String valid = "aa";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("wayTooShort", wayTooShort, msgContainer));
        assertFalse(validator.validate("tooShort", tooShort, msgContainer));
        assertTrue(validator.validate("valid", valid, msgContainer));

    }

    @Test
    public void minimumAndMaximumLengthSpecified() {
        StringValidator validator = new StringValidator.Builder().minimumLength(2).maximumLength(5).build();

        String wayTooShort = null;
        String tooShort = "a";
        String valid = "aaa";
        String tooLong = "aaaaaa";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("wayTooShort", wayTooShort, msgContainer));
        assertFalse(validator.validate("tooShort", tooShort, msgContainer));
        assertTrue(validator.validate("valid", valid, msgContainer));
        assertFalse(validator.validate("tooLong", tooLong, msgContainer));

    }

    @Test
    public void matchesRegularExpressions() {
        Pattern p = Pattern.compile("aa+");
        StringValidator validator = new StringValidator.Builder().pattern(p).build();

        String noMatch = null;
        String noMatch1 = "a";
        String match = "aaa";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("noMatch", noMatch, msgContainer));
        assertFalse(validator.validate("noMatch1", noMatch1, msgContainer));
        assertTrue(validator.validate("match", match, msgContainer));

    }

    @Test
    public void minMaxAndRegExpSpecified() {
        Pattern p = Pattern.compile("aa+");
        StringValidator validator = new StringValidator.Builder().minimumLength(2).maximumLength(5).pattern(p).build();

        String noMatch = null;
        String tooShort = "a";
        String match = "aaa";
        String tooLong = "aaaaaa";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("noMatch", noMatch, msgContainer));
        assertFalse(validator.validate("tooShort", tooShort, msgContainer));
        assertTrue(validator.validate("match", match, msgContainer));
        assertFalse(validator.validate("tooLong", tooLong, msgContainer));

    }

}
