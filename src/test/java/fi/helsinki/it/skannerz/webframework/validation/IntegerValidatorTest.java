package fi.helsinki.it.skannerz.webframework.validation;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by riku on 6.10.2017.
 */
public class IntegerValidatorTest {

    @Test
    public void nonParseableParameter() {
        ParameterValidator validator = new IntegerValidator.Builder().build();
        String notInt = "definitely not an integer.";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("int", notInt, msgContainer));
    }

    @Test
    public void lowerBoundSpecified() {
        ParameterValidator validator = new IntegerValidator.Builder().min(2).build();

        String wayTooSmall = null;
        String tooSmall = "0";
        String ok = "2";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("wayTooSmall", wayTooSmall, msgContainer));
        assertFalse(validator.validate("tooSmall", tooSmall, msgContainer));
        assertTrue(validator.validate("ok", ok, msgContainer));

    }

    @Test
    public void upperBoundSpecified() {
        ParameterValidator validator = new IntegerValidator.Builder().max(10).build();

        String wayTooSmall = null;
        String tooBig = "77";
        String ok = "9";

        List<String> msgContainer = new ArrayList<>();
        assertFalse(validator.validate("wayTooSmall", wayTooSmall, msgContainer));
        assertFalse(validator.validate("tooBig", tooBig, msgContainer));
        assertTrue(validator.validate("ok", ok, msgContainer));
    }
}
