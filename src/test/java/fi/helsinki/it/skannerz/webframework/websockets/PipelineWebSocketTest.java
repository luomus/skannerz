package fi.helsinki.it.skannerz.webframework.websockets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.service.Mailbox;
import fi.helsinki.it.skannerz.service.MailboxInterface;
import fi.helsinki.it.skannerz.utils.gson.DateTimeSerializer;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import org.joda.time.DateTime;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author riku
 */
public class PipelineWebSocketTest {

    private Session session;
    private EndpointConfig endpointConfig;
    private PipelineWebSocket pipelineWS;
    private MailboxInterface<PipelineMessage> mailbox;

    @Before
    public void setUp() {
        session = mock(Session.class);
        endpointConfig = mock(EndpointConfig.class);
        mailbox = new Mailbox<>();
        pipelineWS = new PipelineWebSocket();
        pipelineWS.setMailbox(mailbox);
    }

    @Test
    public void websocketClientIsAddedToMailboxOnOpen() {
        pipelineWS.onOpen(session, endpointConfig);
        assertTrue(mailbox.getReceivers().contains(pipelineWS));
    }

    @Test
    public void websocketClientIsRemovedFromMailboxReceivers() {
        pipelineWS.onClose(session, mock(CloseReason.class));
        assertTrue(!mailbox.getReceivers().contains(pipelineWS));
    }

    @Test
    public void messageIsSentWithDoneFuture() throws Exception {
        Future<PipelineReturnCode> mockFuture = mock(Future.class);
        when(mockFuture.get()).thenReturn(PipelineReturnCode.OK);
        when(mockFuture.isDone()).thenReturn(Boolean.TRUE);

        File imageFile = new File("removeMe.tif");
        Folder mockFolder = mock(Folder.class);
        Image mockImage = mock(Image.class);
        when(mockImage.getName()).thenReturn("removeMe.tif");
        when(mockFolder.getName()).thenReturn("i am a folder");
        SpecimenData specimenData = mock(SpecimenData.class);
        when(specimenData.getFolder()).thenReturn(mockFolder);
        ExtendedMeta meta = new ExtendedMeta();
        meta.setDomain("http://domain");
        meta.setDocumentIds(Arrays.asList(new String[]{"doc id"}));
        when(mockImage.getMeta()).thenReturn(meta);
        Set<Image> images = new HashSet<>();
        images.add(mockImage);
        when(specimenData.getImages()).thenReturn(images);

        PipelineMessage msg = new PipelineMessage(mockFuture, specimenData);

        String metaJson = "{\"removeMe.tif\":" + buildGson().toJson(meta) + '}';

        StringBuilder json = new StringBuilder();
        json.append("{");
        json.append("\"id\":\"").append("doc id").append("\"");
        json.append(",\"statusCode\":\"").append(PipelineReturnCode.OK.name()).append("\"");
        json.append(",\"status\":\"").append(PipelineReturnCode.OK.getMessage()).append("\"");
        json.append(",\"folder\":\"").append("i am a folder").append("\"");
        json.append(",\"meta\":").append(metaJson);
        json.append("}");

        RemoteEndpoint.Basic mockRemoteEndPoint = mock(RemoteEndpoint.Basic.class);
        when(session.getBasicRemote()).thenReturn(mockRemoteEndPoint);
        when(session.isOpen()).thenReturn(Boolean.TRUE);

        pipelineWS.onOpen(session, endpointConfig);
        pipelineWS.handleMail(msg);

        verify(session.getBasicRemote()).sendText(json.toString());
    }

    @Test
    public void testOnMessageUpdateStatuses() {
        MailboxInterface mockMailbox = mock(MailboxInterface.class);
        pipelineWS.setMailbox(mockMailbox);
        String testJson = "{\"action\":\"STATUS_UPDATE\"}";
        pipelineWS.onMessage(testJson);
        verify(mockMailbox).handleMail(pipelineWS);
    }

    private static Gson buildGson() {
        return new GsonBuilder()
            .registerTypeAdapter(
                DateTime.class,
                new DateTimeSerializer())
            .create();
    }
}
