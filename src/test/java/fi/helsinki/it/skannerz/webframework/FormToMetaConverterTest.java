package fi.helsinki.it.skannerz.webframework;

import fi.luomus.kuvapalvelu.client.model.Meta;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import spark.QueryParamsMap;

/**
 *
 * @author waxwax
 */
public class FormToMetaConverterTest {

    private QueryParamsMap queryParamsMap;

    public FormToMetaConverterTest() {
    }

    @Before
    public void setUp() {
        queryParamsMap = createDefaultParamsMap();
    }

    @Test
    public void testConvertDefaultMapSucceeds() {
        System.out.println("testConvertDefaultMapSucceeds");
        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        assertNotNull("meta is null!", meta);
    }

    @Test
    public void testCheckOwner() {
        System.out.println("testCheckOwner");
        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        assertEquals("ownerNameHere", meta.getRightsOwner());
    }

    @Test
    public void testCheckRights() {
        System.out.println("testCheckRights");
        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        assertEquals("your rights", meta.getLicense());
    }

    @Test
    public void testCheckPerson() {
        System.out.println("testCheckPerson");
        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        assertEquals("mirjameik", meta.getUploadedBy());
    }
    
    @Test
    public void testCheckSecret() {
        System.out.println("testCheckPerson");
        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        assertTrue(meta.getSecret());
    }

    @Test
    public void testCheckTags() {
        System.out.println("testCheckTags");
        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        List<String> expectedList = new ArrayList<>();
        expectedList.add("test1");
        expectedList.add("test2");
        expectedList.add("test3");
        expectedList.add("test4");
        expectedList.add("test5");
        expectedList.add("test6");
        expectedList.add("test7");
        assertEquals(expectedList, meta.getTags());
    }

    @Test
    public void testCheckEmptyTags() {
        System.out.println("testCheckEmptyTags");

        QueryParamsMap keywordsMapMock = mock(QueryParamsMap.class);
        when(keywordsMapMock.value()).thenReturn("");
        when(queryParamsMap.get("keywords")).thenReturn(keywordsMapMock);

        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        List<String> expectedList = new ArrayList<>();
        assertEquals(expectedList, meta.getTags());
    }

    @Test
    public void testCheckTagsWithWeirdSymbols() {
        System.out.println("testCheckWeirdSymbols");

        QueryParamsMap keywordsMapMock = mock(QueryParamsMap.class);
        when(keywordsMapMock.value()).thenReturn(" %(/\"&%!¤&=)(\"!/#)= 17.23-6:0_7632&#/!;,\n\t");
        when(queryParamsMap.get("keywords")).thenReturn(keywordsMapMock);

        FormToMetaConverter instance = new FormToMetaConverter();
        Meta meta = instance.convert(queryParamsMap);
        List<String> expectedList = new ArrayList<>();
        expectedList.add("%(/\"&%!¤&=)(\"!/#)= 17.23-6:0_7632&#/!");
        assertEquals(expectedList, meta.getTags());
    }

    private QueryParamsMap createDefaultParamsMap() {
        QueryParamsMap intellectualOwnerMapMock = mock(QueryParamsMap.class);
        when(intellectualOwnerMapMock.value()).thenReturn("ownerNameHere ");
        QueryParamsMap intellectualRightsMapMock = mock(QueryParamsMap.class);
        when(intellectualRightsMapMock.value()).thenReturn(" your rights\n");
        QueryParamsMap keywordsMapMock = mock(QueryParamsMap.class);
        when(keywordsMapMock.value()).thenReturn("test1\ntest2,test3 , test4 ;test5; test6 \n\n test7");
        QueryParamsMap personMock = mock(QueryParamsMap.class);
        when(personMock.value()).thenReturn("mirjameik");
        QueryParamsMap domainMock = mock(QueryParamsMap.class);
        when(domainMock.value()).thenReturn("domain");
        QueryParamsMap secretMock = mock(QueryParamsMap.class);
        when(secretMock.value()).thenReturn("PRIVATE");

        QueryParamsMap queryParamsMapMock = mock(QueryParamsMap.class);
        when(queryParamsMapMock.get("intellectualOwner")).thenReturn(intellectualOwnerMapMock);
        when(queryParamsMapMock.get("intellectualRights")).thenReturn(intellectualRightsMapMock);
        when(queryParamsMapMock.get("keywords")).thenReturn(keywordsMapMock);
        when(queryParamsMapMock.get("person")).thenReturn(personMock);
        when(queryParamsMapMock.get("domain")).thenReturn(domainMock);
        when(queryParamsMapMock.get("publicityRestrictions")).thenReturn(personMock);

        return queryParamsMapMock;
    }

}
