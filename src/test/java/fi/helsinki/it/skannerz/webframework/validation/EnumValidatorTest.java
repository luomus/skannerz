/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.webframework.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author waxwax
 */
public class EnumValidatorTest {

    public EnumValidatorTest() {
    }

    /**
     * Test of validate method, of class EnumValidator.
     */
    @Test
    public void testValidate() {
        System.out.println("testValidate");
        List<String> enums = Arrays.asList(new String[]{
            "valid",
            "921831",
            ""
        });
        EnumValidator enumValidator = new EnumValidator(enums);
        List<String> msgContainer = new ArrayList<>();
        
        assertTrue(enumValidator.validate("KEYNAME", "valid", msgContainer));
        assertTrue(enumValidator.validate("KEYNAME", "", msgContainer));
        assertTrue(enumValidator.validate("KEYNAME", "921831", msgContainer));
        
        assertFalse(enumValidator.validate("KEYNAME", "lol", msgContainer));
        assertFalse(enumValidator.validate("KEYNAME", "Valid", msgContainer));
        assertFalse(enumValidator.validate("KEYNAME", null, msgContainer));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateNull() {
        System.out.println("testValidateNull");
        new EnumValidator(null);
    }

}
