package fi.helsinki.it.skannerz;

import com.google.zxing.ReaderException;
import com.google.zxing.qrcode.QRCodeReader;
import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.service.ImageFolderReader;
import fi.helsinki.it.skannerz.service.ImageFolderReaderInterface;
import fi.helsinki.it.skannerz.service.RootFolderReaderFactory;
import fi.helsinki.it.skannerz.service.RootFolderReaderInterface;
import fi.helsinki.it.skannerz.utils.BarcodeReader;
import fi.helsinki.it.skannerz.utils.IdResolver;
import fi.luomus.kuvapalvelu.client.model.Meta;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Walking skeleton test.
 *
 * @author Peter
 */
public class FolderAndQrReadIntegrationTest {

    private final String testFolderPath = "dc1.2017-09-01_11-06-38_7bbdc5";
    private final String testFolderPathCopy = "dc1.2017-09-01_11-06-38_7bbdc5_copy";
    private RootFolderReaderInterface rootFolderReader;
    private ImageFolderReaderInterface imageFolderReaderInterface;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FolderAndQrReadIntegrationTest.class);
    private final String testRootFolder = this.getClass().getResource("/testRootFolder").getFile();

    private IdResolver QRReader;
    private Meta meta;

    @BeforeClass
    public static void setUpClass() {
        try {
            Configuration.init("local.test.properties");
        } catch (Exception ex) {
            LOG.debug("WalkingSkeletonTest exception while reading configuration" + ex);
        }
    }

    @Before
    public void setUp() {
        meta = new Meta();
        this.QRReader = new BarcodeReader(new QRCodeReader());
        imageFolderReaderInterface = new ImageFolderReader();
        RootFolderReaderFactory.reset();
        rootFolderReader = RootFolderReaderFactory.get(testRootFolder, imageFolderReaderInterface);
    }

    /**
     * Cleans up after the test, by removing the copied folder and all it's
     * files.
     */
    @After
    public void tearDown() {
        String copyPath = testFolderPath + "_copy";
        File parentFile = new File(URI.create(new File(testRootFolder).toURI() + copyPath));

        if (parentFile.listFiles() != null) {
            for (File file : parentFile.listFiles()) {
                file.delete();
            }
            parentFile.delete();
        } else {
            LOG.debug("Test folder for WalkingSkeletonTest was null, when it shouldn't have been, nothing to delete. Tests should have  failed too.");
        }
    }

//    @Test
    public void test() {
        pretendForm();

        Folder folderObject = handleFolders();
        assertTrue(folderObject.isValid());

        callQR(folderObject.getTifImages());
        
        LOG.info("TIF-kuvia "+folderObject.getTifImages().size()+" Nimi: "+folderObject.getName());
        if (folderObject.getTifImages().size() == 0) {
            return;
        }
        LOG.info(meta.getDocumentIds().get(0));
        assertEquals("http://id.luomus.fi/F.119662", meta.getDocumentIds().get(0));
    }

    /**
     * Starts the threads to scan for folders, and to make the copied folder.
     * Also checks that the copied folder is valid, before passing it on.
     *
     * @return Returns the folder the we copied.
     */
    private Folder handleFolders() {
        List<Folder> folderList = new ArrayList<>();
        FileCreatorStub fileCreatorStub = new FileCreatorStub(new File(testRootFolder).toURI());
        Thread rootFolderReaderThread = new Thread(rootFolderReader,"FolderAndQrReadIntegrationTest-RFRThread");
        rootFolderReaderThread.start();
        Thread fileCreatorStubThread = new Thread(fileCreatorStub,"FolderAndQrReadIntegrationTest-FileCreatorThread");
        fileCreatorStubThread.start();
        File copyfile = new File(testRootFolder + "/" + testFolderPathCopy);
        int retries = 8;
        try {
            while (!copyfile.exists() && retries > 0) {
                Thread.sleep(500);
                retries -= 1;
            }
            if (retries == 0) {
                throw new IllegalStateException("File " + copyfile.getPath() + " did not exists after 3 seconds");
            }
        } catch (InterruptedException ex) {
            LOG.debug("Interrupted while waiting for walking skeleton filecreator stub to finish" + ex);
        }
        // watcher might be so slow depending of the implementation, that we really need to wait here enormously long
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            LOG.debug("Interrupted while waiting" + ex);
        }
        Folder f = rootFolderReader.returnNextFolder();
        while(!f.getPath().contains(testFolderPathCopy)){
            f = rootFolderReader.returnNextFolder();
        }
        return f;
    }

    /**
     * Sets the Metadata.
     *
     * @param owner
     * @param license
     * @param tags
     * @param uploadedby
     */
    private void setMeta(String owner, String license, ArrayList<String> tags, String uploadedby) {
        meta.setRightsOwner(owner);
        meta.setLicense(license);
        meta.setTags(tags);
        meta.setUploadedBy(uploadedby);
    }

    /**
     * calls the QR reader.
     *
     * @param files the files to scan.
     */
    private void callQR(Iterable<Image> files) {
        for (File file : files) {
            try {
                LOG.info("Reading QR from tif "+file.getPath());
                String identifier = QRReader.getIdentifier(file.toURI());
                meta.getDocumentIds().add(identifier);
            } catch (ReaderException | IOException ex) {
                LOG.info("QR not found exception: " + ex);
            }
        }
    }

    /**
     * Simulate the form input by manually adding the meta. This is done like
     * this, because trying to setup the form in multiple tests in succession
     * caused exceptions.
     */
    private void pretendForm() {
        ArrayList<String> keyw = new ArrayList<>();
        keyw.add("keyword");
        setMeta("testOwner", "License", keyw, "personTest");
    }

    public class FileCreatorStub extends Thread implements Runnable {

        private final String path = testFolderPath;
        private final File directory1;
        private final File directory2;
        private final URI firstUri;
        private final URI secondUri;

        public FileCreatorStub(URI testPath) {
            firstUri = URI.create(testPath.toString() + path);
            secondUri = URI.create(testPath.toString() + path + "_copy");
            directory1 = new File(firstUri);
            directory2 = new File(secondUri);
        }

        @Override
        public void run() {
            LOG.info("Copying directory...");
            try {
                for (File file : directory1.listFiles()) {
                    FileUtils.copyFileToDirectory(file, directory2);
                }
                LOG.info("Done copying directory.");
                LOG.info("Files in copied directory " + directory1.listFiles().length);
            } catch (IOException ex) {
                LOG.info("Filecreator stub could not write files" + ex);
            }
        }
    }
}
