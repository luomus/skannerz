package fi.helsinki.it.skannerz.domain;

import fi.helsinki.it.skannerz.utils.MetadataUtils;
import java.io.File;
import java.io.IOException;
import org.apache.commons.lang3.Validate;
import org.slf4j.LoggerFactory;

/**
 * Image class represents those images that are found from image folders. Each Image has a relation to a metadata which describes the image and
 * to the folder where the image is located in.
 * 
 * Metadata can be also serialized to a file for later use and therefore Image class has also methods to read and save from this file.
 *
 * @author all
 */
public class Image extends File {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Folder.class);
    private final File metaFile;
    private final Folder parentFolder;
    private ExtendedMeta meta;

    /**
     * Class constructor.
     * 
     * @param path   a complete path to the file as String.
     * @param parent the image Folder in which image resides.
     */
    public Image(String path, Folder parent) {
        super(path);
        Validate.notNull(parent);
        this.metaFile = new File(parent.getPath() + File.separator + "Meta_" + super.getName() + ".json");
        this.parentFolder = parent;
        try {
            loadMeta();
        } catch (IllegalStateException | IOException ex) {
            LOG.warn("Could not load metadata for Image " + metaFile, ex);
        }
    }

    /**
     * Returns the File into which ExtendedMeta is serialized to.
     * 
     * @return the File with serialized content.
     */
    public File getMetaFile() {
        return metaFile;
    }

    /**
     * Returns the Folder where Image is located at.
     *
     * @return the Folder where image belongs.
     */
    public Folder getParentFolder() {
        return parentFolder;
    }

    /**
     * Returns the ExtendedMeta describing image attributes in context of ImageApi.
     * 
     * @return the ExtendedMeta.
     */
    public ExtendedMeta getMeta() {
        return meta;
    }

    private void setMeta(ExtendedMeta meta) {
        this.meta = meta;
    }

    /**
     * Creates ExtendedMeta describing image attributes for the Image by cloning the ExtendedMeta given as parameter.
     * Thus the Image's ExtendedMeta will have values copied from given object.
     *
     * @param meta ExtendedMeta where to copy the values.
     */
    public void createMetadataObjectFrom(ExtendedMeta meta) {
        setMeta(MetadataUtils.clone(meta));
    }

    /**
     * Saves Image's ExtendedMeta by serializing it to a file.
     * 
     * @throws Exception
     */
    public void saveMeta() throws Exception {
        MetadataUtils.saveMetadata(meta, metaFile);
    }

    /**
     * This method will merge values from given ExtendedMeta into this object's own ExtendedMeta. If this object has no ExtendedMeta yet,
     * given ExtendedMeta will be cloned to new one.
     * 
     * @param newMeta
     */
    public void mergeMeta(ExtendedMeta newMeta) {
        setMeta(meta == null
            ? (newMeta == null ? null : MetadataUtils.clone(newMeta))
            : MetadataUtils.merge(meta, newMeta));
    }

    /**
     * This method loads ExtendedMeta from object's own serialized file by deserializing it to an ExtendedMeta object.
     *
     * @throws java.io.IOException if object has no Metadata object yet.
     */
    public void loadMeta() throws IllegalStateException, IOException {
        if (hasMetaFile()) {
            meta = MetadataUtils.getMetadata(metaFile);
        } else {
            throw new IllegalStateException("Metadata file does not exist");
        }
    }

    /**
     * Returns true if ExtendedMeta file exists.
     *
     * @return boolean value
     */
    public boolean hasMetaFile() {
        return metaFile.isFile();
    }

    /**
     * Returns true if ExtendedMeta file is really a JSON file.
     * 
     * @return boolean value
     */
    public boolean isMetaFileJson() {
        try {
            MetadataUtils.getMetadata(metaFile);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof String && this.getName().equals(obj.toString())) {
            return true;
        }
        if (obj instanceof File && ((File) obj).getName().equals(this.getName())) {
            return true;
        }
        if (obj instanceof Image && ((Image) obj).getName().equals(this.getName())) {
            return true;
        }
        return false;
    }

}
