package fi.helsinki.it.skannerz.domain;

/**
 *
 * @author riku
 */
public class IntellectualRight {

    private String id;
    private String value;

    public IntellectualRight(String id, String value) {
        this.id = id;
        this.value = id;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getValue() + " (" + getId() + ")";
    }

}
