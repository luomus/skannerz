package fi.helsinki.it.skannerz.domain;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.nio.file.Path;

/**
 * Folder class represents an image folder produced by the Luomus image capturing system.
 * Each folder should have one or more TIF images, their corresponding thumbnail and preview pictures and a text and XML file with some additional data.  
 *        
 * @author all
 */
public class Folder extends File {

    private static final Logger LOG = LoggerFactory.getLogger(Folder.class);
    public Set<File> jpgThumbs = new HashSet<>();
    public Set<File> jpgPreviews = new HashSet<>();
    public Set<Image> tifImages = new HashSet<>();
    public File asciiFile, xmlFile;

    /**
     * Class constructor. 
     * 
     * @param folderPath a complete path to the folder as String.
     */
    public Folder(String folderPath) {
        super(folderPath);
    }

    /**
     * Class constructor. 
     * 
     * @param file a complete path to the folder as File.
     */
    public Folder(File file) {
        this(file.toString());
    }

    /**
     * Class constructor.
     *
     * @param path a complete path to the folder as Path.
     */
    public Folder(Path path) {
        this(path.toString());
    }

    public void setXmlFile(File xmlFile) {
        this.xmlFile = xmlFile;
    }

    public File getXml() {
        return this.xmlFile;
    }

    public File getAscii() {
        return this.asciiFile;
    }

    public void setAscii(File f) {
        this.asciiFile = f;
    }

    public Set<File> getJpg() {
        return this.jpgPreviews;
    }

    public void setJpg(File... files) {
        this.jpgPreviews.addAll(Arrays.asList(files));
    }

    public Set<File> getJpgThumbs() {
        return this.jpgThumbs;
    }

    public void setJpgThumbs(File... files) {
        this.jpgThumbs.addAll(Arrays.asList(files));
    }

    public Set<Image> getTifImages() {
        return this.tifImages;
    }
    
    /**
     * Returns a specific Image from the folder. Returned image is searched by the name of the File given as parameter.
     * 
     * @param file File of the image to return.
     * @return 
     */
    public Image getTifImage(File file) {
        for (Image image : getTifImages()) {
            if (image.equals(file)) {
                return image;
            }
        }
        return null;
    } 
    
    /**
     * Returns a specific Image from the folder. Returned image is searched by the string given as parameter. Exact match is required.
     * 
     * @param s Name of the file as a String.
     * @return an requested Image if exists. Else null.
     */
    public Image getTifImage(String s) {
        for (Image image : getTifImages()) {
            if (image.equals(s)) {
                return image;
            }
        }
        return null;
    } 

    /**
     * Add TIF files to this Folder.
     * 
     * @param files
     */
    public void addTifImages(File... files) {
        for (File image : files) {
            this.tifImages.add(new Image(image.toString(), this));
        }
    }
    
    /**
     * Return true if folder is already processed.
     * 
     * @return
     */
    public Boolean getProcessed() {
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.equalsIgnoreCase("processed");
            }
        };
        return super.listFiles(filter).length > 0;
    }

    /**
     * Set folder as processed or not processed. 
     * 
     * @param processed
     * @throws Exception
     */
    public void setProcessed(Boolean processed) throws Exception {
        if (this.exists()) {
            if (processed) {
                LOG.info("Creating processed file");
                new File(this.getAbsolutePath() + "/processed").createNewFile();
            } else {
                LOG.info("Deleting processed file");
                File fileToDelete = new File(this.getAbsolutePath() + "/processed");
                if (fileToDelete.exists()) {
                    fileToDelete.delete();
                }
            }
        }
    }

    /**
     * Returns true if Folder is valid image folder. Folder is taken as valid if XML and least one TIF image is present.
     * 
     * @return
     */
    public boolean isValid() {
        return !(xmlFile == null || tifImages.isEmpty());
    }
    
}
