package fi.helsinki.it.skannerz.domain.dataholders;

import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.utils.MetadataUtils;
import java.util.Set;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dataholder for the whole ImageFolder. Contains the file references and the
 * data retrieved from the database.
 *
 * @author waxwax
 */
public class SpecimenData {

    private static final Logger LOG = LoggerFactory.getLogger(SpecimenData.class);

    private final Folder folder;
    private final Set<Image> images;
    private final ExtendedMeta sharedMeta;

    private SpecimenData(Folder folder, ExtendedMeta meta) {
        Validate.notNull(folder);
        this.folder = folder;
        this.sharedMeta = meta;
        this.images = folder.getTifImages();
        try {
            setMetaToImages();
        } catch (Exception ex) {
            LOG.info("Could not create metadataObjects for Images");
        }
    }

    public Folder getFolder() {
        return this.folder;
    }

    public Set<Image> getImages() {
        return this.images;
    }

    public ExtendedMeta getSharedMetadata() {
        return this.sharedMeta;
    }

    private void setMetaToImages() throws Exception {
        for (Image image : folder.getTifImages()) {
            if (image.hasMetaFile() && image.isMetaFileJson()) {
                image.loadMeta();
                image.mergeMeta(this.sharedMeta);
            } else {
                image.createMetadataObjectFrom(sharedMeta);
            }
        }
    }

    public static class SpecimenDataBuilder {

        private Folder folder;
        private ExtendedMeta meta;

        public SpecimenData build() {
            return new SpecimenData(folder, meta);
        }

        public SpecimenDataBuilder setFolder(Folder folder) {
            this.folder = folder;
            return this;
        }

        public SpecimenDataBuilder setData(ExtendedMeta meta) {
            this.meta = meta == null ? null : MetadataUtils.clone(meta);
            return this;
        }

    }
}
