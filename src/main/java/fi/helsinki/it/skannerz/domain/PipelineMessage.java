package fi.helsinki.it.skannerz.domain;

import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Message to be passed in Mailbox to be consumed by Monitor.
 * 
 * @author riku
 * @see Mailbox
 * @see Monitor
 */
public class PipelineMessage {

    private static final Logger LOG = LoggerFactory.getLogger(PipelineMessage.class);

    private final Future<PipelineReturnCode> future;
    private final SpecimenData specimenData;

    public PipelineMessage(Future<PipelineReturnCode> future, SpecimenData specimenData) {
        Validate.notNull(future);
        Validate.notNull(specimenData);

        this.future = future;
        this.specimenData = specimenData;
    }

    public Future<PipelineReturnCode> getFuture() {
        return future;
    }

    public SpecimenData getSpecimenData() {
        return specimenData;
    }

    public PipelineReturnCode getStatus() {
        try {
            return future.isDone() ? future.get(): PipelineReturnCode.PROCESSING;
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            LOG.error("Exception while getting future.", e);
            return PipelineReturnCode.SEND_FAILED_RESEND;
        }
    }

    @Override
    public String toString() {
        return "Future: " + getFuture() + ", folderPath: " + getSpecimenData()
            .getFolder().getName();
    }

}
