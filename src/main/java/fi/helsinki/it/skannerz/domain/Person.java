package fi.helsinki.it.skannerz.domain;

/**
 *
 * @author riku
 */
public class Person {

    private final String label;
    private final String comment;
    private final String shortName;

    public Person(String label, String comment, String shortName) {
        this.label = label;
        this.comment = comment;
        this.shortName = shortName;
    }

    public String getLabel() {
        return label;
    }

    public String getComment() {
        return comment;
    }

    public String getShortName() {
        return shortName;
    }
    
}
