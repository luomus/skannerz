
package fi.helsinki.it.skannerz.domain;

import fi.helsinki.it.skannerz.utils.MetadataUtils;
import fi.luomus.kuvapalvelu.client.model.Meta;
import org.joda.time.DateTime;

/**
 * Class extending the KuvapalveluClient's Meta with additional fields.
 * 
 * @author all
 */
public class ExtendedMeta extends Meta {
    
    private String domain;
    private String uploadedId;
    private String imageName;
    private DateTime lastUpdated;
    
    public ExtendedMeta() {
        super();
    }

    public String getUploadedId() {
        return uploadedId;
    }

    public void setUploadedId(String uploadedIds) {
        this.uploadedId = uploadedIds;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public DateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(DateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString(){
        return MetadataUtils.serializeMetadata(this);
    }
    
}