package fi.helsinki.it.skannerz.domain;

/**
 * Enum representing end of a pipeline.
 * @author psmaatta
 */
public enum PipelineReturnCode {
    PROCESSING("Processing image..."),
    ABORTED("Image process was aborted"),
    OK("Image was processed successfully"),
    HISTORY("Already processed"),
    COULD_NOT_FIND_FILE("File not found in Image-API"),
    CANNOT_UPDATE_UNPROCESSED("Image could not be updated as it has not been processed and sent"),
    SEND_FAILED_FATAL("Error sending image data. Please contact an administrator to solve this issue"),
    SEND_FAILED_RESEND("Error sending image data. Try reprocessing this image later. If the problem persists, contact an administrator to solve this issue"),
    SEND_FAILED_BROKENIMAGE("Error sending image data. One of the images is broken or in an invalid format"),
    SEND_FAILED_NOTSUPPORTED("Error sending image data. The format of one of the images is not supported"),
    SEND_FAILED_MISSINGMETADATA("Error sending image data. The metadata lacks a required value"),
    QR_FAILED("Could not read the QR code. Please enter the image ID manually"),
    UNKNOWN_ERROR("Process stopped due to an unknown error. Please contact an administrator to solve this issue");

    private final String message;

    private PipelineReturnCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
