package fi.helsinki.it.skannerz.config;

/**
 * Defines the type of the Property.
 *
 * @author waxwax
 * @param <T>
 */
abstract class PropertyType<T> {

    /**
     * Helper method which casts the object o to the generic type T.
     *
     * @param o the object to cast
     * @return casted object of type T
     */
    T cast(Object o) {
        return (T) o;
    }

    /**
     * Converts the property String to type T.
     *
     * @param property the property String to convert
     * @return an object of generic type T
     */
    abstract protected T convert(String property);

    /**
     * @return the PropertyType class name.
     */
    String getType() {
        return this.getClass().getSimpleName();
    }
}
