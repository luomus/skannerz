package fi.helsinki.it.skannerz.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Loads properties from files into {@link java.util.Properties} objects.
 *
 * @author Walter Grönholm
 */
public interface PropertiesLoaderInterface {
    
    /**
     * Loads the properties file primarily from the file path given in the
     * system property with name systemPropertyName. If this system property has
     * not been set, the file is read from the path given in the fileName. The
     * file is loaded into the given properties object.
     *
     * @param properties the properties to fill
     * @param fileName the file path to load the properties from
     * @param systemPropertyName the system property name to read the
     * overrideable file path
     * @throws IOException If there was an issue loading the File.
     */
    void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException;

}
