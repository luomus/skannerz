package fi.helsinki.it.skannerz.config;

/**
 * A class used by Boolean typed properties.
 *
 * @author Walter Grönholm
 */
class BooleanProperty extends PropertyType<Boolean> {

    @Override
    protected Boolean convert(String property) {
        if ("true".equalsIgnoreCase(property)) {
            return true;
        }
        if ("false".equalsIgnoreCase(property)) {
            return false;
        }
        throw new IllegalArgumentException("Could not parse boolean of property with value: " + property);
    }

}
