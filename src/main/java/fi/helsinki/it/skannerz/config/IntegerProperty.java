package fi.helsinki.it.skannerz.config;

/**
 * A class used by Integer typed properties.
 *
 * @author waxwax
 */
class IntegerProperty extends PropertyType<Integer> {

    @Override
    protected Integer convert(String property) {
        return Integer.parseInt(property);
    }

}
