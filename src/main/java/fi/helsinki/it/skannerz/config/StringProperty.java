package fi.helsinki.it.skannerz.config;

/**
 * A class used by String typed properties.
 *
 * @author waxwax
 */
class StringProperty extends PropertyType<String> {

    @Override
    protected String convert(String property) {
        return property;
    }
}
