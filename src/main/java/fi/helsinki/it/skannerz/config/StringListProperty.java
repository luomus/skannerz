package fi.helsinki.it.skannerz.config;

import fi.helsinki.it.skannerz.utils.StringSplitterUtil;
import java.util.List;

/**
 * A class used by List&lt;String&gt; typed properties.
 *
 * @author waxwax
 */
class StringListProperty extends PropertyType<List<String>> {
    
    @Override
    protected List<String> convert(String property) {
        return StringSplitterUtil.getListOf(property);
    }
}
