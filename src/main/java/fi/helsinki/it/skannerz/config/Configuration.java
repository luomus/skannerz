package fi.helsinki.it.skannerz.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains the program configuration. The configuration must be initialized
 * with the {@link #init(java.lang.String) init} method before use.
 *
 * @author jarmokal, waxwax
 */
public class Configuration {

    private enum Property {

        USERNAME("username", new StringProperty()),
        PASSWORD("password", new StringProperty()),
        IMAGE_STORAGE_DB_URI("imageStorageDbUri", new StringProperty()),
        PIPELINE_THREADS("pipelineThreads", new IntegerProperty()),
        FOLDER_NAME_REGEX("folderNameRegex", new StringProperty()),
        DOMAINS("domains", new StringListProperty()),
        IMAGE_FOLDER_PATH("imageFolderPath", new StringProperty()),
        IMAGE_FOLDER_PATH_RELATIVE("imageFolderPath.relative", new BooleanProperty()),
        KEYWORDS("keywords", new StringListProperty()),
        INTELLECTUAL_RIGHT_URL("intellectualRights.api.url", new StringProperty()),
        API_LAJI_FI_ACCESS_TOKEN("intellectualRights.api.accessToken", new StringProperty()),
        DEFAULT_INTELLECTUAL_RIGHT("intellectualRights.defaultValue", new StringProperty()),
        MAX_DISPLAYED_HISTORY_ROWS("history.maxDisplayedRows", new IntegerProperty()),
        API_LAJI_CACHE_LOCATION("api.laji.fi.cache.location", new StringProperty()),
        LABEL_MM_PERSON_URL("label.MM.person.url", new StringProperty()),
        LABEL_MM_IMAGE_URL("label.MM.image.url", new StringProperty()),
        DEFAULT_LANGUAGE("defaultLanguage", new StringProperty()),
        DEFAULT_WAIT_TIME_FOR_FOLDER("defaultWaitTimeForFolder", new IntegerProperty());

        private final String name;
        private final PropertyType type;

        private Property(String name, PropertyType type) {
            this.name = name;
            this.type = type;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(Configuration.class);
    private static final Map<Property, Object> PROPERTYMAP = new HashMap<>();

    private Configuration() {
    }

    public static String getUsername() {
        return (String) PROPERTYMAP.get(Property.USERNAME);
    }

    public static String getPassword() {
        return (String) PROPERTYMAP.get(Property.PASSWORD);
    }

    public static String getImageStorageDdUri() {
        return (String) PROPERTYMAP.get(Property.IMAGE_STORAGE_DB_URI);
    }

    public static String getFolderNameRegex() {
        return (String) PROPERTYMAP.get(Property.FOLDER_NAME_REGEX);
    }

    public static Integer getPipelineThreads() {
        return (Integer) PROPERTYMAP.get(Property.PIPELINE_THREADS);
    }

    public static List<String> getDomainList() {
        return (List<String>) PROPERTYMAP.get(Property.DOMAINS);
    }

    public static String getImageFolderPath() {
        return (String) PROPERTYMAP.get(Property.IMAGE_FOLDER_PATH);
    }

    public static Boolean isImageFolderPathRelative() {
        return (Boolean) PROPERTYMAP.get(Property.IMAGE_FOLDER_PATH_RELATIVE);
    }

    public static List<String> getKeywordsList() {
        return (List<String>) PROPERTYMAP.get(Property.KEYWORDS);
    }

    public static String getIntellectualRightURL() {
        return (String) PROPERTYMAP.get(Property.INTELLECTUAL_RIGHT_URL);
    }

    public static String getApiLajiFiAccessToken() {
        return (String) PROPERTYMAP.get(Property.API_LAJI_FI_ACCESS_TOKEN);
    }

    public static String getDefaultIntellectualRight() {
        return (String) PROPERTYMAP.get(Property.DEFAULT_INTELLECTUAL_RIGHT);
    }

    public static Integer getMaxDisplayedHistoryRows() {
        return (Integer) PROPERTYMAP.get(Property.MAX_DISPLAYED_HISTORY_ROWS);
    }

    public static String getApiLajiFiCacheLocation() {
        return (String) PROPERTYMAP.get(Property.API_LAJI_CACHE_LOCATION);
    }

    public static String getMMPersonLabelURL() {
        return (String) PROPERTYMAP.get(Property.LABEL_MM_PERSON_URL);
    }

    public static String getMMImageLabelURL() {
        return (String) PROPERTYMAP.get(Property.LABEL_MM_IMAGE_URL);
    }

    public static String getDefaultLanguage() {
        return (String) PROPERTYMAP.get(Property.DEFAULT_LANGUAGE);
    }

    public static Integer getDefaultFolderWaitTime() {
        return (Integer) PROPERTYMAP.get(Property.DEFAULT_WAIT_TIME_FOR_FOLDER);
    }

    public static String getRootFolderPath() {
        return isImageFolderPathRelative()
            ? Configuration.class.getClassLoader().getResource(getImageFolderPath()).getPath()
            : getImageFolderPath();
    }

    /**
     * Calls method {@link #init(java.lang.String, java.lang.String) } with
     * parameters ("default.properties", propertyFileName)
     *
     * @param localFileName
     * @throws Exception
     */
    public static void init(String localFileName) throws Exception {
        init(new PropertiesLoader(), "default.properties", localFileName);
    }

    /**
     * Initializes the Configuration class. In case the configuration lacks a
     * property, a NoSuchFieldException is thrown.
     *
     * @param defaultFileName file name (or path) containing the default values,
     * which can be overridden
     * @param localFileName file name (or path) containing the property values
     * @throws java.io.IOException If there was an error reading the file(s)
     * @throws java.lang.NoSuchFieldException If a critical field was missing
     * from the configuration
     */
    public static void init(PropertiesLoaderInterface propertiesLoader, String defaultFileName, String localFileName) throws IOException, NoSuchFieldException {
        PROPERTYMAP.clear();

        Properties properties = new Properties();
        propertiesLoader.loadProperties(properties, defaultFileName, "default.properties.path");
        propertiesLoader.loadProperties(properties, localFileName, "local.properties.path");
        insertPropertiesToMap(properties, PROPERTYMAP);

        validateConfiguration(PROPERTYMAP);
    }

    private static void insertPropertiesToMap(Properties properties, Map<Property, Object> propertyMap) {
        for (Property propEnum : Property.values()) {
            if (properties.containsKey(propEnum.name)) {
                propertyMap.put(propEnum, propEnum.type.convert(properties.getProperty(propEnum.name)));
            }
        }
    }

    private static void validateConfiguration(Map<Property, Object> propertyMap) throws NoSuchFieldException {
        boolean missing = false;
        boolean erronous = false;
        StringBuilder missingFieldsMessage = new StringBuilder("Undefined field(s): '");
        StringBuilder erronousFieldsMessage = new StringBuilder("Field(s) with erronous values: '");
        for (Property propEnum : Property.values()) {
            Object property = propertyMap.get(propEnum);
            if (property == null) {
                if (missing) {
                    missingFieldsMessage.append(", ");
                } else {
                    missing = true;
                }
                missingFieldsMessage.append(propEnum.name);
            } else if (!isPropertyCastValid(property, propEnum.type)) {
                if (erronous) {
                    erronousFieldsMessage.append(",\n");
                } else {
                    erronous = true;
                }
                erronousFieldsMessage.append(propEnum.name).append(" could not be cast to type ").append(propEnum.type.getType());
            }
        }
        missingFieldsMessage.append('\'');
        erronousFieldsMessage.append('\'');

        if (missing || erronous) {
            throw new IllegalArgumentException(missingFieldsMessage.append('\n').append(erronousFieldsMessage).toString());
        }
    }

    private static boolean isPropertyCastValid(Object property, PropertyType propertyType) throws IllegalArgumentException {
        try {
            propertyType.cast(property);
            return true;
        } catch (ClassCastException ex) {
            return false;
        }
    }

}
