package fi.helsinki.it.skannerz.config;

import fi.helsinki.it.skannerz.Skannerz;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class PropertiesLoader implements PropertiesLoaderInterface {

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesLoader.class);

    @Override
    public void loadProperties(Properties properties, String fileName, String systemPropertyName) throws IOException {
        InputStream inputStream = null;
        String from = null;

        try {
            if (System.getProperty(systemPropertyName) == null) {
                inputStream = Skannerz.class.getClassLoader().getResourceAsStream(fileName);
                from = "file: " + fileName;
            } else {
                inputStream = new FileInputStream(new File(System.getProperty(systemPropertyName)));
                from = "system property " + systemPropertyName + ": " + System.getProperty(systemPropertyName);
            }
            if (inputStream == null) {
                throw new IOException("Error reading " + from);
            }
            properties.load(inputStream);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                    LOG.info("Configuration was successfully initialized from " + from);
                } catch (IOException e) {
                    LOG.error("Exception while closing input.", e);
                }
            }
        }
    }
}
