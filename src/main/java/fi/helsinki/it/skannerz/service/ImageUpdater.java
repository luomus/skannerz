package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.utils.MetadataUtils;
import fi.luomus.kuvapalvelu.client.ImageApiClient;
import fi.luomus.kuvapalvelu.client.model.Meta;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Work in progress. Use this class to update the metadata in a sent image.
 *
 * Microservice used specifically to update the (already sent) meta data.
 *
 * @author Walter Grönholm
 */
public class ImageUpdater {

    private static final Logger LOG = LoggerFactory.getLogger(ImageUpdater.class);

    /**
     * Updates the image found with the newMeta's uploadedId with only the
     * notNull content of the newMeta. For instance, if you only want to update
     * the tags of the image, you only need to set the tags for the newMeta and
     * leave everything else null.
     *
     * @param newMeta Object containing updated information.
     * @param imageApiClient Object for communicating with Image-API.
     * @return PipelineReturnCode to indicate whether or not the update was
     * successful.
     */
    public PipelineReturnCode update(ExtendedMeta newMeta, ImageApiClient imageApiClient) {
        String id = newMeta.getUploadedId();
        
        Validate.notNull(id);
        Validate.notNull(newMeta);
        Validate.notNull(imageApiClient);

        LOG.info("Updating image with id " + id);
        PipelineReturnCode code;
        try {
            code = update1(MetadataUtils.clone(newMeta), imageApiClient);
            LOG.info("Image " + id + " update successfully sent.");
        } catch (Exception ex) {
            LOG.error("Exception caught in Pipeline", ex);
            code = PipelineReturnCode.UNKNOWN_ERROR;
        } finally {
            saveMetaData();
        }
        return code;
    }

    private PipelineReturnCode update1(ExtendedMeta newMeta, ImageApiClient imageApiClient) {
        try {
            updateImageApi(newMeta.getUploadedId(), newMeta, imageApiClient);
        } catch (ApiException e) {
            return new ApiExceptionHandler().handleApiException(e);
        } catch (NotFoundException e) {
            LOG.error("File not found with id '" + newMeta.getUploadedId() + "'", e);
            return PipelineReturnCode.COULD_NOT_FIND_FILE;
        }
        return PipelineReturnCode.OK;
    }

    private void saveMetaData() {
        try {
            //save new Meta to folder
        } catch (Exception ex) {
            LOG.error("Exception while saving metadata", ex);
        }
    }

    private void updateImageApi(String id, Meta meta, ImageApiClient imageApiClient) throws ApiException, NotFoundException {
        LOG.info("Kuvapalvelu-client update");
        imageApiClient.update(id, meta);
    }
}
