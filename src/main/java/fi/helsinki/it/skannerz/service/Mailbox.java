package fi.helsinki.it.skannerz.service;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Mailbox implementation which notifies registered receivers when a new 
 * message is put into the mailbox.
 *
 * @param <T> type of the message.
 * @author riku
 */
public class Mailbox<T> implements MailboxInterface<T> {
    private static final Logger LOG = LoggerFactory.getLogger(Mailbox.class);

    private final Queue<T> messageQueue = new ConcurrentLinkedQueue<>();
    private final Set<MailboxReceiver<T>> msgReceivers = new LinkedHashSet<>();

    @Override
    public void put(T message) {
        Validate.notNull(message);
        messageQueue.add(message);
        notifyReceivers(message);
    }

    @Override
    public void addReceiver(MailboxReceiver<T> receiver, boolean handleMsgQueue) {
        Validate.notNull(receiver);
        msgReceivers.add(receiver);
        if (handleMsgQueue) {
            messageQueue.forEach(receiver::handleMail);
        }
    }


    @Override
    public void handleMail(MailboxReceiver<T> caller) {
        Validate.notNull(caller);
        if (!msgReceivers.contains(caller)) {
            throw new IllegalArgumentException("Caller isn't registered as a receiver for the mailbox.");
        }
        messageQueue.forEach(caller::handleMail);
    }

    private void notifyReceivers(T message) {
        Validate.notNull(message);
        msgReceivers.forEach(i -> i.handleMail(message));
    }

    @Override
    public void removeReceiver(MailboxReceiver<T> receiver) {
        msgReceivers.remove(receiver);
    }

    @Override
    public Set<MailboxReceiver<T>> getReceivers() {
        return new HashSet<>(msgReceivers);
    }

}
