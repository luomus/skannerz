package fi.helsinki.it.skannerz.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.luomus.utils.exceptions.ApiException;
import java.lang.reflect.Type;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handler for ApiExeptions form Image-API Client
 * 
 * @author arto, Walter Grönholm
 */
class ApiExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ApiExceptionHandler.class);
    private static final Gson GSON = new GsonBuilder().create();

    /**
     * Handles an ApiException from Image-API Client and returns the corresponding {@link fi.helsinki.it.skannerz.domain.PipelineReturnCode}.
     *
     * @param apiException Exception thrown by Image-API-Client
     * @return Corresponding PipelineReturnCode
     */
    PipelineReturnCode handleApiException(ApiException apiException) {
        switch (apiException.getCode()) {
            case "404":
                LOG.error("404 from Image-API", apiException);
                return PipelineReturnCode.SEND_FAILED_RESEND;
            case "400":
                LOG.error("400 from Image-API", apiException);
                return getDetails(apiException);
            default:
                LOG.error("Error while sending to Image-API", apiException);
                return PipelineReturnCode.SEND_FAILED_FATAL;
        }
    }
    
    private PipelineReturnCode getDetails(ApiException apiException) {
        List<Message> missingMetaValuesList = parseGetDetails(apiException.getDetails());
        if (!missingMetaValuesList.isEmpty()) {
            StringBuilder logMessage = new StringBuilder();
            for (Message m : missingMetaValuesList) {
                logMessage = logMessage.append(m.getMessage())
                        .append(" ")
                        .append(m.getPath().lastIndexOf("."))
                        .append(" ")
                        .append(m.getInvalidValue())
                        .append(", ");
            }
            LOG.info(logMessage.toString());
            return PipelineReturnCode.SEND_FAILED_MISSINGMETADATA;
        }    
        return PipelineReturnCode.SEND_FAILED_BROKENIMAGE;        
    }

    private List<Message> parseGetDetails(String details) {
        Type listType = new TypeToken<List<Message>>(){}.getType();
        return GSON.fromJson(details, listType);
    }
    
    private class Message {
        String message;
        String path;
        String invalidValue;

        public String getMessage() {
            return message;
        }

        public String getPath() {
            return path;
        }

        public String getInvalidValue() {
            return invalidValue;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public void setInvalidValue(String invalidValue) {
            this.invalidValue = invalidValue;
        }      
        
    }
}
