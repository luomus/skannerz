package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.Folder;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Peter, Perttu
 */
public class ImageFolderReader implements ImageFolderReaderInterface {

    private static final Logger LOG = LoggerFactory.getLogger(ImageFolderReader.class);

    public ImageFolderReader() {
    }

    /**
     * Make folder object with the files as values.
     *
     * @param path Path of the folder object we are creating.
     * @return Returns the folder object that contains the images.
     */
    @Override
    public Folder createFolderObject(File path) {

        Folder folder = new Folder(path.getPath());
        List<Path> workingFolderFiles = new ArrayList<>();
        try {
            workingFolderFiles = Files.list(path.toPath()).filter(Files::isRegularFile).collect(Collectors.toList());
        } catch (IOException ex) {
            LOG.warn("Couldn't list files from " + path);
        }
        if (workingFolderFiles.isEmpty()) {
            throw new IllegalStateException(this.getClass().getName() + ": imagefolder " + folder.getName() + " is empty, nothing to process.");
        }
        for (Path p : workingFolderFiles) {
            File f = p.toFile();
            switch (getExtension(f)) {
                case "properties":
                    folder.setAscii(f);
                    break;
                case "xml":
                    folder.setXmlFile(f);
                    break;
                case "tif":
                    folder.addTifImages(f);
                    break;
                case "jpg":
                    distinquishJpg(f, folder);
                    break;
                default:
                    break;
            }
        }
        LOG.info("Created folder " + folder.getName() + " with " + folder.getTifImages().size() + " TIFs and "
                + (workingFolderFiles.size() - folder.getTifImages().size()) + " other files.");
        return folder;

    }

    /**
     * Gets the extension of a file.
     *
     * @param f file to get extension of.
     * @return the extension. Throws an exception if the file does not contain
     * an extention.
     */
    private String getExtension(File f) {
        if (!f.getName().contains(".")) {
            LOG.info("Filename doesn't have an extension");
        }
        int i = f.getName().lastIndexOf('.');
        return f.getName().substring(i + 1);
    }

    /**
     * Distinguish the different jpg files from each other. Separates actual
     * preview images from the thumbnails, and saves them accordingly into
     * folder object. Can distinquish between Preview000.jpg preview000.jpg,
     * Thumb000.jpg and thumb000.jpg. Where the "000" numbering can be anything.
     *
     * Throws exception if it cannot distinguish between thumbnail and preview
     * image.
     *
     * @param folder Folder object to save the images into.
     * @param f File that we are distinguishing between preview and thumb
     */
    protected void distinquishJpg(File f, Folder folder) {
        if (f.getName().matches("(^.*([P,p]review).*\\.(jpg)$)")) {
            folder.setJpg(f);
        } else if (f.getName().matches("(^.*([T,t]humb).*\\.(jpg)$)")) {
            folder.setJpgThumbs(f);
        } else {
            LOG.info("Couldn't recognize jpg-image type. ");
        }
    }
}
