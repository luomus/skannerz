package fi.helsinki.it.skannerz.service;

/**
 * Creates new PipelineManager objects
 * @link fi.helsinki.it.skannerz.service.PipelineManager
 * 
 * @author Walter Grönholm
 */
public class PipelineManagerFactory {
    
    private static PipelineManager pipelineManager;
    
    public static void set(PipelineManager pipelineManager) {
        PipelineManagerFactory.pipelineManager = pipelineManager;
    }
    
    public static PipelineManager get() {
        return pipelineManager;
    }
}
