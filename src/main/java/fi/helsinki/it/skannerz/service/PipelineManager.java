package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData.SpecimenDataBuilder;
import fi.helsinki.it.skannerz.utils.IdResolver;
import fi.luomus.kuvapalvelu.client.ImageApiClient;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Manages Pipelines
 * 
 * @author havarto
 */
public class PipelineManager implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(PipelineManager.class);

    private final RootFolderReaderInterface rootFolderReader;
    private final ExecutorService executor;
    private final List<Future<PipelineReturnCode>> pipelineList;
    private final IdResolver idResolver;
    private final PipelineFactory pipelineFactory;
    private final LinkedList<Folder> waitingMetadataQueue;
    private MailboxInterface<PipelineMessage> mailbox;
    private ExtendedMeta imageMetadata;
    private Thread thread;
    private ImageApiClient imageApiClient;

    /**
     *
     * @param rootFolderReader
     * @param idResolver
     * @param pipelineFactory
     */
    public PipelineManager(RootFolderReaderInterface rootFolderReader, IdResolver idResolver, PipelineFactory pipelineFactory) {
        executor = Executors.newFixedThreadPool(Configuration.getPipelineThreads());
        pipelineList = new ArrayList<>();
        this.rootFolderReader = rootFolderReader;
        this.idResolver = idResolver;
        this.pipelineFactory = pipelineFactory;
        mailbox = MailboxFactory.getMailbox(PipelineMessage.class);
        waitingMetadataQueue = new LinkedList<>();
    }

    /**
     * Starts pipeline in new thread
     * @param caller
     * @return new 
     */
    public Thread startPipelines(String... caller) {
        LOG.info("Starting Pipelines");
        if (thread != null && thread.isAlive()) {
            return thread;
        }
        if (caller.length == 0) {
            thread = new Thread(this);
        } else {
            thread = new Thread(this, caller[0]);
        }
        thread.start();
        return thread;
    }

    /**
     *
     */
    public void stopPipelines() {
        LOG.info("Stopping Pipelines");
        if (thread != null && thread.isAlive()) {
            thread.interrupt();
        }
        synchronized (this) {
            for (Future<PipelineReturnCode> future : pipelineList) {
                if (!(future.isDone() || future.isCancelled())) {
                    future.cancel(true);
                }
            }
            pipelineList.clear();
        }
    }

    /**
     *
     * @return
     */
    public List<Future<PipelineReturnCode>> giveCurrentFutures() {
        synchronized (this) {
            LOG.info("Size of PipelineList when calling giveCurrentFutures " + pipelineList.size());
            return new ArrayList<>(pipelineList);
        }
    }

    @Override
    public void run() {
        try {
            LOG.info("Started run() ImageMetadata");
            while (true) {
                Folder folder = nextFolder();

                if (folder == null) {
                    LOG.info("No more folders to process so we wait on lock");
                    synchronized (rootFolderReader) {
                        rootFolderReader.wait();
                    }
                    continue;
                }

                if (imageMetadata == null) {
                    LOG.info("PipelineManager has no active metadata while processing " + folder);
                }

                if (!hasMetadata(folder)) {
                    LOG.info("Folder " + folder + " has no metadata in it");
                } else {
                    LOG.info("Found metadata from folder " + folder);
                }

                if (imageMetadata == null && !hasMetadata(folder)) {
                    LOG.info("Putting folder " + folder + " to waiting queue");
                    waitingMetadataQueue.push(folder);
                    continue;
                }

                process(folder, imageMetadata);
            }
        } catch (InterruptedException | IllegalStateException e) {
            LOG.warn("Exception caught in PipelineManager, stopping process...", e);
        } catch (Exception e) {
            LOG.error("Unexpected exception caught in PipelineManager, stopping process...", e);
            throw e;
        }
    }

    private void submitPipeline(PipelineInterface pipeline, SpecimenData specimenData) {
        synchronized (this) {
            Future<PipelineReturnCode> future = executor.submit(pipeline);
            pipelineList.add(future);
            mailbox.put(new PipelineMessage(future, specimenData));
            LOG.info("Started new Pipeline, PipelineList size is now " + pipelineList.size());
        }
    }

    private Folder nextFolder() {
        Folder folder;
        if (imageMetadata == null || waitingMetadataQueue.isEmpty()) {
            folder = rootFolderReader.returnNextFolder();
        } else {
            folder = waitingMetadataQueue.pop();
        }
        return folder;
    }

    /**
     *
     * @param folder
     */
    public void process(Folder folder) {
        process(folder, null);
    }

    /**
     *
     * @param folder
     * @param meta
     */
    public void process(Folder folder, ExtendedMeta meta) {
        LOG.info("Starting Pipeline from object " + folder);
        SpecimenData specimenData = new SpecimenDataBuilder()
            .setData(meta)
            .setFolder(folder)
            .build();
        PipelineInterface pipeline = pipelineFactory.getPipeline(
            specimenData, imageApiClient, idResolver);
        submitPipeline(pipeline, specimenData);
    }

    /**
     *
     * @param metadata
     */
    public void setImageMetadata(ExtendedMeta metadata) {
        this.imageMetadata = metadata;
        synchronized (rootFolderReader) {
            rootFolderReader.notify();
        }
    }

    /**
     *
     * @param imageApiClient
     */
    public void setImageApiClient(ImageApiClient imageApiClient) {
        this.imageApiClient = imageApiClient;
    }

    /**
     *
     * @param mailbox
     */
    public void setMailbox(MailboxInterface<PipelineMessage> mailbox) {
        this.mailbox = mailbox;
    }
    
    /**
     *
     * @param folder
     * @return
     */
    public boolean hasMetadata(Folder folder) {
        for (Image image : folder.getTifImages()) {
            if (!image.isMetaFileJson()) {
                return false;
            }
        }
        return true;
    }

}
