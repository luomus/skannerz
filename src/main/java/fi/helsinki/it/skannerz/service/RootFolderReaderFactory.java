package fi.helsinki.it.skannerz.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class RootFolderReaderFactory {

    private static final Logger LOG = LoggerFactory.getLogger(RootFolderReaderFactory.class);
    private static final Map<String, RootFolderReaderInterface> PATH_TO_READER_MAP = new HashMap<>();

    /**
     * Factory method to create instances of RootFolderReader.
     *
     * @param rootFolderPath Path to the root folder, which the reader will be
     * scanning for folders.
     * @param imageFolderReaderInterface ImageFolderReader that the
     * rootFolderReader will be using to process the folders.
     * @return Returns the RootFolderReaderInterface that was created with the
     * factory.
     */
    public static RootFolderReaderInterface get(String rootFolderPath, ImageFolderReaderInterface imageFolderReaderInterface) {
        if (!PATH_TO_READER_MAP.containsKey(rootFolderPath)) {
            try {
                PATH_TO_READER_MAP.put(rootFolderPath, new RootFolderReaderV2(rootFolderPath, imageFolderReaderInterface));
            } catch (IOException ex) {
                LOG.info("Couldn't not intialise constructor, check path. " + ex);
                return null;
            }
        }
        RootFolderReaderInterface rootFolderReader = PATH_TO_READER_MAP.get(rootFolderPath);
        return rootFolderReader;
    }
    /**
     * Resets the factory.
     */
    public static void reset() {
        PATH_TO_READER_MAP.clear();
    }
}
