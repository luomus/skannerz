package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import java.util.concurrent.Callable;

/**
 * Reads QR-codes from given images, updates meta data connected to those images and
 * sends them to Image Api using given ImageApiClient.
 * 
 * @author Walter Grönholm
 */
public interface PipelineInterface extends Callable<PipelineReturnCode> {
    
}
