package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.Folder;
import java.io.File;

/**
 * Handles processing individual folders.
 * Processes the different files in the image folder using filename, and extension.
 *
 * @author porttpet
 */
public interface ImageFolderReaderInterface {
     /**
     * Make folder object with the files as values.
     *
     * @param path Path of the folder object to be created.
     * @return Returns the folder object.
     */
    public Folder createFolderObject(File path);
}
