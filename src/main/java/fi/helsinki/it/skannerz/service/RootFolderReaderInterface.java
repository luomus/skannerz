package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.Folder;
import java.util.ArrayList;

/**
 * RootFolderReader uses watcher service to read new files from the root Folder.
 * Reads files using watcher service with root folder registered to listen for EVENT_CREATE events.
 * After a folder is found, it's put on a timer loop, where it resets whenever the folder gets a new file during the configurable window (Default: 3000 milliseconds)
 * When the timer runs out, moves the folder to processing queue, where it can be got from using returnNextFolder()
 * @author all
 */
public interface RootFolderReaderInterface extends Runnable {
    
    /**
     * Gives an image folder. Returns only valid and unprocessed image folders.
     *
     * @return Returns the Folder object that contains the images.
     */
    public Folder returnNextFolder();
    
    /**
     * Return all folders in concurrent queue at once.
     * Avoid using if returnNextFolder(); is able to be used.
     * @return returns ArrayList with the folders.
     */
    public ArrayList<Folder> returnRestFoldersAsList();
}
