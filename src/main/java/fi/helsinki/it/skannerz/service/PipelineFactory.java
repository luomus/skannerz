package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.utils.IdResolver;
import fi.luomus.kuvapalvelu.client.ImageApiClient;

/**
 * Creates new pipelines
 * 
 * @author psmaatta
 */
public class PipelineFactory {

    /**
     * Returns new Pipeline object
     * 
     * @param specimendata Object containing data to be processed (@link fi.helsinki.it.skannerz.domain.dataholders.SpecimenData)
     * @param imageApiClient Object of client for communicating with Image-Api (@link fi.luomus.kuvapalvelu.client.ImageApiClient)
     * @param resolver Object for reading QR-code from images (@link fi.helsinki.it.skannerz.utils.IdResolver)
     * @return new Pipeline object (@link fi.helsinki.it.skannerz.service.pipeline)
     */
    public PipelineInterface getPipeline(SpecimenData specimendata, ImageApiClient imageApiClient, IdResolver resolver) {
        return new Pipeline(specimendata, imageApiClient, resolver);
    }

}
