package fi.helsinki.it.skannerz.service;

import com.google.zxing.ReaderException;
import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.utils.IdResolver;
import fi.helsinki.it.skannerz.utils.MetadataUtils;
import fi.luomus.kuvapalvelu.client.ImageApiClient;
import fi.luomus.kuvapalvelu.client.ImageFormatNotSupported;
import fi.luomus.kuvapalvelu.client.model.Meta;
import fi.luomus.kuvapalvelu.client.model.WGS84Coordinates;
import fi.luomus.utils.exceptions.ApiException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Pipeline implementation which reads QR-codes from images contained in
 * Specimendata using IdResolver and then sends them to Image Api using
 * given ImageApiClient.
 *
 * @author all
 */
public class Pipeline implements PipelineInterface {

    private static final Logger LOG = LoggerFactory.getLogger(Pipeline.class);
    private final Logger log;
    private final IdResolver resolver;
    private final SpecimenData specimendata;
    private final ImageApiClient imageApiClient;
    private final History history;
    private final Set<Image> tifImages;

    /**
     * Initializes new Pipeline object.
     * 
     * @param specimendata contains images and their shared metadata, used to access those 
     * @param imageApiClient used for sending images and corresponding metadata to Image Api 
     * @param resolver used to scan QR-code from an image
     */
    public Pipeline(SpecimenData specimendata, ImageApiClient imageApiClient, IdResolver resolver) {
        this.specimendata = specimendata;
        this.tifImages = specimendata.getImages();
        this.imageApiClient = imageApiClient;
        this.resolver = resolver;
        log = LoggerFactory.getLogger("Pipeline " + specimendata.getFolder().getName());
        history = new History();
    }

    /**
     * Starts executing this pipeline. 
     * <ol>
     * <li>tries to find uri from all given images by using given QR-code resolver.</li>
     * <li>tries to add uri to metadata</li>
     * <li>tries to send all images in folder and their meta to Image Api</li>
     * <li>if sending was successful, marks folder processed</li>
     * <li>if sending was successful tries to add folder to history</li>
     * </ol>
     * @return pipeline return code, tells if process was successful or not.
     */
    @Override
    public PipelineReturnCode call() {
        log.info("Started pipeline");
        PipelineReturnCode code;
        try {
            code = call(tifImages);
        } catch (Exception ex) {
            log.error("Exception caught in Pipeline", ex);
            code = PipelineReturnCode.UNKNOWN_ERROR;
        } finally {
            saveAll(tifImages);
        }
        return code;
    }
    
    private PipelineReturnCode call(Set<Image> tifImages) {
        String uri = getUri(tifImages);

        if (!addUriToMetadata(tifImages, uri)) {
            return PipelineReturnCode.QR_FAILED;
        }

        fillMetaForImages(tifImages);

        for (Image image : tifImages) {
            if (!image.hasMetaFile()) {
                saveMetaData(image);
            }
            log.info("sending  " + image);
            try {
                image.getMeta().setUploadedId(callKuvapalveluClient(image, imageApiClient, log));
            } catch (ApiException e) {
                return new ApiExceptionHandler().handleApiException(e);
            } catch (ImageFormatNotSupported e) {
                return PipelineReturnCode.SEND_FAILED_NOTSUPPORTED;
            } catch (RuntimeException re) {
                LOG.error("RuntimeException when sending to image api", re);
                return PipelineReturnCode.SEND_FAILED_FATAL;
            }
            log.info("File " + image.getName() + "successfully sent.");
        }

        try {
            specimendata.getFolder().setProcessed(Boolean.TRUE);
        } catch (Exception ex) {
            log.error("Couldn't create processed file to Folder", ex);
        }
        try {
            String rootFolderPath = Configuration.getRootFolderPath();
            history.setPath(rootFolderPath);
            history.addFolderToHistory(specimendata.getFolder());
        } catch (Exception ex) {
            log.error("Couldn't add folders to history", ex);
        }
        return PipelineReturnCode.OK;
    }

    private boolean addUriToMetadata(Set<Image> tifImages, String uri) {
        for (Image image : tifImages) {
            List<String> documentIds = image.getMeta().getDocumentIds();
            if (uri == null && documentIds.isEmpty()) {
                return false;
            } else if (uri != null && !documentIds.contains(uri)) {
                documentIds.add(uri);
            }
        }
        return true;
    }

    private void fillMetaForImages(Set<Image> tifImages) {
        for (Image image : tifImages) {
            log.info("filling meta for image " + image);
            ExtendedMeta meta = image.getMeta();
            setSourceSystemToMetadata(meta);
            setCoordinatesToMetadata(meta);
            setTimeStampToMetadata(meta, specimendata.getFolder(), log);
        }
    }

    private void saveAll(Set<Image> tifImages) {
        LOG.info("Saving metadata for all images");
        for (Image image : tifImages) {
            saveMetaData(image);
        }
    }

    private String getUri(Set<Image> tifImages) {
        for (Image image : tifImages) {
            String uri = getSpecimenUriFromQr(image, resolver, log);
            if (uri != null) {
                return uri;
            }
        }
        return null;
    }

    private void saveMetaData(Image image) {
        try {
            image.saveMeta();
        } catch (Exception ex) {
            log.error("Exception caught while saving metadata for image" + image, ex);
        }
    }

    protected static String getSpecimenUriFromQr(Image image, IdResolver resolver, Logger log) {
        log = (log != null) ? log : LOG;
        log.info("QR-reader call. Trying to read QR from image: " + image);
        try {
            String uri = resolver.getIdentifier(image.toURI());
            if (!uri.startsWith("http://")) {
                uri = image.getMeta().getDomain().concat(uri);
            }
            if (!image.getMeta().getTags().contains("label")) {
                image.getMeta().getTags().add("label");
            }
            return uri;
        } catch (ReaderException | IOException e) {
            log.info("Couldn't read QR from image: " + image, e);
        }
        return null;
    }

    protected static void setTimeStampToMetadata(Meta meta, Folder folder, Logger log) {
        log = (log != null) ? log : LOG;
        Pattern pattern = Pattern.compile(Configuration.getFolderNameRegex());
        Matcher matcher = pattern.matcher(folder.getPath());
        if (matcher.find()) {
            DateTimeFormatter dateformatter = DateTimeFormat.forPattern("yyyy-MM-dd_HH-mm-ss");
            DateTime dateTime = dateformatter.parseDateTime(matcher.group(1));
            meta.setCaptureDateTime(dateTime);
        } else {
            try {
                BasicFileAttributes attr = Files.readAttributes(folder.toPath(), BasicFileAttributes.class
                );
                FileTime fileTime = attr.creationTime();
                DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");
                DateTime dateTime = df.parseDateTime(fileTime.toString());
                meta.setCaptureDateTime(dateTime);
            } catch (Exception e) {
                meta.setCaptureDateTime(new DateTime());
            }
        }

    }

    protected static void setCoordinatesToMetadata(Meta meta) {
        WGS84Coordinates wgs84Coordinates = new WGS84Coordinates();
        meta.setWgs84Coordinates(wgs84Coordinates);
    }

    protected static void setSourceSystemToMetadata(Meta meta) {
        String sourceSystem = Configuration.getUsername();
        meta.setSourceSystem(sourceSystem);
    }

    protected static void addUriToMetadata(Meta meta, String URI) {
        meta.getDocumentIds().add(URI);
    }

    protected static String callKuvapalveluClient(Image image, ImageApiClient imageApiClient, Logger log) throws ApiException {
        log = (log != null) ? log : LOG;
        log.info("Kuvapalvelu-client call");
        return imageApiClient.upload(image, MetadataUtils.convertMeta(image.getMeta()));
    }

}
