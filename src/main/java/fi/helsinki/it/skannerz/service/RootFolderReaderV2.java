package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.Folder;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author all
 */
public class RootFolderReaderV2 implements RootFolderReaderInterface {

    private static final Logger LOG = LoggerFactory.getLogger(RootFolderReaderV2.class);

    private final ConcurrentLinkedQueue<Path> concurrentSubfolders;
    private final Path rootFolderPath;
    private final ImageFolderReaderInterface imageFolderReaderInterface;

    // Private constructor to get Singletons only
    RootFolderReaderV2(String rootFolderPath, ImageFolderReaderInterface imageFolderReaderInterface) throws IOException {
        this.imageFolderReaderInterface = imageFolderReaderInterface;
        this.rootFolderPath = new File(rootFolderPath).toPath();
        this.concurrentSubfolders = new ConcurrentLinkedQueue<>();
        // As one might see, there is no use for filehandlinginterface. Let's simplify things for now on.
        List<Path> subfolders = Files.list(this.rootFolderPath).filter(Files::isDirectory).collect(Collectors.toList());
        Collections.sort(subfolders);
        LOG.info("Adding " + subfolders.size() + " folders from " + rootFolderPath + " to queue");
        concurrentSubfolders.addAll(subfolders);
    }

    @Override
    public Folder returnNextFolder() {
        // check if there is something on the queue and if not return null.. This is atomic due to concurrent list no need to synchronize on this
        Path nextPath = concurrentSubfolders.poll();
        if (nextPath == null) {
            return null;
        }
        LOG.info("Taking folder " + nextPath + " from queue to process into imageFolder");
        try {
            Folder nextFolder = imageFolderReaderInterface.createFolderObject(nextPath.toFile());
            if (!folderIsValidImageFolderAndNotYetProcessed(nextFolder)) {
                LOG.info(nextPath + "was not valid or was already processed");
                return returnNextFolder();
            }
            return nextFolder;
        } catch (IllegalStateException ex) {
            LOG.warn(ex.toString());
            return returnNextFolder();
        }
        // check whether the created Folder is invalid or already processed..

    }

    private boolean folderIsValidImageFolderAndNotYetProcessed(Folder f) {
        return (f.isValid() && !f.getProcessed());
    }

    /**
     *
     * Don't use this method at all.
     *
     * @return
     */
    @Override
    public ArrayList<Folder> returnRestFoldersAsList() {
        ArrayList<Folder> folderList = new ArrayList<>();
        if (concurrentSubfolders.isEmpty()) {
            return null;
        } else {
            Path[] subfolders = concurrentSubfolders.toArray(new Path[concurrentSubfolders.size()]);
            for (Path p : subfolders) {
                Folder f = imageFolderReaderInterface.createFolderObject(p.toFile());
                if (folderIsValidImageFolderAndNotYetProcessed(f)) {
                    folderList.add(f);
                }
            }
        }
        return folderList;
    }

    /**
     * Main method of the RootFolderReaderV2's thread. Sets up WatchService, and
     * registers watcher for creating files (ENTRY_CREATE)- Keeps the loop going
     * for processing folders in the specified root folder. Throws an exception
     * if interrupted, or in case of file not found, or unavailable.
     */
    @Override
    public void run() {
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            rootFolderPath.register(watcher, ENTRY_CREATE);
            WatchKey key;
            do {
                // WatchService.take() is a blocking command and will wait until an event has been seen or interrupted.
                key = watcher.take();
                processWatchKey(key);

            } while (key.reset()); // WatchKey.reset will return false if key has become invalid.
        } catch (InterruptedException | IOException ex) {
            LOG.info("Exception, check path or was interrupted. " + ex);
        }
    }

    /**
     * Handles processing child folders (the image folders) for additions to the
     * folder, before sending to processing queue. Main reason for this is to
     * try and ensure getting the full folder, and not partial one, that'll
     * likely fail.
     *
     * @param path the path of the image folder to watch for modifications.
     */
    private void processChildFolderForModifications(Path path) {
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            path.register(watcher, ENTRY_CREATE);
            WatchKey key;
            do {
                // WatchService.take() is a blocking command and will wait until an event has been seen or interrupted.
                key = watcher.poll(1, TimeUnit.SECONDS);
                if (key != null) {
                    processSubFolderForModifications(key);
                }

            } while (key != null && key.reset()); // WatchKey.reset will return false if key has become invalid.
        } catch (InterruptedException | IOException ex) {
            LOG.info("Exception, check path or was interrupted. " + ex);
        }
    }

    private void processWatchKey(WatchKey key) {
        // For each WatchEvent check if context is Path and then process.
        // The list probably never has more than one WatchEvent.
        for (WatchEvent<?> event : key.pollEvents()) {

            if (event.context() instanceof Path && event.kind() == ENTRY_CREATE) {
                // Resolve to full path
                Path child = rootFolderPath.resolve((Path) event.context());
                LOG.info("Starting wait loop to ensure we get the full folder for " + child + ". Wait period is: " + Configuration.getDefaultFolderWaitTime() + " millis after latest file.");
                processChildFolderForModifications(child);
                LOG.debug("Folder should have all it's files, moving on.");
                // no need to synchronize here either
                if (child.toFile().isDirectory()) {
                    LOG.info("Adding folder " + child + " to queue");
                    if (!concurrentSubfolders.contains((child))) {
                        concurrentSubfolders.add(child);
                    }
                    synchronized (this) {
                        this.notify();
                    }
                }
            }
        }
    }

    /**
     * Actually does the checking for additions in the image folder before
     * passing it on to processing queue. Wait the time specified in
     * Configuration after each file. The timer is reset whenever a new file is
     * added, even if the timer didn't run out for the earlier. The timer is
     * *not* additive for each file added, but resets to the default loaded from
     * Configuration.
     *
     * @param key WatchKey to identify what we are monitoring, and processing.
     * Cancelling watchKey is important at the end, if not, will loop forever.
     */
    private void processSubFolderForModifications(WatchKey key) {
        long time = System.currentTimeMillis();
        while (System.currentTimeMillis() - time < Configuration.getDefaultFolderWaitTime()) {
            for (WatchEvent<?> event : key.pollEvents()) {
                if (event.context() instanceof Path && event.kind() == ENTRY_CREATE) {
                    time = System.currentTimeMillis();
                }
            }
        }
        key.cancel();
        LOG.info("no new files, exiting wait loop");
    }
}
