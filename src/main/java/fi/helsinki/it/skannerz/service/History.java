package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.utils.HistoryFutureAdapter;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData.SpecimenDataBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Future;
import org.slf4j.LoggerFactory;

/**
 * History. Handles loading and saving history files.
 * <ol>
 * <li>Saves processed folders as list into a file as plain text.</li>
 * <li>Loads plain text folder names from file, constructs folder and loads
 * proper metas for the folder and it's images.</li>
 * <li>Sends the processed history files to the monitor to be displayed at the
 * bottom of the list.</li>
 * </ol>
 *
 * @author porttpet
 */
public class History {

    private ArrayList<Folder> historyList = new ArrayList<>();
    private String historyFilePath;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(History.class);
    private MailboxInterface<PipelineMessage> mailbox;

    /**
     * Constructor that initializes the mailbox.
     */
    public History() {
        mailbox = MailboxFactory.getMailbox(PipelineMessage.class);
    }

    public String getPath() {
        return historyFilePath;
    }

    public void setPath(String s) {
        historyFilePath = s;
    }

    /**
     * Adds folders to history.
     *
     * @param f Folders added are parameter(s) to be added to the history.
     */
    public void addFolderToHistory(Folder... f) {
        for (Folder fold : f) {
            if (!historyList.contains(fold)) {
                historyList.add(fold);
            }
        }

        saveHistoryToFile();
    }

    /**
     * Removes folders from history.
     *
     * @param f Folders added are parameter(s) to be added to the history.
     */
    public void removeFolderFromHistory(Folder... f) {
        for (Folder fold : f) {
            historyList.remove(fold);
        }
    }

    /**
     * Cleans history.
     */
    public void cleanHistory() {
        historyList.clear();
    }

    /**
     * @return Returns ArrayList containing the full history list.
     */
    public ArrayList<Folder> getHistoryListFull() {
        LOG.info("Returning History");
        return historyList;
    }

    /**
     * Wraps the history folder in SpecimenData, and PipelineMessage, and sends
     * it to the monitor with mailbox..
     *
     * @see SpecimenData
     * @see PipelineMessage
     * @see Mailbox
     */
    public void sendToMonitor() {
        LOG.info("Sending History to monitor.");
        getHistoryListFull().stream().forEach((folder) -> {
            try {
                if (folder.isValid()) {
                    System.out.println(folder.getName());
                    Future<PipelineReturnCode> future = new HistoryFutureAdapter();
                    SpecimenData specimenData = new SpecimenDataBuilder()
                            .setData(null)
                            .setFolder(folder)
                            .build();
                    PipelineMessage plm = new PipelineMessage(future, specimenData);

                    mailbox.put(plm);
                    LOG.info("History sent to monitor.");
                } else {
                    LOG.info(folder.getName() + " Was not valid folder, skipping from sending to monitor as history.");
                }
            } catch (Exception ex) {
                LOG.error("Failed to send History to monitor: " + ex);
            }
        });
    }

    /**
     * Merges old and new history, so that the new history wont override the old
     * one. Simply taked the old history as a list, and appends the new history
     * files to the end.
     */
    private void mergeOldAndNewHistory() {
        ArrayList<Folder> current = getHistoryListFull();
        loadHistoryFromFile();
        for (Folder f : current) {
            if (!this.historyList.contains(f)) {
                this.historyList.add(f);
            }
        }
    }

    /**
     * Saves the history to file as a plain text. Saves the history into a
     * "history" file that it then put into the root folder.
     */
    public void saveHistoryToFile() {
        mergeOldAndNewHistory();
        File f = new File(historyFilePath + "/history");
        LOG.info("History path: " + historyFilePath);
        try {
            f.createNewFile();
            try (FileWriter fw = new FileWriter(f)) {
                for (Folder fold : historyList) {
                    fw.append(fold.getName() + "\n");
                }
                fw.flush();
            }
        } catch (IOException ex) {
            LOG.error("IOException while saving history to file." + ex);
        }

    }

    /**
     * Loads history from the plain text file. Processes the plaintext Folder
     * name, into an image Folder object with ImageFolderReader.
     *
     * @see ImageFolderReader
     * @see Folder
     */
    public void loadHistoryFromFile() {
        LOG.info("Loading History from file...");
        File f = new File(historyFilePath + "/history");
        try (Scanner sc = new Scanner(f)) {
            while (sc.hasNextLine()) {
                String nextLine = sc.nextLine().trim();
                try {
                    ImageFolderReader ifr = new ImageFolderReader();
                    Folder newFolder = ifr.createFolderObject(new File(historyFilePath + "/" + nextLine));
                    if (!historyList.contains(newFolder)) {
                        historyList.add(newFolder);
                    }
                } catch (Exception ex) {
                    LOG.info("Couldn't create folder for history." + ex);
                }
            }
            LOG.info("Loaded History with size: " + historyList.size());

        } catch (IOException ex) {
            LOG.error("IOException while reading history from file." + ex);
        }
    }
}
