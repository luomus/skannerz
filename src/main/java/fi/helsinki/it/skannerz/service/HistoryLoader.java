package fi.helsinki.it.skannerz.service;

import fi.helsinki.it.skannerz.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads the history. Handles loading the history when starting the program.
 *
 * @author Peter, psmaatta
 */
public class HistoryLoader {

    private static final Logger LOG = LoggerFactory.getLogger(HistoryLoader.class);

    /**
     * Loads the history, and sends it to the monitor.
     */
    public void loadHistory() {
        History history = new History();
        try {
            String rootFolderPath = Configuration.getRootFolderPath();
            LOG.info("Creating History from " + rootFolderPath);
            history.setPath(rootFolderPath);
            history.loadHistoryFromFile();
            history.sendToMonitor();
        } catch (Exception ex) {
            LOG.error("Sending history to monitor failed", ex);
        }
    }
}
