package fi.helsinki.it.skannerz.service;

import java.util.Set;

/**
 * Interface for defining mailbox implementations. 
 * 
 * @param <T> type of the message.
 * @author riku
 */
public interface MailboxInterface<T> {
    /**
     * Puts message into the mailbox.
     * 
     * It depends on the implementation whether or not to notify the receivers
     * during an execution of this method.
     * 
     * @param message Message that'll be put into the mailbox.
     */
    void put(T message);

    /**
     * Add's a new receiver for the incoming mail that's put into the mailbox by @put method.
     * 
     * @param receiver Receiver we are going to register for listening incoming messages.
     * @param handleMsgQueue Whether to notify the just registered receiver about old messages in queue.
     */
    void addReceiver(MailboxReceiver<T> receiver, boolean handleMsgQueue);
    
    /**
     * Callback method for receivers of this mailbox to get contents of 
     * this mailbox. 
     * 
     * @param caller Invoker of this method.
     */
    void handleMail(MailboxReceiver<T> caller);

    /**
     * Removes the receiver from the receivers list.
     * 
     * @param receiver MailboxReceiver to be removed from the list.
     */
    void removeReceiver(MailboxReceiver<T> receiver);

    /**
     * Get a copy of the receivers.
     * @return copy of the receivers.
     */
    Set<MailboxReceiver<T>> getReceivers();
}
