package fi.helsinki.it.skannerz.service;

/**
 * Implementors of this interface can be added as a receiver for
 * mailboxes.
 *
 * @author riku
 */
public interface MailboxReceiver<T> {
    /**
     * This is the action invoked by the mailbox this receiver is 
     * registered as a receiver.
     * @param message 
     */
    void handleMail(T message);
}
