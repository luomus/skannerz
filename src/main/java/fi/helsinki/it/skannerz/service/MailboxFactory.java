/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.service;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author riku
 */
public class MailboxFactory {

    private static final Map<Class, MailboxInterface> MAILBOXES = new HashMap<>();

    public static <T> void addMailbox(Class<T> clazz, MailboxInterface<T> mailbox) {
        MAILBOXES.put(clazz, mailbox);
    }

    public static <T> MailboxInterface<T> getMailbox(Class<T> clazz) {
        return (MailboxInterface<T>) MAILBOXES.get(clazz);
    }

}
