package fi.helsinki.it.skannerz.utils.api.laji.fi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fi.helsinki.it.skannerz.domain.IntellectualRight;
import java.lang.reflect.Type;
import java.util.List;

/**
 * IntellectualRighsTransformer transforms the raw Json response from api.laji.fi
 * to List of IntellectualRights objects. 
 * 
 * @author riku
 * @see ResponseTransformer
 */
public class IntellectualRightsTransformer implements ResponseTransformer<List<IntellectualRight>> {

    private static final Gson GSON = new GsonBuilder().create();

    @Override
    public List<IntellectualRight> transform(String source) {
        List<IntellectualRight> intellectualRights;
        Type listType = new TypeToken<List<IntellectualRight>>(){}.getType();
        intellectualRights = GSON.fromJson(source, listType);
        return intellectualRights;
    }
}
