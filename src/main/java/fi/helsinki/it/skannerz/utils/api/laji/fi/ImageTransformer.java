package fi.helsinki.it.skannerz.utils.api.laji.fi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * ImageTransformer transforms the raw response from api.laji.fi to 
 * a Map. 
 *
 * @author riku
 * @see ResponseTransformer
 */
public class ImageTransformer implements ResponseTransformer<Map<String, String>> {

    private static final Gson GSON = new GsonBuilder().create();
    private static final Set<String> wantedLabels = new HashSet<>(Arrays.asList(
        "MZ.intellectualOwner", "MZ.intellectualRights",
        "MM.keyword", "MZ.publicityRestrictions"
    ));

    @Override
    public Map<String, String> transform(String source) {
        Map<String, String> results = new HashMap<>();
        JsonParser parser = new JsonParser();
        JsonElement root = parser.parse(source);
        JsonObject rootObj = (JsonObject) root;
        JsonArray resultsArr = (JsonArray) rootObj.get("results");
        for (JsonElement elem : resultsArr) {
            JsonObject elemObj = (JsonObject) elem;
            String property = elemObj.get("property").getAsString();
            if (wantedLabels.contains(property)) {
                String label = elemObj.get("label").getAsString();
                results.put(property, label);
            }
        }
        return results;
    }

}
