package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.utils.PathCreator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.CacheRequest;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * CacheRequest implementation for connections to api.laji.fi.
 *
 * @author riku
 * @see https://docs.oracle.com/javase/8/docs/api/java/net/CacheRequest.html
 * @see java.net.CacheRequest
 * 
 */
public class ApiLajiFiCacheRequest extends CacheRequest {
    private static final Logger LOG = LoggerFactory.getLogger(ApiLajiFiCacheRequest.class);
    private final String destination;
    private File cacheDestination;
    private OutputStream cacheOutputStream;


    public ApiLajiFiCacheRequest(String destination) {
        Validate.notNull(destination);
        this.destination = destination;
    }

    public ApiLajiFiCacheRequest(File cacheDirectory, String destination, String requestMethod) {
        Validate.notNull(destination);
        Validate.notNull(requestMethod);
        this.destination = destination;
        this.cacheDestination = new File(
            PathCreator.create(
                cacheDirectory.getAbsolutePath(),
                Digester.getHexString("SHA-256", this.destination),
                requestMethod,
                "body"
            )
        );
    }

    @Override
    public OutputStream getBody() throws IOException {
       File parent = cacheDestination.getParentFile();
       parent.mkdirs();
       cacheOutputStream = new FileOutputStream(cacheDestination);
       return cacheOutputStream;
    }

    @Override
    public void abort() {
        File rqstMethodDir = cacheDestination.getParentFile();
        File shaDir = rqstMethodDir = cacheDestination.getParentFile();
        if (cacheDestination.exists()) {
            cacheDestination.delete();
        }
        rqstMethodDir.delete();
        shaDir.delete();
    }

}
