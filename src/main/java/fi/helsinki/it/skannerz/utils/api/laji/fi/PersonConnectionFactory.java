package fi.helsinki.it.skannerz.utils.api.laji.fi;

import java.util.Map;

/**
 * Implementation of ConnectionFactory for querying the Person parameters 
 * for the form.
 *
 * @author riku
 */
public class PersonConnectionFactory implements ConnectionFactory<Map<String, String>> {

    private final String url;
    private final String lang;

    public PersonConnectionFactory(String url, String lang) {
        this.url = url;
        this.lang = lang;
    }

    @Override
    public LajiApiConnection createConnection(String token) {
        return new LajiApiConnection.Builder("GET", token, url)
            .parameter("lang", lang)
            .build();
    }
    
}
