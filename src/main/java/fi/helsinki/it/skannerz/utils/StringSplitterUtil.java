
package fi.helsinki.it.skannerz.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author waxwax
 */
public class StringSplitterUtil {

    public static List<String> getListOf(String string) {
        String regex = "(\n|,|;)";
        String[] split = string.split(regex);
        List<String> tags = new ArrayList<>(split.length);
        for (String tag : split) {
            String trim = tag.trim();
            if (!trim.isEmpty()) {
                tags.add(trim);
            }
        }
        return tags;
    }
}
