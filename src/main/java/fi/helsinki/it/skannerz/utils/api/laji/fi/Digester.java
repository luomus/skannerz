package fi.helsinki.it.skannerz.utils.api.laji.fi;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for transforming Strings into a form of hex strings of 
 * cryptographic hashes. At the moment of this writing, this is used
 * at least in individual paths of cache requests/responses.
 * 
 * @author riku
 */
public class Digester {
    private static final Logger LOG = LoggerFactory.getLogger(Digester.class);

    public static String getHexString(String algo, String source) {
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance(algo);
            byte[] results = md.digest(source.getBytes());
            for (byte b : results) 
                sb.append(String.format("%x", b));
        } catch (NoSuchAlgorithmException nsae) {
            LOG.error("{}", nsae);
        }
        return sb.toString();
    }
    
}
