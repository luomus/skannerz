package fi.helsinki.it.skannerz.utils.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import org.joda.time.DateTime;

/**
 * Deserializes a timestamp to a DateTime object.
 * 
 * @author perdu, Walter Grönholm
 * @see com.google.gson.JsonDeserializer
 */
public class DateTimeDeserializer implements JsonDeserializer<DateTime> {
    
    @Override
    public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new DateTime(json.getAsJsonPrimitive().getAsString());
    }
}
