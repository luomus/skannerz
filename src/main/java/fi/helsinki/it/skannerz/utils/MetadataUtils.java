package fi.helsinki.it.skannerz.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.utils.gson.DateTimeDeserializer;
import fi.helsinki.it.skannerz.utils.gson.DateTimeSerializer;
import fi.luomus.kuvapalvelu.client.model.Meta;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author psmaatta
 */
public class MetadataUtils {

    private static final Logger LOG = LoggerFactory.getLogger(MetadataUtils.class);
    private static final Gson GSON = metadataGsonBuilder();

    private MetadataUtils() {
    }

    /**
     * Merges the oldMeta and newMeta objects into a new ExtendedMeta object.
     * The values in oldMeta are overwritten by the newMeta. In case of lists,
     * the oldMeta list is replaced by the newMeta list.
     *
     * @param oldMeta
     * @param newMeta
     * @return
     */
    public static ExtendedMeta merge(ExtendedMeta oldMeta, ExtendedMeta newMeta) {
        Validate.notNull(oldMeta);
        Validate.notNull(newMeta);
        ExtendedMeta merged = new ExtendedMeta();
        List<String> tags = new ArrayList<>();
        tags.addAll(newMeta.getTags() == null
                ? oldMeta.getTags() : newMeta.getTags());
        merged.setTags(tags);
        List<String> documentIds = new ArrayList<>();
        documentIds.addAll(newMeta.getDocumentIds() == null
                ? oldMeta.getDocumentIds() : newMeta.getDocumentIds());
        merged.setDocumentIds(documentIds);

        merged.setRightsOwner(StringUtils.isEmpty(newMeta.getRightsOwner())
                ? oldMeta.getRightsOwner() : newMeta.getRightsOwner());
        merged.setLicense(StringUtils.isEmpty(newMeta.getLicense())
                ? oldMeta.getLicense() : newMeta.getLicense());
        merged.setUploadedBy(StringUtils.isEmpty(newMeta.getUploadedBy())
                ? oldMeta.getUploadedBy() : newMeta.getUploadedBy());
        merged.setSecret(newMeta.getSecret() == null
                ? oldMeta.getSecret() : newMeta.getSecret());
        merged.setDomain(newMeta.getDomain() == null
                ? oldMeta.getDomain() : newMeta.getDomain());
        merged.setImageName(newMeta.getImageName() == null
                ? oldMeta.getImageName() : newMeta.getImageName());
        merged.setUploadedId(newMeta.getUploadedId() == null
                ? oldMeta.getUploadedId() : newMeta.getUploadedId());
        merged.setLastUpdated(newMeta.getLastUpdated() == null
                ? oldMeta.getLastUpdated() : newMeta.getLastUpdated());
        return merged;
    }

    /**
     * Clones a new ExtendedMeta object from the meta given.
     * @param meta
     * @return 
     */
    public static ExtendedMeta clone(ExtendedMeta meta) {
        Validate.notNull(meta);
        ExtendedMeta clone = new ExtendedMeta();
        List<String> tags = new ArrayList<>();
        tags.addAll(meta.getTags());
        List<String> documentIds = new ArrayList<>();
        documentIds.addAll(meta.getDocumentIds());

        clone.setRightsOwner(meta.getRightsOwner());
        clone.setLicense(meta.getLicense());
        clone.setTags(meta.getTags() == null ? null : tags);
        clone.setUploadedBy(meta.getUploadedBy());
        clone.setSecret(meta.getSecret());
        clone.setDomain(meta.getDomain());
        clone.setImageName(meta.getImageName());
        clone.setUploadedId(meta.getUploadedId());
        clone.setDocumentIds(meta.getDocumentIds() == null ? null : documentIds);
        clone.setLastUpdated(meta.getLastUpdated());
        return clone;
    }
    
    public static void saveMetadata(ExtendedMeta meta, File metadataJson) throws Exception {
        Validate.notNull(meta);
        Validate.notNull(metadataJson);
        meta.setLastUpdated(new DateTime());
        String json = GSON.toJson(meta);
        LOG.debug("Saving meta for file " + metadataJson.getName() + ":\n" + json);
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(metadataJson), "UTF-8")) {
            writer.append(json);
            writer.write(System.lineSeparator());
        }
    }

    public static String serializeMetadata(ExtendedMeta meta) {
        Validate.notNull(meta);
        return GSON.toJson(meta);
    }

    public static ExtendedMeta getMetadata(File metadataJson) throws IOException {
        if (!metadataJson.isFile()) {
            throw new IllegalStateException("Metadata " + metadataJson + " doesn't exists");
        }
        try (JsonReader reader = new JsonReader(new FileReader(metadataJson));) {
            reader.setLenient(true);
            return GSON.fromJson(reader, ExtendedMeta.class);
        }
    }

    private static Gson metadataGsonBuilder() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeDeserializer());
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeSerializer());
        return gsonBuilder.create();
    }

    public static Meta convertMeta(Meta oldMeta) {
        Meta newMeta = new Meta();

        List<String> tags = new ArrayList<>();
        tags.addAll(oldMeta.getTags());

        newMeta.setRightsOwner(oldMeta.getRightsOwner());
        newMeta.setLicense(oldMeta.getLicense());
        newMeta.setTags(tags);
        newMeta.setUploadedBy(oldMeta.getUploadedBy());
        newMeta.setSecret(oldMeta.getSecret());
        newMeta.setSourceSystem(oldMeta.getSourceSystem());
        newMeta.setWgs84Coordinates(oldMeta.getWgs84Coordinates());
        newMeta.setCaptureDateTime(oldMeta.getCaptureDateTime());
        newMeta.setDocumentIds(oldMeta.getDocumentIds());

        return newMeta;
    }

}
