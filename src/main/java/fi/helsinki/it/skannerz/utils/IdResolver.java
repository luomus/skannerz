package fi.helsinki.it.skannerz.utils;

import com.google.zxing.ReaderException;
import java.io.IOException;
import java.net.URI;

/**
 * Interface for resolving LUOMUS id's. Created by riku on 12.9.2017.
 */
public interface IdResolver {

    /*
     * Resolves an identifier decoded from a file which is given as a parameter.
     *
     * @param uri  URI object where the identifier should be read.
     * @return String  the resolved identifier
     * @throws ReaderException when the id can not be read or identified
     * from the source.
     * @throws IOException when the URI is malformed or null.
     */
    String getIdentifier(URI uri) throws ReaderException, IOException;
}
