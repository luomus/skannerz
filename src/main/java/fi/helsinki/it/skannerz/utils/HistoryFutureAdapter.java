/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.helsinki.it.skannerz.utils;

import fi.helsinki.it.skannerz.domain.PipelineReturnCode;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author porttpet
 */
public class HistoryFutureAdapter implements Future {

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return true;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return true;
    }

    @Override
    public PipelineReturnCode get() throws InterruptedException, ExecutionException {
        return PipelineReturnCode.HISTORY;
    }

    @Override
    public PipelineReturnCode get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return PipelineReturnCode.HISTORY;
    }

}
