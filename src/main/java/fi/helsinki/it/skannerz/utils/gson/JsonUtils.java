package fi.helsinki.it.skannerz.utils.gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Contains convenience methods for retrieving data from JSON.
 *
 * @author walter, riku
 */
public class JsonUtils {
    
    /**
     * @param jsonObject the object containing the array
     * @param fieldName the field/key name of the array
     * @return the JsonArray from the field with the given fieldName
     * @throws IllegalStateException if the field is not an array
     */
    public static JsonArray safeGetArray(JsonObject jsonObject, String fieldName) {
        return jsonObject == null || !jsonObject.has(fieldName) ? null
            : jsonObject.get(fieldName).getAsJsonArray();
    }

    /**
     * @param jsonObject the object containing the string
     * @param fieldName the field/key name of the string
     * @return the String value from the field with the given fieldName
     * @throws IllegalStateException if the field is not a String
     */
    public static String safeGetString(JsonObject jsonObject, String fieldName) {
        return jsonObject == null || !jsonObject.has(fieldName) ? null
            : jsonObject.get(fieldName).getAsString();
    }

}
