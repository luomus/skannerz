package fi.helsinki.it.skannerz.utils.api.laji.fi;

/**
 * 
 * Defines an action to build URL of the request and possibly prepares
 * other parameters of the request.
 *
 * @see IntellectualRightsConnectionFactory
 * @author riku
 * 
 */
public interface ConnectionFactory<T> {

    /**
     * Creates the LajiApiConnection object used to make the actual
     * connection to api.laji.fi.
     * 
     * @param token
     * @return LajiApiConnection instance for this connection.
     */
    LajiApiConnection createConnection(String token);
    
}
