package fi.helsinki.it.skannerz.utils.api.laji.fi;

import java.io.IOException;
import java.io.InputStream;
import java.net.SecureCacheResponse;
import java.net.URI;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLPeerUnverifiedException;

/**
 * Class extending abstract class of Java's web cache SecureCacheResponse
 * is needed for connections made using SSL/TLS.
 * 
 * SSL/TLS methods are never invoked during the connection as we only care
 * about the response's body.
 *
 * @author riku
 */
public class ApiLajiFiSecureCacheResponse extends SecureCacheResponse {

    private final ApiLajiFiCacheResponse cacheResponse;

    public ApiLajiFiSecureCacheResponse(ApiLajiFiCacheResponse cacheResponse) {
        this.cacheResponse = cacheResponse;
    }

    @Override
    public Map<String, List<String>> getHeaders() throws IOException {
        return cacheResponse.getHeaders();
    }

    @Override
    public InputStream getBody() throws IOException {
        return cacheResponse.getBody();
    }

    // SSL/TLS methods are not needed for our purposes.

    @Override
    public String getCipherSuite() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<Certificate> getLocalCertificateChain() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Certificate> getServerCertificateChain() throws SSLPeerUnverifiedException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Principal getLocalPrincipal() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
