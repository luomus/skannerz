package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.config.Configuration;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LajiApiEndpoint combines one ConnectionFactory instance and one
 * ResponseTransformer instance to a LajiApiEndpoint.
 *
 * @author riku
 */
public class LajiApiEndPoint<T> {
    private static final Logger LOG = LoggerFactory.getLogger(LajiApiEndPoint.class);

    private final ConnectionFactory<T> connectionFactory;
    private final ResponseTransformer<T> responseTransformer;

    public LajiApiEndPoint(ConnectionFactory<T> connectionFactory, 
        ResponseTransformer<T> responseTransformer) {
        this.connectionFactory = connectionFactory;
        this.responseTransformer = responseTransformer;
    }

    /**
     * Fetches something from api.laji.fi.
     * 
     * @return T representing transformed response from api.laji.fi.
    */
    public T get() {
        String token = Configuration.getApiLajiFiAccessToken();

        LajiApiConnection connection = connectionFactory.createConnection(token);

        T results;
        try {
            LOG.info("Fetching data from api.laji.fi.");
            results = responseTransformer.transform(connection.getResult());
            LOG.info("Fetched Data from api.laji.fi succesfully.");
        } catch (FileNotFoundException fe) {
            LOG.error("Didn't find anything from that URL. Are you sure it's correct? ");
            LOG.error("connection: " + connection.toString());
            throw new RuntimeException(fe);
        } catch (IOException ioe) {
            LOG.error("Couldn't fetch data from laji.api.fi.");
            LOG.error(ioe.toString());
            throw new RuntimeException(ioe);
        } 
        return results;
    }

}
