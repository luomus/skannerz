package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.domain.IntellectualRight;
import java.util.List;

/**
 * Implementation of ConnectionFactory for querying the intellectual rights
 * list of the form.
 *
 * @author riku
 */
public class IntellectualRightsConnectionFactory implements ConnectionFactory<List<IntellectualRight>> {

    private final String url;

    public IntellectualRightsConnectionFactory(String url) {
        this.url = url;
    }

    @Override
    public LajiApiConnection createConnection(String token) {
        return new LajiApiConnection.Builder("GET", token, url)
            .header("Accept", "application/json")
            .parameter("lang", "en")
            .parameter("asLookUpObject", "false")
            .parameter("classTypeAsList", "false")
            .build();
    }

}
