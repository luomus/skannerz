package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.utils.PathCreator;
import java.io.File;
import java.io.IOException;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.HttpURLConnection;
import java.net.ResponseCache;
import java.net.URI;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Implementation of Java's web cache for connections with destination of 
 * api.laji.fi. This web cache implementation does not honor or notice
 * the possible HTTP cache headers and only caches request's/response's 
 * of api.laji.fi. 
 *
 * @author riku
 */
public class ApiLajiFiResponseCache extends ResponseCache {
    private static final Logger LOG = LoggerFactory.getLogger(ApiLajiFiResponseCache.class);
    private File cacheDirectory;

    public ApiLajiFiResponseCache(String cacheDirectoryPath) {
        Validate.notNull(cacheDirectoryPath);
        this.cacheDirectory = new File(cacheDirectoryPath);
        if (cacheDirectory.isFile()) {
            throw new IllegalArgumentException("Cache directory (" + cacheDirectory + ") can't be a regular file.");
        }
        LOG.info("Cache directory: {}", cacheDirectory.getAbsolutePath());
        if (!cacheDirectory.exists()) {
            cacheDirectory.mkdirs();
        }
    }

    /**
     * Fetches the response from cache instead of making an actual connection
     * in case the URI and HTTP request method has a match in the cache.
     * @param uri 
     * @param rqstMethod HTTP Request method
     * @param rqstHeaders HTTP Request headers
     * @return CacheResponse
     * @throws IOException 
     */
    @Override
    public CacheResponse get(URI uri, String rqstMethod, Map<String, List<String>> rqstHeaders) throws IOException {
        if (!uri.toASCIIString().matches("https?://api.laji.fi.*")) {
            return null;
        }

        boolean isSecure = uri.getScheme().equals("https");
        String location = PathCreator.create(
            cacheDirectory.getAbsolutePath(), 
            Digester.getHexString("SHA-256", uri.toASCIIString()), 
            rqstMethod,
            "body"
        );

        LOG.info("Trying to fetch from cache for {}, {}", uri.toASCIIString(), rqstMethod);
        LOG.info("The cache location for it should be {}.", location);

        File cacheFile = new File(location);
        if (cacheFile.exists() && 
            System.currentTimeMillis() - cacheFile.lastModified() < 60*60*1000) {
            LOG.info("Cache hit for {}.", location);

            ApiLajiFiCacheResponse cacheResponse = 
                new ApiLajiFiCacheResponse(cacheDirectory, uri.toASCIIString(), rqstMethod, rqstHeaders);
            if (isSecure) {
                return new ApiLajiFiSecureCacheResponse(cacheResponse);
            }
            return cacheResponse;
        }

        LOG.info("Didn't fetch anything from cache for {}.", location);
        return null;
    }

    /**
     * If no cache hit and the connection was made to api.laji.fi, it'll
     * save it to our local cache.
     * @param uri
     * @param conn
     * @return
     * @throws IOException 
     */
    @Override
    public CacheRequest put(URI uri, URLConnection conn) throws IOException {
        if (uri.toASCIIString().matches("https?://api.laji.fi.*")) {
            return new ApiLajiFiCacheRequest(
                cacheDirectory,
                uri.toASCIIString(), 
                ((HttpURLConnection) conn).getRequestMethod()
            );
        }
        return null;
    }

}
