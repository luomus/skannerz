package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.utils.QueryStringBuilder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * LajiApiConnection is a class for creating connections to api.laji.fi.
 * 
 * LajiApiConnection can't be instantiated directly and you have to use
 * the LajiApiConnection.Builder class to set up and initialize an instance.
 *
 * @author riku
 * @see IntellectualRightsConnectionFactory
 */
public class LajiApiConnection {

    private static final Logger LOG = LoggerFactory.getLogger(LajiApiConnection.class);

    private final String requestMethod;
    private final String queryString;
    private final URL url;
    private final URLConnection connection;
    private final Set<Integer> successResponseCodes;
    private final Set<Integer> errorResponseCodes;
    private Map<String, List<String>> headers;

    private LajiApiConnection(Builder builder) {
        this.requestMethod = builder.requestMethod;
        this.successResponseCodes = builder.successResponseCodes;
        this.errorResponseCodes = builder.errorResponseCodes;
        this.queryString = builder.queryStringBuilder.toString();

        try {
            url = new URL(buildURLString(builder.path, queryString));
        } catch (MalformedURLException mue) {
            LOG.error("URL path is invalid: "
                + buildURLString(builder.path,
                    builder.queryStringBuilder.toString())
            );
            throw new IllegalArgumentException("URL isn't valid.");
        }

        connection = initializeConnection(builder);

    }

    public String getQueryString() {
        return this.queryString;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public URL getUrl() {
        return url;
    }

    public Set<Integer> getSuccessResponseCodes() {
        return successResponseCodes;
    }

    public Set<Integer> getErrorResponseCodes() {
        return errorResponseCodes;
    }

    public String getHeaderField(String key) {
        return connection.getRequestProperty(key);
    }

    /**
     * Gets the response from api.laji.fi.     * 
     * 
     * @throws IOException if Responses response code indicated failure or is unknown.
     * @return LajiApiResult<T> where T is a class representing the data received.
    */
    public String getResult() throws IOException {
        if (requestMethod.equals("POST")) {
            writeToBody(queryString);
        }
        InputStream input;
        connection.setConnectTimeout(2000);
        input = new BufferedInputStream(connection.getInputStream());

        List<Byte> results = new ArrayList<>();
        for (int read = input.read(); read != -1; read = input.read()) {
            results.add((byte) read);
        }

        if (connection instanceof HttpURLConnection) {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;

            Integer responseCode = httpConnection.getResponseCode();
            if (errorResponseCodes.contains(responseCode) || !successResponseCodes.contains(responseCode)) {
                throw new IOException("Bad response (" + (responseCode) + ") from api.laji.fi.");
            }
            httpConnection.disconnect();
        }

        byte[] data = new byte[results.size()];
        for (int i = 0; i < results.size(); data[i] = results.get(i), i++)
            ;
        return new String(data, "UTF-8");
    }

    private URLConnection initializeConnection(Builder builder) {
        try {
            URLConnection urlConnection = url.openConnection();
            for (String key : builder.headers.keySet()) {
                urlConnection.setRequestProperty(key, builder.headers.get(key));
            }

            if (urlConnection instanceof HttpURLConnection) {
                ((HttpURLConnection) urlConnection).setRequestMethod(requestMethod);
            }

            return urlConnection;
        } catch (IOException ioe) {
            LOG.error("IOException while initializing connection.");
            LOG.error(ioe.toString());
        }
        return null;
    }

    private String buildURLString(String path, String queryString) {
        if (requestMethod.equals("GET")) {
            return path + "?" + queryString;
        }
        return path;
    }

    private void writeToBody(String queryString) {
        connection.setDoOutput(true);
        try {
            OutputStream out = new BufferedOutputStream(connection.getOutputStream());
            OutputStreamWriter writer = new OutputStreamWriter(out);
            writer.write(queryString);
            writer.flush();
            writer.close();
        } catch (IOException ioe) {
            LOG.error("Couldn't write parameters to request body.");
            LOG.error(ioe.toString());
        }
    }

    public static class Builder {

        private final String requestMethod;
        private final String path;
        private final QueryStringBuilder queryStringBuilder;

        private final Map<String, String> headers;
        private final Set<Integer> successResponseCodes;
        private final Set<Integer> errorResponseCodes;

        public Builder(String requestMethod, String accessToken, String path) {
            this.requestMethod = requestMethod;
            this.path = path;
            this.queryStringBuilder = new QueryStringBuilder();
            this.queryStringBuilder.add("access_token", accessToken);
            this.headers = new HashMap<>();
            this.successResponseCodes = new HashSet<>();
            this.successResponseCodes.add(200);
            // -1 means that the results are from local cache.
            this.successResponseCodes.add(-1);
            this.errorResponseCodes = new HashSet<>();
            this.errorResponseCodes.add(401);
        }

        public Builder header(String name, String value) {
            headers.put(name, value);
            return this;
        }

        public Builder parameter(String name, String value) {
            queryStringBuilder.add(name, value);
            return this;
        }

        public Builder successResponseCode(Integer ... responseCodes) {
            this.successResponseCodes.addAll(Arrays.asList(responseCodes));
            return this;
        }

        public Builder errorResponseCode(Integer ... responseCodes) {
            this.errorResponseCodes.addAll(Arrays.asList(responseCodes));
            return this;
        }

        public LajiApiConnection build() {
            return new LajiApiConnection(this);
        }
    }

}
