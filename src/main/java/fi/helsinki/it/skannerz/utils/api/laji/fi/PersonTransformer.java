package fi.helsinki.it.skannerz.utils.api.laji.fi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fi.helsinki.it.skannerz.domain.Person;
import java.util.HashMap;
import java.util.Map;

/**
 * Transforms the raw Json response got as a response from api.laji.fi
 * to a Map.
 *
 * @author riku
 * @see ResponseTransformer
 */
public class PersonTransformer implements ResponseTransformer<Map<String, String>> {

    private static final Gson GSON = new GsonBuilder().create();


    @Override
    public Map<String, String> transform(String source) {
        Map<String, String> results = new HashMap<>();
        Person person = GSON.fromJson(source, Person.class);
        results.put("MA.person", person.getLabel());
        return results;
    }
    
}
