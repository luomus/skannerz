package fi.helsinki.it.skannerz.utils.api.laji.fi;

import fi.helsinki.it.skannerz.utils.PathCreator;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.CacheResponse;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for cache results.
 *
 * @author riku
 * @see https://docs.oracle.com/javase/8/docs/api/java/net/CacheResponse.html
 * @see java.net.CacheResponse
 */
public class ApiLajiFiCacheResponse extends CacheResponse {
    private static final Logger LOG = LoggerFactory.getLogger(ApiLajiFiCacheResponse.class);
    private final String destination;
    private final String rqstMethod;
    private final Map<String, List<String>> headers;
    private final File cacheDirectory;

    public ApiLajiFiCacheResponse(File cacheDirectory, String destination, String rqstMethod, Map<String, List<String>> headers) {
        Validate.notNull(destination);
        Validate.notNull(cacheDirectory);
        this.destination = destination;
        this.rqstMethod = rqstMethod;
        this.headers = headers;
        this.cacheDirectory = cacheDirectory;
    }

    @Override
    public Map<String, List<String>> getHeaders() throws IOException {
        return headers;
    }

    @Override
    public InputStream getBody() throws IOException {
        LOG.info("Getting FileInputStream from cache.");
        InputStream fis = new FileInputStream(
            PathCreator.create(
                cacheDirectory.getAbsolutePath(),
                Digester.getHexString("SHA-256", destination),
                rqstMethod,
                "body"
            )
        );
        LOG.info("Bytes available from cache: {}.", fis.available());

        return fis;
    }
    
}
