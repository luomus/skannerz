package fi.helsinki.it.skannerz.utils;

/**
 *
 * @author riku
 */
public class PathCreator {

    public static String create(String... parts) {
        StringBuilder results = new StringBuilder();
        for (String part : parts) {
            results.append(part).append(System.getProperty("file.separator"));
        }
        return results.toString();
    }
    
}
