package fi.helsinki.it.skannerz.utils.api.laji.fi;

/**
 * 
 * Classes implementing this are able to transform textual data received
 * from api.laji.fi to dataholder classes representing that textual data.
 *
 * @author riku
 */
public interface ResponseTransformer<T> {

    /**
     * Transforms the given source String (possibly Json etc.) to an object
     * of type T.
     * 
     * @param source
     * @return transformed object of type T.
     */
    T transform(String source);
    
}
