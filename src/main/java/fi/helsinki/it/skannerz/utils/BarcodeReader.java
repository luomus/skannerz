package fi.helsinki.it.skannerz.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.ImageReader;
import com.google.zxing.common.HybridBinarizer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by riku on 12.9.2017.
 */
public class BarcodeReader implements IdResolver {

    private static final Logger LOG = LoggerFactory.getLogger(BarcodeReader.class);
    private static final Object SCALE_LOCK = new Object();

    private final Reader reader;

    public BarcodeReader(Reader reader) {
        this.reader = reader;
    }

    @Override
    public String getIdentifier(URI uri) throws ReaderException, IOException {
        Validate.notNull(uri);

        LOG.info("Reading from URI: " + uri);
        BufferedImage bufferedImage = ImageReader.readImage(uri);
        // 1500 * 1000 seems to be a fine size for reading qr images.
        if (bufferedImage.getWidth() * bufferedImage.getHeight() > 1500 * 1000) {
            long start = System.currentTimeMillis();
            bufferedImage = getScaledImage(bufferedImage);
            LOG.info("Scaling image " + uri + " took " + (System.currentTimeMillis() - start) + " ms.");
        }
        return decode(getBitmapOf(bufferedImage), uri);
    }

    private BinaryBitmap getBitmapOf(BufferedImage bufferedImage) {
        LuminanceSource lsource = new BufferedImageLuminanceSource(bufferedImage);
        Binarizer binarizer = new HybridBinarizer(lsource);
        BinaryBitmap bitmap = new BinaryBitmap(binarizer);
        return bitmap;
    }

    private String decode(BinaryBitmap bitmap, URI uri) throws FormatException, ChecksumException, NotFoundException {
        Map<DecodeHintType, Object> hints = getHints();
        synchronized (this) {
            long start = System.currentTimeMillis();
            Result result = reader.decode(bitmap, hints);
            reader.reset();
            LOG.info(uri + " Text we found: " + result.getText());
            LOG.info("QR scanning took " + (System.currentTimeMillis() - start));
            return result.getText();
        }
    }

    private Map<DecodeHintType, Object> getHints() {
        Map<DecodeHintType, Object> hints = new HashMap<>();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        List<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, formats);
        return hints;
    }

    private BufferedImage getScaledImage(BufferedImage source) throws IOException {
        double aspectRatio = Math.min(1500.0 / source.getWidth(), 1500.0 / source.getHeight());
        synchronized (SCALE_LOCK) {
            LOG.info("Going to scale from " + source.getWidth() + " x " + source.getHeight()
                + " to " + (source.getWidth() * aspectRatio) + " x " + 
                (source.getHeight() * aspectRatio)
            );
            AffineTransform at = new AffineTransform();
            at.scale(aspectRatio, aspectRatio);
            AffineTransformOp atop = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            source = atop.filter(source, null);
        }
        return source;
    }

}
