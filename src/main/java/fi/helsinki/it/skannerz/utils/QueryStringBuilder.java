package fi.helsinki.it.skannerz.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Little helper class for building HTTP query strings.
 * Values of the key-value pairs of HTTP query strings have to be
 * x-www-url-encoded (percent encoding).
 *
 * @author riku
 */
public class QueryStringBuilder {
    
    private static final Logger LOG = LoggerFactory.getLogger(QueryStringBuilder.class);
    private StringBuilder builder;

    public QueryStringBuilder() {
        this.builder = new StringBuilder();
    }

    public QueryStringBuilder add(String name, String value) {
        if (builder.length() != 0) {
            builder.append("&");
        }
        builder.append(name).append("=");
        try {
            builder.append(URLEncoder.encode(value, "UTF-8"));
        } catch (UnsupportedEncodingException uee) {
            LOG.error("Couldn't append " + value + " to the query string due to unsupported encoding.");
            LOG.error(uee.toString());
        }
        return this;
    }

    @Override
    public String toString() {
        return builder.toString();
    }

}
