package fi.helsinki.it.skannerz;

import fi.helsinki.it.skannerz.config.Configuration;
import com.google.zxing.qrcode.QRCodeReader;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.service.HistoryLoader;
import fi.helsinki.it.skannerz.service.ImageFolderReader;
import fi.helsinki.it.skannerz.service.ImageFolderReaderInterface;
import fi.helsinki.it.skannerz.service.Mailbox;
import fi.helsinki.it.skannerz.service.MailboxFactory;
import fi.helsinki.it.skannerz.service.PipelineFactory;
import fi.helsinki.it.skannerz.service.PipelineManager;
import fi.helsinki.it.skannerz.service.PipelineManagerFactory;
import fi.helsinki.it.skannerz.service.RootFolderReaderFactory;
import fi.helsinki.it.skannerz.service.RootFolderReaderInterface;
import fi.helsinki.it.skannerz.utils.BarcodeReader;
import fi.helsinki.it.skannerz.utils.api.laji.fi.ApiLajiFiResponseCache;
import fi.helsinki.it.skannerz.webframework.Router;
import fi.helsinki.it.skannerz.webframework.controllers.ImageApiClientFactory;
import fi.helsinki.it.skannerz.webframework.freemarker.FreemarkerConfiguration;
import java.net.ResponseCache;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.servlet.SparkApplication;

/**
 * Copyright (c) 2017 University Of Helsinki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author TODO
 */
public class Skannerz implements SparkApplication {

    private static final Logger LOG = LoggerFactory.getLogger(Skannerz.class);
    private PipelineManager manager;
    /*
     * Note that server's tomcat does not execute main method at all. This is executed only 
     * by embedded tomcat used for developing and Spark's embedded jetty server. 
     * That is, this is only run when executing locally.
     * 
     * Tomcat starts it's execution by executing the init() method below.
     */
    public static void main(String[] args) throws Exception {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8081);
        tomcat.getHost().setAppBase(".");
        Context context = tomcat.addWebapp("/skannerz", ".");
        context.setAltDDName(Skannerz.class.getClassLoader().getResource("web.xml").getFile());

        StandardJarScanner jarScanner = (StandardJarScanner) context.getJarScanner();
        jarScanner.setScanClassPath(true); 
        // ^ needed for ServletContainerInitializators (such as websockets) among other things.

        tomcat.start();
        tomcat.getServer().await();
    }

    private static void loadConfiguration() {
        LOG.info("Loading properties...");
        try {
            Configuration.init("local.properties");
        } catch (Exception e) {
            LOG.error("Could not read configuration.", e);
            System.exit(0);
        }
    }

    /*
     * This is required to make Spark application execute in other web servers than its own.
     */
    @Override
    public void init() {
        LOG.info("Starting Skannerz...");
        loadConfiguration();
        initMailboxes();
        RootFolderReaderInterface rootFolderReader = startRootFolderReader(Configuration.getRootFolderPath());
        initPipelineManagerFactory(rootFolderReader);
        new HistoryLoader().loadHistory();

        ResponseCache.setDefault(
            new ApiLajiFiResponseCache(Configuration.getApiLajiFiCacheLocation())
        ); 

        manager = PipelineManagerFactory.get();
        manager.startPipelines();
        
        new Router.Builder()
            .setPipelineManager(manager)
            .setTemplateEngine(FreemarkerConfiguration.ENGINE)
            .build()
            .init(8090);
        
        
        LOG.info("...Done");
    }
    
    public void stopManager() {
        manager.stopPipelines();
    }

    private void initMailboxes() {
        MailboxFactory.addMailbox(PipelineMessage.class, new Mailbox<PipelineMessage>());
    }

    private void initPipelineManagerFactory(RootFolderReaderInterface rootFolderReader) {
        BarcodeReader qrReader = new BarcodeReader(new QRCodeReader());
        PipelineManager manager = new PipelineManager(rootFolderReader, qrReader, new PipelineFactory());
        manager.setImageApiClient(ImageApiClientFactory.getClient());
        PipelineManagerFactory.set(manager);
    }

    private RootFolderReaderInterface startRootFolderReader(String rootFolderPath) {
        ImageFolderReaderInterface imageFolderReaderInterface = new ImageFolderReader();
        RootFolderReaderInterface rootFolderReader = RootFolderReaderFactory.get(rootFolderPath, imageFolderReaderInterface);
        new Thread(rootFolderReader).start();
        return rootFolderReader;
    }

}
