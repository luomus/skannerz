package fi.helsinki.it.skannerz.webframework;

import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.utils.StringSplitterUtil;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import spark.QueryParamsMap;

/**
 * Converts data structures to Meta objects. It is assumed that the data
 * structures are valid.
 *
 * @author Walter Grönholm
 */
public class FormToMetaConverter implements FormToMetaConverterInterface {

    private static final Logger LOG = LoggerFactory.getLogger(FormToMetaConverter.class);

    @Override
    public ExtendedMeta convert(QueryParamsMap queryParamsMap) {
        ExtendedMeta meta = new ExtendedMeta();

        meta.setRightsOwner(getIntellectualOwner(queryParamsMap));
        meta.setLicense(getIntellectualRights(queryParamsMap));
        meta.setTags(getKeywords(queryParamsMap));
        meta.setUploadedBy(getPerson(queryParamsMap));
        meta.setDomain(getDomain(queryParamsMap));
        meta.setSecret(getPublicityRestrictions(queryParamsMap));

        return meta;
    }

    private static String getPerson(QueryParamsMap queryParamsMap) {
        String person = queryParamsMap.get("person").value().trim();
        LOG.debug("person=" + person);
        return person;
    }

    private static boolean getPublicityRestrictions(QueryParamsMap queryParamsMap) {
        String publicityRestrictions = queryParamsMap.get("publicityRestrictions").value().trim();
        LOG.debug("publicityRestrictions=" + publicityRestrictions);
        return !publicityRestrictions.toUpperCase().equals("PUBLIC");
    }

    private static String getIntellectualOwner(QueryParamsMap queryParamsMap) {
        String intellectualOwner = queryParamsMap.get("intellectualOwner").value().trim();
        LOG.debug("intellectualOwner=" + intellectualOwner);
        return intellectualOwner;
    }

    private static String getIntellectualRights(QueryParamsMap queryParamsMap) {
        String intellectualRights = queryParamsMap.get("intellectualRights").value().trim();
        LOG.debug("intellectualRights=" + intellectualRights);
        return intellectualRights;
    }

    private static List<String> getKeywords(QueryParamsMap queryParamsMap) {
        List<String> tags = StringSplitterUtil.getListOf(queryParamsMap.get("keywords").value());
        LOG.debug("keywords=" + tags);
        return tags;
    }
    
    private static String getDomain(QueryParamsMap queryParamsMap) {
        String domain = queryParamsMap.get("domain").value().trim();
        LOG.debug("domain=" + domain);
        return domain;
    }

}
