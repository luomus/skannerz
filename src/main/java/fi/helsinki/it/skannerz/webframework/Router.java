package fi.helsinki.it.skannerz.webframework;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.service.PipelineManager;
import fi.helsinki.it.skannerz.utils.api.laji.fi.IntellectualRightsConnectionFactory;
import fi.helsinki.it.skannerz.utils.api.laji.fi.IntellectualRightsTransformer;
import fi.helsinki.it.skannerz.utils.api.laji.fi.LajiApiEndPoint;
import fi.helsinki.it.skannerz.domain.IntellectualRight;
import fi.helsinki.it.skannerz.webframework.controllers.ErrorController;
import fi.helsinki.it.skannerz.webframework.controllers.FormController;
import fi.helsinki.it.skannerz.webframework.controllers.FormControllerFactory;
import fi.helsinki.it.skannerz.webframework.controllers.MonitorController;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Session;
import static spark.Spark.exception;
import static spark.Spark.staticFileLocation;
import spark.TemplateEngine;
import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.notFound;
import static spark.Spark.port;
import static spark.Spark.post;

/**
 * A class encapsulating the Spark web framework.
 *
 * @author Walter Grönholm
 */
public class Router {

    private static final Logger LOG = LoggerFactory.getLogger(Router.class);

    private final TemplateEngine engine;
    private final PipelineManager manager;

    private Router(TemplateEngine engine, PipelineManager manager) {
        Validate.notNull(engine);
        Validate.notNull(manager);
        this.engine = engine;
        this.manager = manager;
    }

    /**
     * Initializes the Spark web framework with the given port.
     * @param port The port to listen to
     */
    public void init(int port) {
        LOG.info("Initing Spark with local port: " + port);
        staticFileLocation("/spark/public");
        port(port); //only localhost

        List<IntellectualRight> enumList = new LajiApiEndPoint<>(
            new IntellectualRightsConnectionFactory(Configuration.getIntellectualRightURL()),
            new IntellectualRightsTransformer()
        ).get();

        FormController formControler = FormControllerFactory.get(enumList);
        get("/form", formControler.getRoute(engine));
        post("/form", formControler.postRoute(engine, manager));
        get("/monitor", MonitorController.getRoute(engine));
        post("/monitor/newform", MonitorController.newForm(engine));
        post("/monitor/abort", MonitorController.abort(engine, manager));
        post("/monitor/reset", MonitorController.resetMeta(engine, manager));
        after("/form", afterFilter());
        after("/monitor", afterFilter());

        exception(Exception.class, ErrorController.handleException(engine, 500));
        notFound(ErrorController.getRoute(engine, 404));
        LOG.info("...Done");
    }

    private static Filter afterFilter() {
        return (Request req, Response res) -> {
            Session session = req.session(false);
            if (session != null && session.attribute("messages") != null) // clear messages after every request so they will be only shown once.
            {
                req.session(false).attribute("messages", null);
            }
        };
    }

    public static class Builder {

        private PipelineManager pipelineManager;
        private TemplateEngine engine;

        public Builder setTemplateEngine(TemplateEngine engine) {
            this.engine = engine;
            return this;
        }

        public Builder setPipelineManager(PipelineManager pipelineManager) {
            this.pipelineManager = pipelineManager;
            return this;
        }

        public Router build() {
            return new Router(engine, pipelineManager);
        }
    }
}
