package fi.helsinki.it.skannerz.webframework.validation;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Validates that a parameter is a valid enumerative string.
 *
 * @author waxwax
 */
public class EnumValidator implements ParameterValidator {

    private final Set<String> enums;

    public EnumValidator(Collection<String> enums) {
        if (enums == null) {
            throw new IllegalArgumentException("enums must not be null!");
        }
        this.enums = new HashSet<>(enums);
    }

    @Override
    public boolean validate(String key, String value, List<String> msgContainer) {
        StringBuilder message = new StringBuilder(key).append("(").append(value).append(") ");
        if (!enums.contains(value)) {
            msgContainer.add(message.append(" is not a valid enum.").toString());
            return false;
        }
        return true;
    }

}
