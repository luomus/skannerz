package fi.helsinki.it.skannerz.webframework.websockets;

/**
 *
 * @author riku
 */
import java.util.HashSet;
import java.util.Set;

import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class defines a configuration for used WebSockets. 
 * Tomcat invokes methods of every class having a type of 
 * ServerApplicationConfig implicitly during application deployment.
 * 
 * When creating new web socket server end point classes, 
 * modify the methods declared here so they'll work.
 * 
 * See JavaEE JSR-356 specification for more information about
 * endpoint config and annotation based classes and their differences.
 * 
 * Path of web sockets should be placed under /websockets/ 
 * (this is defined in web.xml) as otherwise the Spark filter
 * will override it and web sockets won't work.
 * 
 */
public class WebsocketConfig implements ServerApplicationConfig {
    private static final Logger LOG = LoggerFactory.getLogger(WebsocketConfig.class);

    /**
     * Add classes extending javax.websocket.Endpoint to this method.
     */
    @Override
    public Set<ServerEndpointConfig> getEndpointConfigs(
        Set<Class<? extends Endpoint>> scanned) {

        Set<ServerEndpointConfig> result = new HashSet<>();

        if (scanned.contains(PipelineWebSocket.class)) {
            result.add(ServerEndpointConfig.Builder.create(
                PipelineWebSocket.class,
                "/websocket/echoProgrammatic").build());
        }

        return result;
    }


    /**
     * Add annotation based classes here.
     */
    @Override
    public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {

        Set<Class<?>> results = new HashSet<>();
        // results.add(MyAnnotatedWebSocketEndpoint.class);
        return results;
    }
}
