package fi.helsinki.it.skannerz.webframework.websockets;

import com.google.gson.JsonObject;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.service.MailboxInterface;
import fi.helsinki.it.skannerz.service.MailboxReceiver;

/**
 *
 * @author riku
 * @see MessageAction
 */
public class StatusUpdateMessageAction implements MessageAction {

    private final MailboxReceiver<PipelineMessage> receiver;
    private MailboxInterface<PipelineMessage> mailbox;

    /**
     * 
     * @param mailbox source of the messages that should be updated.
     * @param receiver receiver of the messages
     */
    public StatusUpdateMessageAction(MailboxInterface<PipelineMessage> mailbox, MailboxReceiver<PipelineMessage> receiver) {
        this.mailbox = mailbox;
        this.receiver = receiver;
    }

    @Override
    public void doAction(JsonObject object) {
        mailbox.handleMail(receiver);
    }

    public void setMailbox(MailboxInterface<PipelineMessage> mailbox) {
        this.mailbox = mailbox;
    }
    
}
