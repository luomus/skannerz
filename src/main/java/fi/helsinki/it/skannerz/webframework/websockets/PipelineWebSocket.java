package fi.helsinki.it.skannerz.webframework.websockets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fi.helsinki.it.skannerz.domain.Image;
import fi.helsinki.it.skannerz.domain.PipelineMessage;
import fi.helsinki.it.skannerz.domain.dataholders.SpecimenData;
import fi.helsinki.it.skannerz.service.MailboxFactory;
import fi.helsinki.it.skannerz.service.MailboxReceiver;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fi.helsinki.it.skannerz.service.MailboxInterface;
import java.util.concurrent.CancellationException;
import javax.ws.rs.ProcessingException;
import fi.helsinki.it.skannerz.utils.gson.DateTimeSerializer;
import fi.helsinki.it.skannerz.utils.gson.JsonUtils;
import java.util.HashMap;
import java.util.Map;
import org.joda.time.DateTime;

/**
 * Websocket responsible for sending the statuses of pipelines to clients. One
 * instance is created for every connection.
 *
 * @author riku, waxwax
 */
public class PipelineWebSocket extends WebSocket implements MailboxReceiver<PipelineMessage>, WebSocketHandler {

    private static final Logger LOG = LoggerFactory.getLogger(PipelineWebSocket.class);
    private static final Gson GSON = buildGson();
    private static final Object SEND_TEXT_LOCK = new Object();
    private static final String FRONTEND_SEPARATOR = "//";

    private final Map<String, MessageAction> actions;
    private MailboxInterface<PipelineMessage> mailbox = MailboxFactory.getMailbox(PipelineMessage.class);

    public PipelineWebSocket() {
        actions = new HashMap<>();
        actions.put("STATUS_UPDATE", new StatusUpdateMessageAction(mailbox, this));
        actions.put("REPROCESS", new ReprocessFolderMessageAction(FRONTEND_SEPARATOR));
        actions.put("UPDATE_QR", new UpdateQRCodeMessageAction(FRONTEND_SEPARATOR));
    }

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        super.onOpen(session, endpointConfig);
        mailbox.addReceiver(this, true);
    }

    /**
     * If the client needs to update monitor page for example by pressing a
     * button, we'll query the mailbox again using the callback method
     * handleMail(MailboxReceiver<T>).
     *
     * In this case the handleMail works as an update method.
     *
     * @param message Message sent by the client of this instance.
     */
    @Override
    public void onMessage(String message) {
        LOG.debug("Got message from WebSocket: " + message);
        try {
            JsonObject data = new JsonParser().parse(message).getAsJsonObject();
            String action = JsonUtils.safeGetString(data, "action");
            if (actions.containsKey(action)) {
                actions.get(action).doAction(data);
            } else {
                LOG.warn("Retrieved unknown websocket message: {}", message);
            }
        } catch (Exception ex) {
            LOG.error("Exception caught while processing websocket message: " + message, ex);
        }
    }

    /**
     * Informs the client about the new message in mailbox. If the processing of
     * a pipeline isn't done yet, this method will send a message telling that
     * the pipeline is still in processing and will start a new thread which is
     * waiting for the processing to finish and the thread will then invoke this
     * method again.
     *
     * @param message message found in the mailbox.
     */
    @Override
    public void handleMail(PipelineMessage message) {
        if (!message.getFuture().isDone()) {
            getFutureWaitingThread(message).start();
        }
        synchronized (SEND_TEXT_LOCK) {
            try {
                if (getSession().isOpen()) {
                    getSession().getBasicRemote().sendText(GSON.toJson(message));
                }
            } catch (IOException ioe) {
                LOG.error("Sending message to client " + getSession() + " failed.", ioe);
            }
        }
    }

    /**
     * @param pipelineMessage the PipelineMessage which is waited for to finish.
     * @return a thread which waits for the {@link fi.helsinki.it.skannerz.service.Pipeline} associated with the given pipelineMessage to finish and rehandles the message afterwards.
     */
    Thread getFutureWaitingThread(PipelineMessage pipelineMessage) {
        return new Thread(() -> {
            try {
                pipelineMessage.getFuture().get(); // this will block if it's not done yet.
            } catch (ProcessingException pe) {
                LOG.error("Processing exception ", pe);
            } catch (InterruptedException | ExecutionException | CancellationException e) {
                LOG.error("Future is done in handleMail", e);
                /* In case of an exception the status has changed to
                * PipelineReturnCode.ABORTED
                */
            }
            handleMail(pipelineMessage);
        });
    }

    /**
     * Invoked during closing of this connection. This method
     * removes this connection from the mailboxes' receiver list.
     * @param session
     * @param closeReason 
     */
    @Override
    public void onClose(Session session, CloseReason closeReason) {
        super.onClose(session, closeReason);
        mailbox.removeReceiver(this);
    }

    @Override
    protected WebSocketHandler messageHandler(Session session) {
        return this;
    }

    public void setMailbox(MailboxInterface<PipelineMessage> mailbox) {
        this.mailbox = mailbox;
        if (actions.get("STATUS_UPDATE") instanceof StatusUpdateMessageAction) {
            ((StatusUpdateMessageAction) actions.get("STATUS_UPDATE")).setMailbox(mailbox);
        }
    }

    private static Gson buildGson() {
        return new GsonBuilder()
            .registerTypeAdapter(
                PipelineMessage.class,
                new PipelineMessageSerializer())
            .registerTypeAdapter(
                DateTime.class,
                new DateTimeSerializer())
            .create();
    }

    /**
     * Custom Gson JSON serializer as PipelineMessage contains a Future object
     * and a String representing the folder name. The output will vary depending
     * on whether the pipeline processing is done or not and that's why we need
     * custom serializer.
     */
    private static class PipelineMessageSerializer implements JsonSerializer<PipelineMessage> {

        private static final Logger LOG = LoggerFactory.getLogger(PipelineWebSocket.PipelineMessageSerializer.class);

        @Override
        public JsonElement serialize(PipelineMessage message, Type type, JsonSerializationContext jsc) {
            JsonObject jsonObject = new JsonObject();
            String status = message.getStatus().getMessage();
            String statusCode = message.getStatus().name();
            SpecimenData specimenData = message.getSpecimenData();

            jsonObject.add("id", new JsonPrimitive(getDocumentId(specimenData)));
            jsonObject.add("statusCode", new JsonPrimitive(statusCode));
            jsonObject.add("status", new JsonPrimitive(status));
            jsonObject.add("folder", new JsonPrimitive(specimenData.getFolder().getName()));
            jsonObject.add("meta", getMetaMap(specimenData, jsc));
            jsonObject.add("lastUpdated", getLastUpdateDateTime(specimenData, jsc));
            return jsonObject;
        }

        private String getDocumentId(SpecimenData specimenData) {
            for (Image image : specimenData.getImages()) {
                if (image.getMeta() != null
                    && image.getMeta().getDocumentIds() != null
                    && !image.getMeta().getDocumentIds().isEmpty()) {
                    return image.getMeta().getDocumentIds().get(0);
                }
            }
            return "N/A";
        }

        private JsonObject getMetaMap(SpecimenData specimenData, JsonSerializationContext jsc) {
            JsonObject metaMap = new JsonObject();
            for (Image image : specimenData.getImages()) {
                metaMap.add(image.getName(), jsc.serialize(image.getMeta()));
            }
            return metaMap;
        }

        private JsonElement getLastUpdateDateTime(SpecimenData specimenData, JsonSerializationContext jsc) {
            for (Image image : specimenData.getImages()) {
                if (image.getMeta() != null
                        && image.getMeta().getLastUpdated() != null)
                    return jsc.serialize(image.getMeta().getLastUpdated());
            }
            return null;
        }

    }

}
