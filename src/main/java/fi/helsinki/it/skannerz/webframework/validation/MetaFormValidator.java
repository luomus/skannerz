package fi.helsinki.it.skannerz.webframework.validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import spark.Request;


/**
 * Created by riku on 6.10.2017.
 *
 * This class works as a validator for the meta data form.
 *
 * Every parameter has its own ParameterValidator, which is used to validate the
 * parameter itself.
 *
 * Parameter can be either a required or optional one. Required parameters will
 * be always validated and checked if they are given as input by the user and
 * optional parameters are validated only when the user has given any input on
 * to the fields of optional parameters.
 *
 */
public class MetaFormValidator implements RequestValidator {

    private final Map<String, ParameterValidator> validators = new HashMap<>();

    public MetaFormValidator(Collection<String> domainList, Collection<String> rightsList) {
        validators.put("intellectualOwner", new StringValidator.Builder().minimumLength(1).build());
        validators.put("intellectualRights", new EnumValidator(rightsList));
        validators.put("domain", new EnumValidator(domainList));
        validators.put("person", new StringValidator.Builder().minimumLength(1).build());
    }

    private final Set<String> requiredParameters = new HashSet<>(Arrays.asList(
            "intellectualOwner",
            "intellectualRights",
            "domain",
            "person"
    ));

    private final Set<String> optionalParameters = new HashSet<>(Arrays.asList());

    @Override
    public boolean validate(Request req, List<String> msgContainer) {

        // check that every required param is present.
        for (String param : requiredParameters) {
            if (req.queryParams(param) == null) {
                msgContainer.add(param + " is required.");
                return false;
            }
        }

        // validate required parameters
        for (String param : requiredParameters) {
            if (!validators.get(param).validate(param, req.queryParams(param), msgContainer)) {
                return false;
            }
        }

        // validate optional parameters, if they are present
        for (String param : optionalParameters) {
            String value = req.queryParams(param);
            if (value != null && !value.trim().isEmpty() && !validators.get(param).validate(param, req.queryParams(param), msgContainer)) {
                return false;
            }
        }

        return true;
    }

}
