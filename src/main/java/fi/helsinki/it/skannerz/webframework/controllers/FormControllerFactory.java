package fi.helsinki.it.skannerz.webframework.controllers;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.IntellectualRight;
import fi.helsinki.it.skannerz.webframework.validation.MetaFormValidator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Convenience class used to create FormControllers.
 *
 * @author Walter Grönholm
 */
public class FormControllerFactory {

    /**
     * Returns the default FormController with the given
     * {@link IntellectualRight} list.
     *
     * @param enumList
     * @return a new FormController
     */
    public static FormController get(List<IntellectualRight> enumList) {
        return new FormController.Builder()
            .setEnumList(enumList)
            .setValidator(new MetaFormValidator(
                Configuration.getDomainList(),
                enumList.stream().map(i -> i.getId()).collect(Collectors.toList())
            )).build();
    }
}
