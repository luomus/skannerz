package fi.helsinki.it.skannerz.webframework.validation;

import spark.Request;

import java.util.List;

/**
 * Created by riku on 6.10.2017.
 *
 * Interface for request validation.
 */
public interface RequestValidator {

    /**
     * Validates the Request parameters. Appends any error messages to the given messageList.
     *
     * @param req the Spark Request object
     * @param messageList a list for messages the method might produce
     * and those messages can then be send in a response back to the client.
     * @return true in case of a valid request.
     */
    boolean validate(Request req, List<String> messageList);
}
