package fi.helsinki.it.skannerz.webframework.controllers;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.service.PipelineManager;
import fi.helsinki.it.skannerz.utils.api.laji.fi.ImageConnectionFactory;
import fi.helsinki.it.skannerz.utils.api.laji.fi.ImageTransformer;
import fi.helsinki.it.skannerz.utils.api.laji.fi.LajiApiEndPoint;
import fi.helsinki.it.skannerz.utils.api.laji.fi.PersonConnectionFactory;
import fi.helsinki.it.skannerz.utils.api.laji.fi.PersonTransformer;
import fi.helsinki.it.skannerz.domain.IntellectualRight;
import fi.helsinki.it.skannerz.webframework.FormToMetaConverter;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.getContextPath;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.putInputParams;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.render;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fi.helsinki.it.skannerz.webframework.validation.RequestValidator;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import spark.Request;
import spark.Response;
import spark.Route;

import spark.TemplateEngine;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.putGlobalParams;
import spark.Session;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.putMessagesFromSession;
import static spark.Spark.halt;

/**
 * Controller class of the form page.
 *
 * @author waxwax
 */
public class FormController {

    private static final Logger LOG = LoggerFactory.getLogger(FormController.class);

    private final List<IntellectualRight> enumList;
    private final RequestValidator validator;
    
    private final Map<String, Map<String, String>> labels = new HashMap<>();

    private FormController(List<IntellectualRight> enumList, RequestValidator validator) {
        this.enumList = enumList;
        this.validator = validator;
    }

    /**
     * @param engine the TemplateEngine used to render the page
     * @return the GET route of the form page.
     */
    public Route getRoute(TemplateEngine engine) {
        return (Request req, Response res) -> {
            Map<String, Object> params = new HashMap<>();
            putGlobalParams(params, req);
            putListParams(params, req);
            putMessagesFromSession(params, req);
            putMetaParams(params, req);
            putLabels(params, req);
            return render(engine, "form.ftl", params, res);
        };
    }

    /**
     * @param engine the TemplateEngine used to render the page
     * @param pipelineManager the PipelineManager instance which meta is set
     * @return the POST route of the form page.
     */
    public Route postRoute(TemplateEngine engine, PipelineManager pipelineManager) {
        return (Request req, Response res) -> {
            List<String> messages = new ArrayList<>();
            if (validator.validate(req, messages)) {
                messages.add("Form sent succesfully!");
                ExtendedMeta meta = new FormToMetaConverter().convert(req.queryMap());
                pipelineManager.setImageMetadata(meta);

                Session session = req.session(true);
                session.attribute("messages", messages);
                session.attribute("lastMeta", meta);
                res.redirect(getContextPath(req) + "/monitor");
                throw halt();
            }
            LOG.info("Invalid metadata entered");

            Map<String, Object> params = new HashMap<>();
            putGlobalParams(params, req);
            putListParams(params, req);
            putInputParams(params, req);
            putLabels(params, req);
            params.put("messages", messages);
            return render(engine, "form.ftl", params, res);
        };
    }

    private void putListParams(Map<String, Object> params, Request req) {
        params.put("intellectualRightsEnumList", enumList);
        params.put("intellectualRights", Configuration.getDefaultIntellectualRight());
        params.put("domainList", Configuration.getDomainList());
        params.put("keywordsEnumList", Configuration.getKeywordsList());
    }

    private static void putMetaParams(Map<String, Object> params, Request req) {
        Session session = req.session(false);
        if (session != null && session.attribute("lastMeta") != null) {
            ExtendedMeta lastMeta = session.attribute("lastMeta");
            params.put("domain", lastMeta.getDomain());
            params.put("person", lastMeta.getUploadedBy());
            params.put("intellectualOwner", lastMeta.getRightsOwner());
            params.put("intellectualRights", lastMeta.getLicense());
            params.put("keywordsList", lastMeta.getTags());
            params.put("publicityRestrictions", lastMeta.getSecret() ? "private" : "public");
        }
    }

    private void putLabels(Map<String, Object> params, Request req) {
        String language = Configuration.getDefaultLanguage();
        Session s = req.session(false);
        if (s != null && s.attribute("language") != null) {
            language = s.attribute("language");
        }

        params.putAll(getLabels(language));
    }

    private Map<String, String> getLabels(String language) {
        if (!labels.containsKey(language)) {
            Map<String, String> newLabels = new HashMap<>();
            Map<String, String> mmPersonLabels = new LajiApiEndPoint<>(
                new PersonConnectionFactory(Configuration.getMMPersonLabelURL(), language),
                new PersonTransformer()
            ).get();

            Map<String, String> mmImageLabels = new LajiApiEndPoint<>(
                new ImageConnectionFactory(Configuration.getMMImageLabelURL(), language),
                new ImageTransformer()
            ).get();

            newLabels.putAll(mmPersonLabels);
            newLabels.putAll(mmImageLabels);
            labels.put(language, newLabels);
        }

        return labels.get(language);
    }

    public static class Builder {

        private List<IntellectualRight> enumList;
        private RequestValidator validator;

        public FormController build() {
            return new FormController(enumList, validator);
        }

        public Builder setEnumList(List<IntellectualRight> enumList) {
            this.enumList = enumList;
            return this;
        }

        public Builder setValidator(RequestValidator validator) {
            this.validator = validator;
            return this;
        }
    }
}
