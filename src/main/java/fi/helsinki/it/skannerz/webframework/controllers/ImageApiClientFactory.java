package fi.helsinki.it.skannerz.webframework.controllers;

import fi.helsinki.it.skannerz.config.Configuration;
import fi.luomus.kuvapalvelu.client.ImageApiClient;
import fi.luomus.kuvapalvelu.client.ImageApiClientImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates a IamgeApiClients from the default credentials given in the
 * Configuration.
 *
 * @author waxwax
 */
public class ImageApiClientFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ImageApiClientFactory.class);

    /**
     * Creates a new ImageApiClient from the default credentials given in the
     * Configuration.
     *
     * @return an ImageApiClient
     */
    public static ImageApiClient getClient() {
        LOG.debug("Creating a new ImageApiClient");

        String imageStorageDbUri = Configuration.getImageStorageDdUri();
        String username = Configuration.getUsername();
        String password = Configuration.getPassword();

        return ImageApiClientImpl.builder()
            .uri(imageStorageDbUri)
            .username(username)
            .password(password)
            .build();
    }

}
