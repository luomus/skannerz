package fi.helsinki.it.skannerz.webframework.websockets;

import com.google.gson.JsonObject;

/**
 * MessageAction represents an single action that can be made as a response
 * for a websocket message.
 * 
 * @see PipelineWebSocket#onMessage(java.lang.String) 
 * @author riku
 * 
 */
public interface MessageAction {

    void doAction(JsonObject object);
    
}
