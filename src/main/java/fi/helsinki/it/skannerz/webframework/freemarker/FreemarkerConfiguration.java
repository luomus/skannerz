package fi.helsinki.it.skannerz.webframework.freemarker;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import java.io.File;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import spark.template.freemarker.FreeMarkerEngine;

/**
 * Contains the FreeMarker
 * {@link freemarker.template.Configuration configuration} and 
 * {@link spark.template.freemarker.FreeMarkerEngine FreeMarkerEngine}.
 *
 * @author hcriku
 */
public class FreemarkerConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(FreemarkerConfiguration.class);
    private static final Configuration cfg = initConfiguration();
    /**
     * A configured FreeMarkerEngine
     */
    public static final FreeMarkerEngine ENGINE = initEngine();

    private static FreeMarkerEngine initEngine() {
        return new FreeMarkerEngine(cfg);
    }

    private static Configuration initConfiguration() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
        configuration.setDefaultEncoding("UTF-8");
        try {
            configuration.setDirectoryForTemplateLoading(
                new File(
                    FreemarkerConfiguration.class.getClassLoader().getResource("spark/template/freemarker").getPath()
                )
            );
        } catch (IOException ioe) {
            LOG.warn("Something went wrong while settings directory for Freemarker templates.", ioe);
        }
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        configuration.setLogTemplateExceptions(false);
        return configuration;
    }

}
