package fi.helsinki.it.skannerz.webframework.websockets;

import java.io.IOException;
import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class extending javax.websocket.Endpoint. Important part of this
 * class is the saving the session of the connection we get when a connection
 * is initialized.
 * 
 * @author Walter Grönholm
 * @see WebsocketConfig
 */
public abstract class WebSocket extends Endpoint {

    private static final Logger LOG = LoggerFactory.getLogger(WebSocket.class);
    private Session session;

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        LOG.debug("Websocket opened for path: " + session.getRequestURI());
        session.addMessageHandler(String.class, messageHandler(session));
        this.session = session;
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        try {
            session.close(closeReason);
        } catch (IOException ioe) {
            LOG.error("IOException When closing session " + session + ".", ioe);
        }
    }

    protected Session getSession() {
        return session;
    }

    abstract protected WebSocketHandler messageHandler(Session session);
}
