package fi.helsinki.it.skannerz.webframework.validation;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by riku on 6.10.2017.
 *
 * This class can be used to validate strings.
 *
 * Even though the min and max length could be done only with regular
 * expressions it seemed sensible to put min and max length as properties as it
 * adds readability and many of the params have only length restrictions.
 */
public class StringValidator implements ParameterValidator {

    private final Integer minimumLength;
    private final Integer maximumLength;
    private final Pattern pattern; // compiled regular expression

    private StringValidator(Builder builder) {
        this.minimumLength = builder.minimumLength;
        this.maximumLength = builder.maximumLength;
        this.pattern = builder.pattern;
    }

    @Override
    public boolean validate(String key, String param, List<String> msgContainer) {
        StringBuilder message = new StringBuilder(key).append(" (").append(param).append(") ");
        
        if (param == null) {
            msgContainer.add(message
                    .append("should not be empty.").toString()
            );
            return false;
        }
        
        if (minimumLength != null && param.length() < minimumLength) {
            msgContainer.add(message
                    .append("should be at least ")
                    .append(minimumLength)
                    .append(" characters long.").toString()
            );
            return false;
        }

        if (maximumLength != null && param.length() > maximumLength) {
            msgContainer.add(message
                    .append(" is too long. ")
                    .append("Maximum length is ")
                    .append(maximumLength)
                    .append(" characters.").toString()
            );
            return false;
        }

        if (pattern != null && !pattern.matcher(param).matches()) {
            msgContainer.add(message
                    .append(" is in a wrong format. ")
                    .append(" It should be in a format like ")
                    .append(pattern.toString()).toString()
            // this message should be more human friendly in the future.
            // regular expression probably doesn't tell much to the user.
            );
            return false;
        }

        return true;
    }

    public static class Builder {

        private Integer minimumLength;
        private Integer maximumLength;
        private Pattern pattern;

        public Builder minimumLength(Integer minimumLength) {
            if (minimumLength != null && maximumLength != null && minimumLength > maximumLength) {
                throw new IllegalArgumentException(
                        "Minimum length (" + minimumLength + ")"
                        + " can't be greater than maximum length (" + maximumLength + ")."
                );
            }
            this.minimumLength = minimumLength;
            return this;
        }

        public Builder maximumLength(Integer maximumLength) {
            if (maximumLength != null && minimumLength != null && maximumLength < minimumLength) {
                throw new IllegalArgumentException(
                        "Maximum length (" + maximumLength + ")"
                        + " can't be less than minimum length (" + minimumLength + ")."
                );
            }
            this.maximumLength = maximumLength;
            return this;
        }

        public Builder pattern(Pattern pattern) {
            this.pattern = pattern;
            return this;
        }

        public StringValidator build() {
            return new StringValidator(this);
        }
    }

}
