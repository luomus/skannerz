package fi.helsinki.it.skannerz.webframework.controllers;

import fi.helsinki.it.skannerz.service.PipelineManager;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.getContextPath;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.render;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.TemplateEngine;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.putGlobalParams;
import spark.Session;
import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.putMessagesFromSession;
import static spark.Spark.halt;

/**
 * Controller class of the monitor page.
 *
 * @author waxwax
 * @see fi.helsinki.it.skannerz.webframework.websockets.PipelineWebSocket
 */
public class MonitorController {

    private static final Logger LOG = LoggerFactory.getLogger(MonitorController.class);

    public static Route getRoute(TemplateEngine engine) {
        return (Request req, Response res) -> {
            Map<String, Object> params = new HashMap<>();
            putGlobalParams(params, req);
            putMessagesFromSession(params, req);

            return render(engine, "monitor.ftl", params, res);
        };
    }

    /**
     * <b>Note:</b> Should only be used to shut down the whole PipelineManager
     * process.
     *
     * @param engine the TemplateEngine used to render the page
     * @param pipelineManager the PipelineManager instance which is aborted
     * @return
     */
    public static Route abort(TemplateEngine engine, PipelineManager pipelineManager) {
        return (Request req, Response res) -> {
            LOG.info("Aborting pipeline process");
            pipelineManager.stopPipelines();
            List<String> messages = new ArrayList<>();
            messages.add("Process has been aborted!");

            req.session(true).attribute("messages", messages);
            res.redirect(getContextPath(req) + "/monitor");
            throw halt();
        };
    }

    /**
     * Route used to reset the meta file in
     * {@link fi.helsinki.it.skannerz.service.PipelineManager}.
     *
     * @param engine the TemplateEngine used to render the page
     * @param pipelineManager the PipelineManager instance which meta is reset
     * @return
     */
    public static Route resetMeta(TemplateEngine engine, PipelineManager pipelineManager) {
        return (Request req, Response res) -> {
            LOG.info("Resetting meta data to null, currently running pipelines will keep running with the current meta data.");
            pipelineManager.setImageMetadata(null);

            List<String> messages = new ArrayList<>();
            messages.add("Meta data has been reset.");
            Session session = req.session(true);
            session.attribute("messages", messages);
            session.attribute("lastMeta", null);
            res.redirect(getContextPath(req) + "/monitor");
            throw halt();
        };
    }

    /**
     * Route used to redirect the user to the form page.
     *
     * @param engine the TemplateEngine used to render the page
     * @return
     */
    public static Route newForm(TemplateEngine engine) {
        return (Request req, Response res) -> {
            LOG.info("Redirecting to form page");
            List<String> messages = new ArrayList<>();
            messages.add("Enter a new form");

            req.session(true).attribute("messages", messages);
            res.redirect(getContextPath(req) + "/form");
            throw halt();
        };
    }

}
