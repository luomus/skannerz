package fi.helsinki.it.skannerz.webframework.controllers;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Session;
import spark.TemplateEngine;

/**
 * Contains convenience methods used by Controller classes.
 *
 * @author waxwax
 */
class ControllerUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerUtil.class);

    /**
     * Preserves the user input by re-putting them to the parameter map
     *
     * @param params the parameter map to fill
     * @param req the Spark Request object given by the Route
     */
    static void putInputParams(Map<String, Object> params, Request req) {
        for (String queryParam : req.queryParams()) {
            params.put(queryParam, req.queryParams(queryParam));
        }
    }

    /**
     * Puts global parameters into the given params map, e.g. the title of the web page.

     * @param params the parameter map to fill
     * @param req the Spark Request object given by the Route
     */
    static void putGlobalParams(Map<String, Object> params, Request req) {
        params.put("title", "Skannerz");
        String contextPath = getContextPath(req);
        params.put("contextPath", contextPath);
    }

    /**
     * Retrieves the page messages from the session and re-puts them into the given params map.

     * @param params the parameter map to fill
     * @param req the Spark Request object given by the Route
     */
    static void putMessagesFromSession(Map<String, Object> params, Request req) {
        Session session = req.session(false);
        if (session != null && session.attribute("messages") != null) {
            params.put("messages", session.attribute("messages"));
        }
    }

    /**
     * @param req the Spark Request object given by the Route
     * @return the contextPath of the Request, or an empty String if it isn't defined.
     */
    static String getContextPath(Request req) {
        return req.contextPath() == null ? "" : req.contextPath();
    }

    static String render(TemplateEngine engine, String contentFile, Map<String, Object> params, Response res) {
        LOG.debug("Rendering '" + contentFile + "' file");
        params.put("contentFile", contentFile);
        return engine.render(new ModelAndView(params, "master.ftl"));
    }
}
