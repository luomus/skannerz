package fi.helsinki.it.skannerz.webframework.validation;

import java.util.List;

/**
 * Validates that an Integer parameter is within range.
 *
 * Created by riku on 6.10.2017.
 */
public class IntegerValidator implements ParameterValidator {

    private final Integer min;
    private final Integer max;

    private IntegerValidator(Builder builder) {
        this.min = builder.min;
        this.max = builder.max;
    }

    @Override
    public boolean validate(String key, String param, List<String> msgContainer) {
        Integer value;
        StringBuilder message = new StringBuilder(key).append("(").append(param).append(") ");
        try {
            value = Integer.parseInt(param);
        } catch (NumberFormatException nfe) {
            msgContainer.add(message
                .append(" should be a number.").toString()
            );
            return false;
        }

        if (min != null && value < min) {
            msgContainer.add(message
                .append(" is too small. ")
                .append("It should be greater than or equal to ")
                .append(min).append(".").toString()
            );
            return false;
        }

        if (max != null && value > max) {
            msgContainer.add(message
                .append(" is too large. ")
                .append("it should be less than or equal to ")
                .append(max)
                .append(".").toString()
            );
            return false;
        }

        return true;
    }

    public static class Builder {

        private Integer min = Integer.MIN_VALUE;
        private Integer max = Integer.MAX_VALUE;

        public Builder min(Integer min) {
            this.min = min;
            return this;
        }

        public Builder max(Integer max) {
            this.max = max;
            return this;
        }

        public IntegerValidator build() {
            return new IntegerValidator(this);
        }

    }
}
