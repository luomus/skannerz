package fi.helsinki.it.skannerz.webframework.validation;

import java.util.List;

/**
 * Created by riku on 6.10.2017.
 */
public interface ParameterValidator {

    /**
     * Validates the given key value pair. Appends any error messages to the
     * given messageList.
     *
     * @param key the key/field name of the parameter. Used for logging.
     * @param value the value which is to be validated
     * @param messageList a list where the error messages are added to
     * @return true in case of a valid parameter.
     */
    boolean validate(String key, String value, List<String> messageList);
}
