package fi.helsinki.it.skannerz.webframework.websockets;

import javax.websocket.MessageHandler;

/**
 * @deprecated 
 * @author Walter Grönholm
 */
public interface WebSocketHandler extends MessageHandler.Whole<String> {
    
}
