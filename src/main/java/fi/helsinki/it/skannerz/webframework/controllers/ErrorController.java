package fi.helsinki.it.skannerz.webframework.controllers;

import static fi.helsinki.it.skannerz.webframework.controllers.ControllerUtil.render;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import spark.ExceptionHandler;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.TemplateEngine;

/**
 * Used to test the 500 error page.
 *
 * @author waxwax
 */
public class ErrorController {

    private static final Logger LOG = LoggerFactory.getLogger(ErrorController.class);

    public static Route getRoute(TemplateEngine engine, int status) {
        return (Request req, Response res) -> {
            return engine.render(new ModelAndView(getParams(status), "error.ftl"));
        };
    }

    public static ExceptionHandler handleException(TemplateEngine engine, int status) {
        return (Exception ex, Request req, Response res) -> {
            LOG.warn("Expection while handling a request. ", ex);
            Map<String, Object> params = getParams(status);
            params.put("exception", ex);
            params.put("stacktrace", ex.getStackTrace());
            res.status(status);
            try {
                render(engine, "error.ftl", params, res);
            } catch (Exception e) {
                LOG.error("Expection occured while handling exception! ", e);
            }
        };
    }

    /**
     * For testing purposes only
     *
     * @return
     * @deprecated DO NOT USE
     */
    public static Route throwException() {
        return (Request req, Response res) -> {
            throw new IllegalArgumentException("Hi!");
        };
    }
    
    private static Map<String, Object> getParams(int status) {
        Map<String, Object> params = new HashMap<>();
        params.put("title", "Skannerz");
        params.put("status", status);
        return params;
    }
}
