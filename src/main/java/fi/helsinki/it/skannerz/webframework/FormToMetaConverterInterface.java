package fi.helsinki.it.skannerz.webframework;

import fi.luomus.kuvapalvelu.client.model.Meta;
import spark.QueryParamsMap;

/**
 * Creates Meta objects from Maps. The converter assumes the given data is
 * valid.
 *
 * @author Walter Grönholm
 */
public interface FormToMetaConverterInterface {

    /**
     * Retrieves the Meta values from the given queryParamsMap. Does not add any
     * other data than given in the map.
     *
     * @param queryParamsMap
     * @return a Meta filled with the information given in the queryParamsMap
     */
    public Meta convert(QueryParamsMap queryParamsMap);
}
