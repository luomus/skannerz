package fi.helsinki.it.skannerz.webframework.websockets;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.service.ImageFolderReader;
import fi.helsinki.it.skannerz.service.PipelineManager;
import fi.helsinki.it.skannerz.service.PipelineManagerFactory;
import fi.helsinki.it.skannerz.utils.gson.JsonUtils;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author riku
 * @see MessageAction
 */
public class UpdateQRCodeMessageAction extends ReprocessFolderMessageAction {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateQRCodeMessageAction.class);

    public UpdateQRCodeMessageAction(String frontEndSeparator) {
        super(frontEndSeparator);
    }

    @Override
    public void doAction(JsonObject object) {
        LOG.info("Doing qr action.");
        object = object.get("data").getAsJsonObject();
        JsonElement folder = object.get("folder");
        String qrCode = JsonUtils.safeGetString(object, "qrCode");
        if (validate(qrCode)) {
            LOG.debug("qrCode {} was valid.", qrCode);
            PipelineManager pipelineManager = PipelineManagerFactory.get();
            ImageFolderReader imageFolderReader = new ImageFolderReader();
            ExtendedMeta meta = new ExtendedMeta();
            ArrayList<String> documentIds = new ArrayList<>();
            documentIds.add(qrCode);
            meta.setDocumentIds(documentIds);
            reprocessFolder(folder, meta, imageFolderReader, pipelineManager);
        } else {
            LOG.info("Invalid qrCode sent: {}", qrCode);
        }
    }

    private boolean validate(String qrCode) {
        return qrCode != null && qrCode.matches("https?://.*");
    }

}
