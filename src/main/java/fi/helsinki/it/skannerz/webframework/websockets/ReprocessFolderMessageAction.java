package fi.helsinki.it.skannerz.webframework.websockets;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fi.helsinki.it.skannerz.config.Configuration;
import fi.helsinki.it.skannerz.domain.ExtendedMeta;
import fi.helsinki.it.skannerz.domain.Folder;
import fi.helsinki.it.skannerz.service.ImageFolderReader;
import fi.helsinki.it.skannerz.service.PipelineManager;
import fi.helsinki.it.skannerz.service.PipelineManagerFactory;
import fi.helsinki.it.skannerz.utils.gson.JsonUtils;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author riku
 * @see MessageAction
 */
public class ReprocessFolderMessageAction implements MessageAction {
    private static final Logger LOG = LoggerFactory.getLogger(ReprocessFolderMessageAction.class);

    private final String frontEndSeparator;

    public ReprocessFolderMessageAction(String frontEndSeparator) {
        this.frontEndSeparator = frontEndSeparator;
    }

    @Override
    public void doAction(JsonObject object) {
        JsonArray folderNames = JsonUtils.safeGetArray(object, "data");
        PipelineManager pipelineManager = PipelineManagerFactory.get();
        ImageFolderReader imageFolderReader = new ImageFolderReader();
        for (JsonElement folderName : folderNames) {
            reprocessFolder(folderName, null, imageFolderReader, pipelineManager);
        }

    }

    protected void reprocessFolder(JsonElement folderName, ExtendedMeta meta, ImageFolderReader imageFolderReader, PipelineManager pipelineManager) {
        LOG.debug(getFolderPath(folderName));
        File folderFile = new File(getFolderPath(folderName));
        Folder folder = imageFolderReader.createFolderObject(folderFile);
        pipelineManager.process(folder, meta);
    }

    protected String getFolderPath(JsonElement folderName) {
        return Configuration.getRootFolderPath() + File.separatorChar + folderName.getAsString().split(frontEndSeparator)[0];
    }
    
}
