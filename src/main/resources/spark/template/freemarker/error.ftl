<h2>${status}</h2>
<#if exception??>
    <span><code>${exception}</code></span>
    <#list stacktrace>
        <code style="white-space: pre-wrap">
            <#items as element>
                ${element}
            </#items>
        </code>
    </#list>
</#if>