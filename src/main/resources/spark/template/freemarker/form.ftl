<div class="center width-786">
    <h2>Enter data</h2>
    <#include "messages.ftl">
    <form id="sendForm" name="sendForm" action="${contextPath}/form" method="post" onreset="resetKeywords()">
        <fieldset>
            <legend>Required fields</legend>
            <div class="field">
                Domain
                <select class="right half" form="sendForm" name="domain" value="${domain!}">
                    <option value="null">-Select-</option>
                    <#list domainList as enum>
                        <#if domain?? && enum == domain>
                            <option value=${enum} selected>${enum}</option>
                        <#else>
                            <option value=${enum}>${enum}</option>
                        </#if>
                    </#list>
                </select>
            </div>
            <div class="field">
                <label><div>
                        <span title="MA.person">${MA\.person!"MA.person"}</span>
                        <input class="half right" name="person" type="text" value="${person!}"/>
                </div></label>
            </div>
            <div class="field">
                <label><div>
                        <span title="MZ.intellectualOwner">${MZ\.intellectualOwner!"MZ.intellectualOwner"}</span>
                        <input class="half right" name="intellectualOwner" type="text" value="${intellectualOwner!}"/>
                </div></label>
            </div>
            <div class="field">
                <span title="MZ.intellectualRights">${MZ\.intellectualRights!"MZ.intellectualRights"}</span>
                <select class="full" form="sendForm" name="intellectualRights" value="${intellectualRights!}">
                    <#list intellectualRightsEnumList as enum>
                        <#if intellectualRights?? && enum.id == intellectualRights>
                            <option value=${enum.id} selected>${enum.value}</option>
                        <#else>
                            <option value=${enum.id}>${enum.value}</option>
                        </#if>
                    </#list>
                </select>
            </div>
        </fieldset>
        <fieldset>
            <legend>Additional Information</legend>
            <div class="field">
                <label for="keywords-text"><div>
                        <span title="MM.keyword">${MM\.keyword!"MM.keyword"}</span>
                        <input id="keywords-input" type="hidden" name="keywords" value="${keywords!}">
                        <button class="quarter right" id="keywords-button" type="button" onclick="addKeyword()">Add</button>
                        <input class="quarter right" id="keywords-text" list="keywords-datalist">
                        <datalist id="keywords-datalist">
                            <#list keywordsEnumList as enum>
                                <option value=${enum}>${enum}</option>
                            </#list>
                        </datalist>
                        <ul id="keywords-list">
                            <li style="display: none" id="keywords-list-template">
                                <div class="keyword-item"><button type="button" onclick="deleteKeyword(this)">&nbsp;X&nbsp;</button><span class="keyword-name"></span></div>
                            </li>
                            <#if keywordsList??>
                                <#list keywordsList as keyword>
                                <li>
                                    <div class="keyword-item" data-added="false"><button type="button" onclick="deleteKeyword(this)">&nbsp;X&nbsp;</button><span class="keyword-name">${keyword}</span></div>
                                </li>
                                </#list>
                            </#if>
                        </ul>
                </div></label>
            </div>
            <div class="field">
                <span title="MZ.publicityRestrictions">${MZ\.publicityRestrictions!"MZ.publicityRestrictions"}</span>
                <select name="publicityRestrictions" class="right half" form="sendForm" value="${publicityRestrictions!}">
                    <option value="public" 
                            <#if publicityRestrictions?? && publicityRestrictions == "public">
                                selected
                            </#if>>PUBLIC</option>
                    <option value="protected">PROTECTED</option>
                    <option value="private" 
                            <#if publicityRestrictions?? && publicityRestrictions == "private">
                                selected
                            </#if>>PRIVATE</option>
                </select>
            </div>
        </fieldset>
        <div>
            <input class="half" type="submit" name="submitButton" value="Submit" />
            <input class="half" type="button" onclick="resetForm()" name="resetButton" value="Clear fields" />
        </div>
    </form>
</div>
<script>
    <#include 'javascript/form.js' parse=false encoding="UTF-8">
    returnKeywords("${keywords!}");
</script>
