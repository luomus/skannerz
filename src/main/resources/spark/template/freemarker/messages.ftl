<#if messages??>
    <#list messages>
        <ul id="messages">
            <#items as message>
               <li name="messageField">${message}</li>
            </#items>
        </ul>
    </#list>
<#else>
    <ul id="messages" class="hide"></ul>
</#if>
<li id="websocket-closed-template" class="hide">Connection closed. Please <a href="">refresh</a> page.</li>
<li id="websocket-error-template" class="hide">Connection error occurred. Please contact administrator.</li>
<li id="message-template" class="hide"></li>