<!DOCTYPE html> 
    <#include "head.ftl">
    <body>
        <#include "header.ftl">
        <#include "${contentFile}">
        <#include "footer.ftl">
    </body>
</html>
