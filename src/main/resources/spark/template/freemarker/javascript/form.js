
var list_separator = ';';

document.addEventListener("DOMContentLoaded", function(event) {
    var keywords = document.querySelectorAll(".keyword-item");
    console.log(keywords);
    Array.from(keywords).filter(kw => kw.dataset.added === "false").forEach(function(kw) {
        appendKeyword(kw.querySelector(".keyword-name").textContent, false);
    });
});

function resetForm() {
    console.log("resetting now.");
    var domain = document.querySelector("[name=domain]");
    domain.querySelectorAll("[selected]").forEach(function(s) {
        s.removeAttribute("selected");
    });
    domain.selectedIndex = 0;
    var person = document.querySelector("[name=person]");
    console.log("person: " + person);
    person.value = "";
    var intOwner = document.querySelector("[name=intellectualOwner]");
    console.log("person: " + intOwner);
    intOwner.value = "";
    var intRights = document.querySelector("[name=intellectualRights]");
    var defaultI = 0;
    var i = 0;
    intRights.querySelectorAll("option").forEach(function(s) {
        s.removeAttribute("selected");
        if (s.getAttribute("value") === "MZ.intellectualRightsPD")
            defaultI = i;
        i++;
    });
    intRights.selectedIndex = defaultI;
    resetKeywords();
    var pubRest = document.querySelector("[name=publicityRestrictions]");
    pubRest.querySelectorAll("[selected]").forEach(function(s) {
        s.removeAttribute("selected");
    });
    pubRest.selectedIndex = 0;
}


function returnKeywords(keywords) {
    console.log("returnKeywords: " + keywords);
    if (keywords) {
        var split = keywords.split(list_separator);
        split.forEach(function (value, index) {
            if (value)
                addKeywordListItem(value);
        });
    }
}

function _tokenize(value) {
    return list_separator + value + list_separator;
}

function appendKeyword(keyword, addListItem) {
    if (keyword && _isInputLegal(keyword)) {
        var input = document.getElementById('keywords-input');
        if (!input.value.includes(_tokenize(keyword))) {
            input.value += _tokenize(keyword);

            // already there
            if (addListItem)  {
                addKeywordListItem(escapeHtml(keyword));
            }
            return true;
        }
    }
    return false;
}

function addKeyword() {
    var keyword_text = document.getElementById('keywords-text');
    if (appendKeyword(keyword_text.value, true)) {
        keyword_text.value = '';
    }
}

function _isInputLegal(text) {
    return !text.includes(list_separator);
}

function escapeHtml(unsafe) {
    return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
}

function unescapeHtml(safe) {
    return safe
            .replace(/&amp;/g, "&")
            .replace(/&lt;/g, "<")
            .replace(/&gt;/g, ">")
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
}

function addKeywordListItem(value) {
    var ul = document.getElementById('keywords-list');
    var li_clone = _getClonableKeywordRow().cloneNode(true);
    li_clone.style = '';
    li_clone.removeAttribute("id");
    var span = _getKeywordRowSpan(li_clone);
    span.innerHTML = value;

    ul.appendChild(li_clone);
}

function _getClonableKeywordRow() {
    var clone = document.querySelector("#keywords-list-template");
    return clone;
}

function _getKeywordRowSpan(keywordRow) {
    return keywordRow.getElementsByTagName('span')[0];
}

function getParentByTagName(elem, tagName) {
    while (elem && elem.tagName !== tagName) {
        elem = elem.parentNode;
    }
    return elem;
}

function deleteKeyword(elem) {
    var li = getParentByTagName(elem, 'LI');

    var span = li.getElementsByTagName('span')[0];
    var keyword = unescapeHtml(span.innerHTML);

    var input = document.getElementById('keywords-input');
    input.value = input.value.replace(_tokenize(keyword), '');

    li.parentNode.removeChild(li);
}

function removeChildNodes(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function resetKeywords() {
    var input = document.getElementById('keywords-input');
    input.value = '';
    
    var clonableRow = _getClonableKeywordRow();
    var ul = document.getElementById('keywords-list');
    removeChildNodes(ul);
    ul.appendChild(clonableRow);
}

