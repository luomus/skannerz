// Create WebSocket connection.
var socketURL = 'ws://' + location.host + '/skannerz/websocket/echoProgrammatic';
var socket = initWS(socketURL);
var tableRowMap = new Map();
var connectionErrorAmount = 0;
var connectionCloseAmount = 0;
var validQR = /https?:\/\/.*/;

var pathSeparator = '//';

var statusCodes = {
    PROCESSING: "PROCESSING",
    ABORTED: "ABORTED",
    OK: "OK",
    HISTORY: "HISTORY",
    COULD_NOT_FIND_FILE: "COULD_NOT_FIND_FILE",
    CANNOT_UPDATE_UNPROCESSED: "CANNOT_UPDATE_UNPROCESSED",
    SEND_FAILED_FATAL: "SEND_FAILED_FATAL",
    SEND_FAILED_RESEND: "SEND_FAILED_RESEND",
    SEND_FAILED_BROKENIMAGE: "SEND_FAILED_BROKENIMAGE",
    SEND_FAILED_NOTSUPPORTED: "SEND_FAILED_NOTSUPPORTED",
    SEND_FAILED_MISSINGMETADATA: "SEND_FAILED_MISSINGMETADATA",
    QR_FAILED: "QR_FAILED",
    UNKNOWN_ERROR: "UNKNOWN_ERROR"
};

var iconPath = "svg/";
var icons = { //alert = user fixable issue
    PROCESSING: "spinner.svg",
    ABORTED: "timer.svg",
    OK: "ok.svg",
    HISTORY: "ok.svg",
    COULD_NOT_FIND_FILE: "fail.svg",
    CANNOT_UPDATE_UNPROCESSED: "alert.svg",
    SEND_FAILED_FATAL: "fail.svg",
    SEND_FAILED_RESEND: "alert.svg",
    SEND_FAILED_BROKENIMAGE: "fail.svg",
    SEND_FAILED_NOTSUPPORTED: "fail.svg",
    SEND_FAILED_MISSINGMETADATA: "alert.svg",
    QR_FAILED: "qr.svg",
    UNKNOWN_ERROR: "fail.svg"
};

/**
 * Listen for messages
 * @param {URL} url which the WebSocket tries to connect to
 * @returns {WebSocket}
 */
function initWS(url) {
    var newSocket = new WebSocket(url);
    newSocket.addEventListener('message', onConnectionMessage);
    newSocket.addEventListener('error', onConnectionError);
    newSocket.addEventListener('close', onConnectionClosed);
    return newSocket;
}

function statusUpdate() {
    var message = {};
    message.action = 'STATUS_UPDATE';
    socket.send(JSON.stringify(message));
}

function reprocess() {
    var message = {};
    message.action = 'REPROCESS';
    message.data = getAllSelectedFolders();
    socket.send(JSON.stringify(message));
    uncheckAllSelectedFolders();
}

function resend() {
    alert('Not yet implemented :) Try reprocessing the folders instead');
}

function updateQR(input, folderName) {
    if (_updateQR(folderName, input.value)) {
        input.readOnly = true;
    }
}

function _updateQR(folderName, link) {
    var message = {};
    message.action = 'UPDATE_QR';
    message.data = {
        folder: folderName,
        qrCode: link
    };

    if (link.match(validQR)) {
        console.log(link + " input ok.");
        socket.send(JSON.stringify(message));
        return true;
    } else {
        var tmplt = document.querySelector("#message-template");
        var msgLi = tmplt.cloneNode(true);
        msgLi.removeAttribute("id");
        msgLi.classList.remove("hide");
        msgLi.appendChild(document.createTextNode(link + " is not a valid ID."));
        addMessage(msgLi);
    }
    return false;
}

function getAllSelectedFolders() {
    var selectedFolders = [];
    var headers = document.querySelectorAll(".dropdown-header");
    for (var i = 0; i < headers.length; i++) {
        var header = headers[i];

        var checkBox = header.querySelector(".dropdown-checkbox");
        if (checkBox && checkBox.checked) {
            var folderName = header.id.replace("image-", "");
            selectedFolders.push(folderName);
        }
    }
    return selectedFolders;
}

function uncheckAllSelectedFolders() {
    var checkBoxes = document.querySelectorAll(".dropdown-checkbox");
    for (var i = 0; i < checkBoxes.length; i++) {
        checkBoxes[i].checked = false;
    }
}

function addMessage(listElement, replaceOld) {
    replaceOld = replaceOld || false;
    var messageList = document.querySelector("#messages");
    messageList.classList.remove("hide");
    
    if (replaceOld) {
        var id = listElement.getAttribute("id");
        var old = messageList.querySelector("#" + id);
        if (old) {
            old.remove();
        }
    }

    messageList.appendChild(listElement);
}


function onConnectionError(event) {
    console.log("Error occurred. " + event);
    if (connectionErrorAmount++ == 0) {
        console.log("This is the first error. Trying to reconnect once.");
        socket = initWS(socketURL);
        return;
    }

    var tmplt = document.querySelector("#websocket-error-template");
    var errorMsgLi = tmplt.cloneNode(true);
    errorMsgLi.setAttribute("id", "websocket-error");
    errorMsgLi.classList.remove("hide");
    addMessage(errorMsgLi, true);
}

function onConnectionClosed(event) {
    console.log("onConnectionClosed call.");
    if (connectionCloseAmount++ == 0) {
        console.log("First time of closing connection.");
        socket = initWS(socketURL);
        return;
    }

    console.log("Connection closed. " + event);

    var tmplt = document.querySelector("#websocket-closed-template");
    var closeMsgLi = tmplt.cloneNode(true);
    closeMsgLi.setAttribute("id", "websocket-closed");
    closeMsgLi.classList.remove("hide");

    addMessage(closeMsgLi, true);
}

/**
 * Called when PipelineWebSockets send a message
 * @param {type} event
 */
function onConnectionMessage(event) {
    var data = JSON.parse(event.data);

    for (var imageName in data.meta) {
        var meta = data.meta[imageName];
        
        var rowId = data.folder + pathSeparator + imageName;
        var row = tableRowMap.get(rowId);
        if (!row) {
            row = _addNewDropdownForRow();
            var displayableRow = _addNewDisplayableRow();
            row.id = 'image-details-' + rowId;
            displayableRow.id = 'image-' + rowId;
            displayableRow.setAttribute('onclick', 'toggleDropdown(this, \'' + row.id + '\')');

            tableRowMap.set(rowId, row);
        }
        updateRow(data, imageName, meta, row, rowId);
    }
}

function _cloneMonitorTableRow(template) {
    var tr_clone = template.cloneNode(true);
    var body = document.querySelector("table#monitor tbody");
    var topmostRow = _getTopmostRow();
    if (topmostRow) {
        body.insertBefore(tr_clone, topmostRow);
    } else {
        body.appendChild(tr_clone);
    }
    return tr_clone;
}

function _addNewDisplayableRow() {
    var tr_clone = _cloneMonitorTableRow(_getTemplateDisplayableRow());
    tr_clone.classList.remove('hide');
    return tr_clone;
}

function _addNewDropdownForRow() {
    return _cloneMonitorTableRow(_getTemplateDetailsRow());
}

function _getTemplateDisplayableRow() {
    return document.getElementById('image-template');
}

function _getTemplateDetailsRow() {
    return document.getElementById('image-details-template');
}

function _getTopmostRow() {
    var rows = document.querySelectorAll('table#monitor tbody tr');
    return rows.length > 2 ? rows[2] : null;
}

/**
 * Updates the dropdown row values, which in turn updates the displayable row's values
 * @param {type} data
 * @param {type} imageName the name of the image
 * @param {type} meta the Image specific metadata
 * @param {type} detailsRow
 * @param {type} rowId used for displayableRow HTML element id
 * @returns {undefined}
 */
function updateRow(data, imageName, meta, detailsRow, rowId) {
    detailsRow.querySelector('.dropdown-details').innerHTML = getDropdownDetailsHTML(data);
    detailsRow.querySelector('.dropdown-metaform').innerHTML = getDropdownMetaformHTML(data, meta);

    var displayableRowData = [
        _getIconFromStatusCode(data.statusCode, data.status),
        _getLink(data),
        data.folder,
        imageName,
        formatLastUpdated(data.lastUpdated),
        getCheckBox(rowId)
    ];
    var displayableRow = getDisplayableRow(detailsRow);
    setDisplayableRowValues(displayableRowData, displayableRow);
}

function getDropdownDetailsHTML(data) {
    var html = '';
    html += '<p>' + data.status + '</p>';
    return html;
}

function getDropdownMetaformHTML(data, meta) {
    var html = '';
    for (var property in meta) {
        html += '<div class="field">' + property + ': ' + JSON.stringify(meta[property]) + '</div>';
    }
    html += '<div class="field">id: ' + data["id"] + '</div>';
    html += '<div class="field">folder: ' + data["folder"] + '</div>';
    return html;
}

function _getLink(data) {
    if (data.statusCode === statusCodes.QR_FAILED) {
        return '<input type="url" onchange="updateQR(this,\'' + data.folder + '\');">';
    } else {
        return _getLinkFromId(data.id);
    }
}

function _getLinkFromId(id) {
    return id ? '<a href="' + id + '" target="_blank">' + id + '</a>' : '';
}

function _getIconFromStatusCode(statusCode, statusMessage) {
    var image = icons[statusCode] ? icons[statusCode] : 'spinner.svg';
    return '<img src="' + iconPath + image + '" alt="' + statusCode
            + '" title="' + statusMessage + '" class="icon">';
}

function formatLastUpdated(lastUpdated) {
    return lastUpdated.split('T')[1].split('.')[0];
}

function getCheckBox(rowId) {
    return '<input id="' + rowId + '-check" class="dropdown-checkbox" type="checkbox"'+
            'onclick="event.stopPropagation();">';
}

function getDisplayableRow(detailsRow) {
    var id = detailsRow.id;
    var newId = id.replace('image-details-', 'image-');
    return document.getElementById(newId);
}

function setDisplayableRowValues(displayableRowData, displayableRow) {
    var rowCells = displayableRow.getElementsByTagName("td");
    for (var i = 0, rowCell; (rowCell = rowCells[i]); i++) {
        rowCell.innerHTML = displayableRowData[i];
    }
}

function toggleDropdown(element, targetId) {
    element.classList.toggle('dropdown-toggle');
    document.getElementById(targetId).classList.toggle('hide');
}