<div class="center width-786">
    <#include "messages.ftl">
    <form method="post">
        <button name="formButton" class="half" formaction="${contextPath}/monitor/newform" type="submit">Change metadata</button>
        <!-- <button name="abortButton"  class="half" formaction="${contextPath}/monitor/abort" type="submit">Abort</button> -->
        <button name="resetButton"  class="half" formaction="${contextPath}/monitor/reset" type="submit">Reset metadata</button>
    </form>
    <table id="monitor" class="center full">
        <colgroup>
           <col span="1" style="width: 8%;">
           <col span="1" style="width: 23%;">
           <col span="1" style="width: 33%;">
           <col span="1" style="width: 13%;">
           <col span="1" style="width: 18%;">
           <col span="1" style="width: 5%;">
        </colgroup>
        <thead>
            <tr>
                <th>Status</th>
                <th>ID</th>
                <th>Folder name</th>
                <th>Image name</th>
                <th>Updated</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr id="image-template" class="hide dropdown-header" onclick="toggleDropdown(this, 'image-details-template')">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr id="image-details-template" class="hide dropdown-body">
                <td colspan="6">
                    <div class="dropdown-details"></div>
                    <div class="dropdown-metaform"></div>
                    <div class="dropdown-actions hide"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <button class="half" onclick="statusUpdate()">Refresh statuses</button>
        <button class="half" onclick="reprocess()">Reprocess</button>
    </div>
</div>
<script>
    <#include 'javascript/monitor.js' parse=false encoding="UTF-8">
</script>
