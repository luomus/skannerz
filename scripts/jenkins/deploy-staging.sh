#!/bin/bash
#
# This script deploys the .war file built by Jenkins into a staging tomcat instance.
#

# anna logia jotenkin jenkinsille


WAR_PATH="build/libs/skannerz.war"
cp "$WAR_PATH" "/var/lib/tomcat/webapps/"

[[ -e $WAR_PATH ]];

if [[ -$? -ne 0 ]]; then
	echo "$WAR_PATH does not exist. Exiting."
	exit 1
fi
	

if [[ $? -ne 0 ]]; then
	echo "Couldn't copy the war in $WAR_PATH to '/var/lib/tomcat/webapps'"
	exit 1
fi

LOG_DIR="/usr/share/tomcat/logs"
LOG_FILE="$LOG_DIR/catalina.$(date +%Y-%m-%d).log"
CREATE_EVENT="./ CREATE catalina.$(date +%Y-%m-%d).log"

if [[ ! -e $LOG_FILE ]]; then
	echo "Waiting for creation of $LOG_FILE for up to 60 seconds."
	inotifywait --timeout 60 -e create $LOG_DIR
fi

if [[ -e $LOG_FILE ]]; then
	echo "Outputting log for 60 seconds."
	timeout 60 tail -f $LOG_FILE
	[[ $? -eq 124 ]]; 
	# timeout returns 124 in case of a time out, hence the [[ $? -eq 124 ]];
else
	echo "Create event of inotifywait was for something else than $LOG_FILE."
fi

